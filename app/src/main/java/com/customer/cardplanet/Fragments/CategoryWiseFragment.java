package com.customer.cardplanet.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.Activities.FilterCateWiseActivity;
import com.customer.cardplanet.AdapterClasses.CateViewAdapter;
import com.customer.cardplanet.AdapterClasses.MerchantAdapter;
import com.customer.cardplanet.AdapterClasses.SecondCategoryAdapter;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.Merchant_Category;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CategoryWiseFragment extends Fragment implements SecondCategoryAdapter.CateMethodCallBack {


    @BindView(R.id.noData)
    RelativeLayout noData;
    @BindView(R.id.root)
    RelativeLayout root;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleCate;
    ArrayList<Merchant_Category> subCateList = new ArrayList<>();
    private SecondCategoryAdapter cateViewAdapter;
    @BindView(R.id.searchView)
    SearchView searchView;
    private Animation animation;
    private GridLayoutManager gridLayoutManager;

    public CategoryWiseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common_filter, container, false);
        ButterKnife.bind(this,view);
        cardPlanetProgress=new CardPlanetProgress(getActivity());
        root.setVisibility(View.GONE);
        //set animation on adapter...
        int resId1 = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId1);
        viewRecycleCate.setLayoutAnimation(controller);
        //end animation...
        gridLayoutManager = new GridLayoutManager(getActivity(),3);
        viewRecycleCate.setLayoutManager(gridLayoutManager);
        cateViewAdapter = new SecondCategoryAdapter(subCateList, getActivity(),this);
        viewRecycleCate.setAdapter(cateViewAdapter);
        fetchJCateList();

        return view;
    }

    private void fetchJCateList() {
        cardPlanetProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("view","");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.PRODUCT_CATE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error== null){
                    try {
                        if (subCateList != null)
                            subCateList.clear();

                        subCateList =Merchant_Category.parseCategoryArray(jsonObject.getJSONArray("ProdCate_data"));
                        cateViewAdapter.setSubCateList(subCateList);
                        cateViewAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleCate.getAdapter().notifyDataSetChanged();
                        viewRecycleCate.scheduleLayoutAnimation();
                        //-----------end------------

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    subCateList.clear();
                    cateViewAdapter.setSubCateList(subCateList);
                    cateViewAdapter.notifyDataSetChanged();
                 //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        }, getActivity(), params);
    }

    @Override
    public void cateMethod(Merchant_Category subCateBean) {
        Intent intent= new Intent(getActivity(), FilterCateWiseActivity.class);
        intent.putExtra("cate_id",subCateBean.getId());
        intent.putExtra("cate_name",subCateBean.getName());
        startActivity(intent);
        Animatoo.animateShrink(getActivity());

    }
}