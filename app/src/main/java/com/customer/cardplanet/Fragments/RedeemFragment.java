package com.customer.cardplanet.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.Activities.MerchantDetails;
import com.customer.cardplanet.AdapterClasses.CustomerServiceAdapter;
import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RedeemFragment extends Fragment implements CustomerServiceAdapter.ProdMethodCallBack {


    private Animation animation;
    @BindView(R.id.noData)
    RelativeLayout noData;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.rowcount)
    TextView rows;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<Cust_ServiceBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private CustomerServiceAdapter productAdapter;

    public RedeemFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common, container, false);
        ButterKnife.bind(this, view);
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);


        cardPlanetProgress=new CardPlanetProgress(getActivity());

        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        //set animation on adapter...
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...
        layoutManager = new LinearLayoutManager(getActivity());
        viewRecycleProduct.setLayoutManager(layoutManager);
        productAdapter = new CustomerServiceAdapter(merchantBeans, getActivity(),this);
        viewRecycleProduct.setAdapter(productAdapter);

        return  view;
    }

    @Override
    public void prodMethod(Cust_ServiceBean cust_serviceBean) {

        Gson gson = new Gson();
        String prod_data = gson.toJson(cust_serviceBean, Cust_ServiceBean.class);
        Intent intent = new Intent(getActivity(), MerchantDetails.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateShrink(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchMerchantList();
    }



    private void fetchMerchantList() {
        HashMap<String, String> params = new HashMap<>();
        //    Toast.makeText(getActivity(),"id "+ User.getCurrentUser().getId(),Toast.LENGTH_LONG).show();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("v_type", "redeem");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CUST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = Cust_ServiceBean.parseCust_ServiceBeanArray(jsonObject.getJSONArray("service_data"));
                        productAdapter.setMerchantBeans(merchantBeans);
                        productAdapter.notifyDataSetChanged();
                        rows.setVisibility(View.VISIBLE);
                        rows.setText("Total Entry-" +merchantBeans.get(0).getRowcount()+"");
                        //setanimation on adapter...
                        viewRecycleProduct.getAdapter().notifyDataSetChanged();
                        viewRecycleProduct.scheduleLayoutAnimation();
                        //-----------end------------
                        noData.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                    productAdapter.setMerchantBeans(merchantBeans);
                    productAdapter.notifyDataSetChanged();
                    noData.setVisibility(View.VISIBLE);

                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, getActivity(), params);
    }

    @Override
    public void prodAddCartMethod(Cust_ServiceBean productBean) {

    }
}