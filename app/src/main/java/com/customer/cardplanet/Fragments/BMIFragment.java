package com.customer.cardplanet.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.customer.cardplanet.Activities.ResultcActivity;
import com.customer.cardplanet.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BMIFragment extends Fragment {

    float height,weight;
    TextView height_txt,age;
    int count_weight = 50,count_age = 19;
    RelativeLayout weight_plus, weight_minus, age_plus, age_minus;
    boolean male_clk = true, female_clk = true, check1 = true, check2 = true;
    TextView weight_txt;
    SeekBar Seekbar;
    String gender="";
    private Animation animation;
    public BMIFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_bmi, container, false);
        ButterKnife.bind(this,view);
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        height_txt = view.findViewById(R.id.height_txt);

        final TextView female_text = view.findViewById(R.id.female);
        final TextView male_text = view.findViewById(R.id.male);

        CardView card_female = view.findViewById(R.id.cardView_female);
        CardView card_male = view.findViewById(R.id.cardView_male);

        age_minus = view.findViewById(R.id.age_minus);
        age_plus = view.findViewById(R.id.age_plus);

        weight_minus = view.findViewById(R.id.weight_minus);
        weight_plus = view.findViewById(R.id.weight_plus);

        card_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check1) {
                    card_male.startAnimation(animation);
//                    if (male_clk) {
                        female_text.setTextColor(Color.parseColor("#8D8E99"));
                        female_text.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.female,0,0);
                        male_text.setTextColor(Color.parseColor("#049bdd"));
                        male_text.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.male_white,0,0);
//                        male_clk = false;
//                        check2 = true;
//                         female_clk = true;
//                         check1 = false;
                          gender="Male";
                //    Toast.makeText(getActivity(),gender,Toast.LENGTH_SHORT).show();


//                    } else {
//
//                        male_text.setTextColor(Color.parseColor("#8D8E99"));
//                        male_text.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.male,0,0);
//                        male_clk = true;
//                        check2 = true;
//                    }
                }
            }
        });

        card_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check2) {
//                    if (female_clk) {
                    card_female.startAnimation(animation);
                        male_text.setTextColor(Color.parseColor("#8D8E99"));
                        male_text.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.male,0,0);
                        female_text.setTextColor(Color.parseColor("#049bdd"));
                        female_text.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.female_white,0,0);
//                        check1 = true;
//                        female_clk = false;
                        gender="Female";
                  //  Toast.makeText(getActivity(),gender,Toast.LENGTH_SHORT).show();
//                        female_clk = false;
//                        check1 = false;
//                    }
//                    else  {
//
//                        female_text.setTextColor(Color.parseColor("#8D8E99"));
//                        female_text.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.female,0,0);
//                        female_clk = true;
//                        check1 = true;
//                    }
                }
            }
        });


        TextView calculate = view.findViewById(R.id.calculate);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculate.startAnimation(animation);
                if (gender.equalsIgnoreCase("")){
                    Toast.makeText(getActivity(),"Select your Gender",Toast.LENGTH_SHORT).show();
                }else if (height_txt.getText().toString().equalsIgnoreCase("0cm")){
                    Toast.makeText(getActivity(),"Mentioned your Height",Toast.LENGTH_SHORT).show();
                }else {
                    CalculateBMI();
                }

            }
        });

        weight_txt = view.findViewById(R.id.weight);
        Seekbar = view.findViewById(R.id.Seekbar);
        age = view.findViewById(R.id.age);


        age_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                age_plus.startAnimation(animation);
                count_age++;
                age.setText(String.valueOf(count_age));
            }
        });

        age_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                age_minus.startAnimation(animation);
                count_age--;
                age.setText(String.valueOf(count_age));
            }
        });



        weight_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weight_plus.startAnimation(animation);
                count_weight++;
                weight_txt.setText(String.valueOf(count_weight));
            }
        });

        weight_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weight_minus.startAnimation(animation);
                count_weight--;
                weight_txt.setText(String.valueOf(count_weight));
            }
        });

        weight = Float.parseFloat(weight_txt.getText().toString());

        Seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String ht = progress + getResources().getString(R.string.cm);
                height_txt.setText(ht);
                height = (float)(progress)/100;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;

    }




    private void CalculateBMI() {

        float BMI = weight / (height * height);
        Intent intent = new Intent(getActivity(), ResultcActivity.class);
        intent.putExtra("BMI",BMI);
        intent.putExtra("gender",gender);
        intent.putExtra("age",age.getText().toString());
        startActivity(intent);
    }
}