package com.customer.cardplanet.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.Activities.FilterCateWiseActivity;
import com.customer.cardplanet.Activities.MerchantDetailsWithransaction;
import com.customer.cardplanet.AdapterClasses.MerchantAdapter;
import com.customer.cardplanet.AdapterClasses.MerchantDiscountAdapter;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DiscountWiseFragment extends Fragment implements MerchantDiscountAdapter.ProdMethodCallBack {

    @BindView(R.id.noData)
    RelativeLayout noData;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<MerchantBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private MerchantDiscountAdapter productAdapter;
    @BindView(R.id.searchView)
    SearchView searchView;
    private Animation animation;
    String es_cate_id="";
    public DiscountWiseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_common_filter, container, false);
        ButterKnife.bind(this,view);
        cardPlanetProgress=new CardPlanetProgress(getActivity());
        searchView.setQueryHint("Enter discount ...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE
                productAdapter.getFilter().filter(query);
                return false;
            }
        });


        //set animation on adapter...
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...
        layoutManager = new LinearLayoutManager(getActivity());
        viewRecycleProduct.setLayoutManager(layoutManager);
        productAdapter = new MerchantDiscountAdapter(merchantBeans, getActivity(),this);
        viewRecycleProduct.setAdapter(productAdapter);
//        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        fetchMerchantList();
        return view;
    }

    private void fetchMerchantList() {

        HashMap<String, String> params = new HashMap<>();

        params.put("cate_id", "");

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MERCHANT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();

                        merchantBeans = MerchantBean.parseMerchantBeanArray(jsonObject.getJSONArray("merchant_data"));

                        if (merchantBeans.size() > 0){
                            productAdapter.setMerchantBeans(merchantBeans);
                            productAdapter.notifyDataSetChanged();
                            //setanimation on adapter...
                            viewRecycleProduct.getAdapter().notifyDataSetChanged();
                            viewRecycleProduct.scheduleLayoutAnimation();
                            //-----------end------------
                            noData.setVisibility(View.GONE);
                        }else{
                            productAdapter.setMerchantBeans(merchantBeans);
                            productAdapter.notifyDataSetChanged();
                            noData.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                    productAdapter.setMerchantBeans(merchantBeans);
                    productAdapter.notifyDataSetChanged();
                    noData.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, getActivity(), params);
    }
    @Override
    public void prodMethod(MerchantBean cust_serviceBean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(cust_serviceBean, MerchantBean.class);
        Intent intent = new Intent(getActivity(), MerchantDetailsWithransaction.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateShrink(getActivity());
    }

    @Override
    public void prodAddCartMethod(MerchantBean productBean) {

    }
}