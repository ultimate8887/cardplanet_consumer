package com.customer.cardplanet.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.Activities.AllMerchantList;
import com.customer.cardplanet.Activities.DiscountWiseActivity;
import com.customer.cardplanet.Activities.FilterCateWiseActivity;
import com.customer.cardplanet.Activities.FinalHomeActivity;
import com.customer.cardplanet.Activities.LocationActivity;
import com.customer.cardplanet.Activities.MerchantDetailsWithransaction;
import com.customer.cardplanet.Activities.PaymentActivity;
import com.customer.cardplanet.Activities.TodaySavingActivity;
import com.customer.cardplanet.Activities.ViewAllCategoryActivity;
import com.customer.cardplanet.AdapterClasses.B_SliderAdapter;
import com.customer.cardplanet.AdapterClasses.CateViewAdapter;
import com.customer.cardplanet.AdapterClasses.Discount_Adapter;
import com.customer.cardplanet.AdapterClasses.MerchantHomeAdapter;
import com.customer.cardplanet.AdapterClasses.SliderAdapter;
import com.customer.cardplanet.BeanClasses.Customer_Saving;
import com.customer.cardplanet.BeanClasses.Discount;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.Merchant_Category;
import com.customer.cardplanet.BeanClasses.NewsData;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeFragment extends Fragment implements CateViewAdapter.CateMethodCallBack,MerchantHomeAdapter.ProdMethodCallBack,Discount_Adapter.DiscountMethodCallBack{

    @BindView(R.id.today_saving)
    TextView today_saving;
    @BindView(R.id.total_saving)
    TextView total_saving;
    @BindView(R.id.today_card)
    LinearLayout today_card;
    @BindView(R.id.total_card)
    LinearLayout total_card;
    private Animation animation;
    CardPlanetProgress cancelProgressBar;
    SliderAdapter adapter;
    B_SliderAdapter b_adapter;
    SliderView sliderView,b_sliderView;
    ArrayList<NewsData> sliderDataArrayList = new ArrayList<>();
    ArrayList<Customer_Saving> customer_savings;

    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<MerchantBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    MerchantHomeAdapter merchantHomeAdapter;

    @BindView(R.id.viewRecycleDiscount)
    RecyclerView viewRecycleDiscount;
    ArrayList<Discount> discounts = new ArrayList<>();
    Discount_Adapter discount_adapter;

    @BindView(R.id.viewRecycleCate)
    RecyclerView viewRecycleCate;
    ArrayList<Merchant_Category> subCateList = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;
    private CateViewAdapter cateViewAdapter;
    @BindView(R.id.viewall)
    TextView viewall;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.viewallcate)
    TextView viewallcate;
    int i=0;


    @BindView(R.id.t_view1)
    RelativeLayout t_view1;
    @BindView(R.id.f_view1)
    LinearLayout f_view1;

    @BindView(R.id.b_t_view1)
    RelativeLayout b_t_view1;
    @BindView(R.id.b_f_view1)
    LinearLayout b_f_view1;

    @BindView(R.id.discount_shimmer)
    ShimmerFrameLayout discount_shimmer;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.b_shimmer)
    ShimmerFrameLayout b_shimmer;

    @BindView(R.id.cate_shimmer)
    ShimmerFrameLayout cate_shimmer;
    @BindView(R.id.merchant_shimmer)
    ShimmerFrameLayout merchant_shimmer;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        cancelProgressBar=new CardPlanetProgress(getActivity());

        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);
         sliderView = view.findViewById(R.id.slider);
         b_sliderView = view.findViewById(R.id.b_slider);
         adapter = new SliderAdapter(sliderDataArrayList, getActivity());
         b_adapter = new B_SliderAdapter(sliderDataArrayList, getActivity());


        //set animation on adapter...
        int resId3 = R.anim.layout_animation_left_to_right;
        final LayoutAnimationController controller2 =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId3);
        viewRecycleDiscount.setLayoutAnimation(controller2);
        //end animation...

        // Merchant list...

        viewRecycleDiscount.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        discount_adapter = new Discount_Adapter(discounts, getActivity(),this);
        viewRecycleDiscount.setAdapter(discount_adapter);




        //set animation on adapter...
        int resId = R.anim.layout_animation_left_to_right;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...

        viewRecycleProduct.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        merchantHomeAdapter = new MerchantHomeAdapter(merchantBeans, getActivity(),this);
        viewRecycleProduct.setAdapter(merchantHomeAdapter);


        //set animation on adapter...
        int resId1 = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId1);
        viewRecycleCate.setLayoutAnimation(controller);
        //end animation...
        gridLayoutManager = new GridLayoutManager(getActivity(),4);
        viewRecycleCate.setLayoutManager(gridLayoutManager);
        cateViewAdapter = new CateViewAdapter(subCateList, getActivity(),this);
        viewRecycleCate.setAdapter(cateViewAdapter);
        newslist();

        //Top SliderView.......
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);
        // below method is used to
        // setadapter to sliderview.
        sliderView.setSliderAdapter(adapter);
        // below method is use to set
        // scroll time in seconds.
        sliderView.setScrollTimeInSec(4);
        // to set it scrollable automatically
        // we use below method.
        sliderView.setAutoCycle(true);
        // to start autocycle below method is used.
        sliderView.startAutoCycle();
        //SliderView Code end....

        //Bottom SliderView..
        bottom_banner();
        b_sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
//        b_sliderView.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
//        b_sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);
        // below method is used to
        // setadapter to sliderview.
        b_sliderView.setSliderAdapter(b_adapter);
        // below method is use to set
        // scroll time in seconds.
   //     b_sliderView.setScrollTimeInSec(4);
        // to set it scrollable automatically
        // we use below method.
   //     b_sliderView.setAutoCycle(true);
        // to start autocycle below method is used.
   //     b_sliderView.startAutoCycle();
        //b_SliderView Code end....

        return view;
    }

    private void bottom_banner() {

    }

    @Override
    public void onResume() {
        super.onResume();
        fetchMerchantList();
        newslist();
        bottom_banner();
        fetchJCateList();
        fetWalletPrice();
        fetchDiscount();
        discount_shimmer.startShimmer();
        shimmer.startShimmer();
        cate_shimmer.startShimmer();
        merchant_shimmer.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
        discount_shimmer.startShimmer();
        shimmer.startShimmer();
        cate_shimmer.startShimmer();
        merchant_shimmer.startShimmer();
    }

    private void fetchDiscount() {
        cancelProgressBar.show();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.DISCOUNT_URL, apiCallback1, getActivity(), null);

    }
    ApiHandler.ApiCallback apiCallback1 = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            cancelProgressBar.dismiss();

            if (error == null) {
                try {
                    if (discounts != null) {
                        discounts.clear();
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("discount_data");
                    discounts = Discount.parseDiscountArrayyy(jsonArray);

                    discount_adapter.setDiscounts(discounts);
                    discount_adapter.notifyDataSetChanged();

                    discount_shimmer.stopShimmer();
                    discount_shimmer.setVisibility(View.GONE);
                    viewRecycleDiscount.setVisibility(View.VISIBLE);

                    //setanimation on adapter...
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

          //      Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @OnClick(R.id.viewall)
    public void viewall() {
        viewall.startAnimation(animation);
        Intent intent= new Intent(getActivity(), AllMerchantList.class);
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    @OnClick(R.id.text)
    public void text() {
      //  text.startAnimation(animation);
        Intent intent= new Intent(getActivity(), AllMerchantList.class);
        startActivity(intent);
        Animatoo.animateFade(getActivity());
    }

    @OnClick(R.id.viewallcate)
    public void viewallcate() {
        viewallcate.startAnimation(animation);
        Intent intent= new Intent(getActivity(), ViewAllCategoryActivity.class);
//        intent.putExtra("cate_id","");
//        intent.putExtra("cate_name","");
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }


    private void fetchMerchantList() {
        HashMap<String, String> params = new HashMap<>();
        params.put("cate_id","");
        params.put("view","home");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MERCHANT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = MerchantBean.parseMerchantBeanArray(jsonObject.getJSONArray("merchant_data"));
                        merchantHomeAdapter.setMerchantBeans(merchantBeans);
                        merchantHomeAdapter.notifyDataSetChanged();


                        //setanimation on adapter...
                        viewRecycleProduct.getAdapter().notifyDataSetChanged();
                        viewRecycleProduct.scheduleLayoutAnimation();
                        //-----------end------------


                        merchant_shimmer.stopShimmer();
                        merchant_shimmer.setVisibility(View.GONE);
                        viewRecycleProduct.setVisibility(View.VISIBLE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                    merchantHomeAdapter.setMerchantBeans(merchantBeans);
                    merchantHomeAdapter.notifyDataSetChanged();


               //     Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, getActivity(), params);
    }


    @OnClick(R.id.today_card)
    public void today_card() {
        today_saving.startAnimation(animation);
    //     startActivity(new Intent(getActivity(), PaymentActivity.class));
        Intent intent= new Intent(getActivity(), TodaySavingActivity.class);
        intent.putExtra("v_type","today");
        intent.putExtra("saving",today_saving.getText().toString());
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    @OnClick(R.id.total_card)
    public void total_card() {
        total_saving.startAnimation(animation);
        // startActivity(new Intent(getActivity(), AllMerchantList.class));
        Intent intent= new Intent(getActivity(), TodaySavingActivity.class);
        intent.putExtra("v_type","history");
        intent.putExtra("saving",total_saving.getText().toString());
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    private void fetWalletPrice() {

        HashMap<String, String> params = new HashMap<>();
        params.put("userid",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SAVING_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (customer_savings != null)
                            customer_savings.clear();
                        customer_savings = Customer_Saving.parseCustomer_SavingArrayyy(jsonObject.getJSONArray("saving_data"));

                        //  Log.e("title" , offer_banners.get(0).getC_status());
                        if (!customer_savings.get(0).getToday().equalsIgnoreCase("0")){
                            today_saving.setText(customer_savings.get(0).getToday());
                        }else {
                            today_saving.setText("0");
                        }
                        if (!customer_savings.get(0).getTotal().equalsIgnoreCase("0")){
                            total_saving.setText(customer_savings.get(0).getTotal());
                        }else {
                            total_saving.setText("0");
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }


        }, getActivity(), params);
    }



    private void newslist() {
        i++;
        cancelProgressBar.show();
        ApiHandler.apiHit(Request.Method.GET, Constants.getBaseURL() + Constants.NEWS_URL, apiCallback, getActivity(), null);

    }
    ApiHandler.ApiCallback apiCallback = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            cancelProgressBar.dismiss();

            if (error == null) {
                try {
//                    if (sliderDataArrayList != null) {
//                        sliderDataArrayList.clear();
//                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("news_data");
                    sliderDataArrayList = NewsData.parseNewsDataArrayyy(jsonArray);

                    Log.e("image2", String.valueOf(sliderDataArrayList.get(0).getImage()));
                    if (sliderDataArrayList.get(0).getDate()!=null) {
                        Log.e("image", sliderDataArrayList.get(0).getDate());
                    }else{
                        Log.e("image", "empty");
                    }
                    //Top SliderView
                    adapter.setmSliderItems(sliderDataArrayList);
                    adapter.notifyDataSetChanged();
                    shimmer.stopShimmer();
                    shimmer.setVisibility(View.GONE);
                    t_view1.setVisibility(View.VISIBLE);

                    //bottom SliderView
                    b_adapter.setmSliderItems(sliderDataArrayList);
                    b_adapter.notifyDataSetChanged();
                    b_shimmer.stopShimmer();
                    b_shimmer.setVisibility(View.GONE);
                    b_t_view1.setVisibility(View.VISIBLE);

                        //setanimation on adapter...
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

          //      Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void cateMethod(Merchant_Category subCateBean) {
        Intent intent= new Intent(getActivity(), FilterCateWiseActivity.class);
        intent.putExtra("cate_id",subCateBean.getId());
        intent.putExtra("cate_name",subCateBean.getName());
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    private void fetchJCateList() {
        cancelProgressBar.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("view","home");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.PRODUCT_CATE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error== null){
                    try {
                        if (subCateList != null)
                            subCateList.clear();

                        subCateList =Merchant_Category.parseCategoryArray(jsonObject.getJSONArray("ProdCate_data"));
                        cateViewAdapter.setSubCateList(subCateList);
                        cateViewAdapter.notifyDataSetChanged();


                        //setanimation on adapter...
                        viewRecycleCate.getAdapter().notifyDataSetChanged();
                        viewRecycleCate.scheduleLayoutAnimation();
                        //-----------end------------


                        cate_shimmer.stopShimmer();
                        cate_shimmer.setVisibility(View.GONE);
                        viewRecycleCate.setVisibility(View.VISIBLE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    subCateList.clear();
                    cateViewAdapter.setSubCateList(subCateList);
                    cateViewAdapter.notifyDataSetChanged();
                //    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        }, getActivity(), params);
    }

    @Override
    public void prodMethod(MerchantBean merchantBean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(merchantBean, MerchantBean.class);
        Intent intent = new Intent(getActivity(), MerchantDetailsWithransaction.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    @Override
    public void prodAddCartMethod(MerchantBean productBean) {

    }

    @Override
    public void discountMethod(Discount discount) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(discount, Discount.class);
        Intent intent = new Intent(getActivity(), DiscountWiseActivity.class);
        intent.putExtra("discount_data", prod_data);
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }


    //    @OnClick(R.id.logout)
//    public void logout() {
//
//    }
}