package com.customer.cardplanet.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.Activities.AllMerchantList;
import com.customer.cardplanet.Activities.FilterCateWiseActivity;
import com.customer.cardplanet.Activities.MerchantDetails;
import com.customer.cardplanet.Activities.MerchantDetailsWithransaction;
import com.customer.cardplanet.AdapterClasses.CateViewAdapter;
import com.customer.cardplanet.AdapterClasses.MerchantAdapter;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.Merchant_Category;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SearchFragment extends Fragment implements CateViewAdapter.CateMethodCallBack,MerchantAdapter.ProdMethodCallBack {

    @BindView(R.id.viewRecycleCate)
    RecyclerView viewRecycleCate;
    ArrayList<Merchant_Category> subCateList = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;
    private CateViewAdapter cateViewAdapter;
    private Animation animation;
    @BindView(R.id.noData)
    RelativeLayout noData;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.refresh)
    FloatingActionButton refresh;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<MerchantBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private MerchantAdapter productAdapter;
    @BindView(R.id.viewall)
    TextView viewall;

    @BindView(R.id.cate_shimmer)
    ShimmerFrameLayout cate_shimmer;
    @BindView(R.id.merchant_shimmer)
    ShimmerFrameLayout merchant_shimmer;


    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this,view);
        cardPlanetProgress=new CardPlanetProgress(getActivity());

        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.btn_blink_animation);

        //set animation on adapter...
        int resId1 = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId1);
        viewRecycleCate.setLayoutAnimation(controller);
        //end animation...

        // category list...
        viewRecycleCate.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
     //   viewRecycleCate.setLayoutManager(gridLayoutManager);
        cateViewAdapter = new CateViewAdapter(subCateList, getActivity(),this);
        viewRecycleCate.setAdapter(cateViewAdapter);


        //set animation on adapter...
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...
        layoutManager = new LinearLayoutManager(getActivity());
        viewRecycleProduct.setLayoutManager(layoutManager);
        viewRecycleProduct.setNestedScrollingEnabled(false);
        productAdapter = new MerchantAdapter(merchantBeans, getActivity(),this);
        viewRecycleProduct.setAdapter(productAdapter);
        fetchJCateList();
        fetchMerchantList();
        return  view;
    }

    @OnClick(R.id.viewall)
    public void viewall() {
        viewall.startAnimation(animation);
       // startActivity(new Intent(getActivity(), AllMerchantList.class));

        Intent intent= new Intent(getActivity(), AllMerchantList.class);
        intent.putExtra("cate_id","");
        intent.putExtra("cate_name","");
        startActivity(intent);

        Animatoo.animateZoom(getActivity());
    }


    private void fetchMerchantList() {
        HashMap<String, String> params = new HashMap<>();
        params.put("cate_id","");
        params.put("view","h_merchant");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MERCHANT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = MerchantBean.parseMerchantBeanArray(jsonObject.getJSONArray("merchant_data"));
                        productAdapter.setMerchantBeans(merchantBeans);
                        productAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleProduct.getAdapter().notifyDataSetChanged();
                        viewRecycleProduct.scheduleLayoutAnimation();
                        //-----------end------------


                        merchant_shimmer.stopShimmer();
                        merchant_shimmer.setVisibility(View.GONE);
                        viewRecycleProduct.setVisibility(View.VISIBLE);

                        noData.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                    productAdapter.setMerchantBeans(merchantBeans);
                    productAdapter.notifyDataSetChanged();
                    noData.setVisibility(View.VISIBLE);

                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, getActivity(), params);
    }

    private void fetchJCateList() {
        cardPlanetProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("view","search");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.PRODUCT_CATE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
              cardPlanetProgress.dismiss();
                if (error== null){
                    try {
                        if (subCateList != null)
                            subCateList.clear();

                        subCateList =Merchant_Category.parseCategoryArray(jsonObject.getJSONArray("ProdCate_data"));
                        cateViewAdapter.setSubCateList(subCateList);
                        cateViewAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleCate.getAdapter().notifyDataSetChanged();
                        viewRecycleCate.scheduleLayoutAnimation();
                        //-----------end------------

                        cate_shimmer.stopShimmer();
                        cate_shimmer.setVisibility(View.GONE);
                        viewRecycleCate.setVisibility(View.VISIBLE);

                        noData.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    subCateList.clear();
                    cateViewAdapter.setSubCateList(subCateList);
                    cateViewAdapter.notifyDataSetChanged();
                    noData.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        }, getActivity(), params);
    }

    @Override
    public void cateMethod(Merchant_Category subCateBean) {

        Intent intent= new Intent(getActivity(), FilterCateWiseActivity.class);
        intent.putExtra("cate_id",subCateBean.getId());
        intent.putExtra("cate_name",subCateBean.getName());
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    @Override
    public void prodMethod(MerchantBean merchantBean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(merchantBean, MerchantBean.class);
        Intent intent = new Intent(getActivity(), MerchantDetailsWithransaction.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateZoom(getActivity());
    }

    @Override
    public void prodAddCartMethod(MerchantBean merchantBean) {

    }
}