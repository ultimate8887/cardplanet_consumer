package com.customer.cardplanet.BeanClasses;

import com.customer.cardplanet.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MerchantBean {

    /**
     * id : 2
     * cate_id : 3
     * subcate_id : 2
     * childcate_id : 3
     * cate_name : Automotive
     * subcate_name : Auto Accessories
     * childcate_name : Others
     * name : solar cell
     * address : mitha bajar
     * city : Awankha
     * state : Punjab
     * pincode : 143531
     * image : images/hospitals/1618651581IMG-20200731-WA0000.jpg
     * discount : 18
     * owner_name :
     */

    private static String ID = "id";
    private static String CAT_ID = "cate_id";
    private static String SUBCAT_ID = "subcate_id";
    private static String CHILDCAT_ID = "childcate_id";
    private static String CAT_NAME = "cate_name";
    private static String SUBCAT_NAME = "subcate_name";
    private static String CHILDCAT_NAME = "childcate_name";
    private static String NAME = "name";
    private static String ADD = "address";
    private static String CITY = "city";
    private static String STATE = "state";
    private static String IMAGE = "image";
    private static String PIN = "pincode";
    private static String OWNER_NAME = "owner_name";
    private static String DISCOUNT = "discount";
    private static String CONTACT = "contact";
    private static String LAT = "latitude";
    private static String LONG = "longitude";
    private static String FID = "f_id";
    private static String COUNT = "rowcount";

    private String id;
    private String cate_id;
    private String subcate_id;
    private String childcate_id;
    private String cate_name;
    private String subcate_name;
    private String childcate_name;
    private String name;
    private String address;
    private String city;
    private String state;
    private String pincode;
    private String image;
    private String discount;
    private String owner_name;
    private String rowcount;

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }



    public String getF_id() {
        return f_id;
    }

    public void setF_id(String f_id) {
        this.f_id = f_id;
    }

    private String f_id;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    private String latitude;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    private String longitude;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    private String contact;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCate_id() {
        return cate_id;
    }

    public void setCate_id(String cate_id) {
        this.cate_id = cate_id;
    }

    public String getSubcate_id() {
        return subcate_id;
    }

    public void setSubcate_id(String subcate_id) {
        this.subcate_id = subcate_id;
    }

    public String getChildcate_id() {
        return childcate_id;
    }

    public void setChildcate_id(String childcate_id) {
        this.childcate_id = childcate_id;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public String getSubcate_name() {
        return subcate_name;
    }

    public void setSubcate_name(String subcate_name) {
        this.subcate_name = subcate_name;
    }

    public String getChildcate_name() {
        return childcate_name;
    }

    public void setChildcate_name(String childcate_name) {
        this.childcate_name = childcate_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }


    public static ArrayList<MerchantBean> parseMerchantBeanArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<MerchantBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                MerchantBean p = parseMerchantBean(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static MerchantBean parseMerchantBean(JSONObject jsonObject) {
        MerchantBean data = new MerchantBean();

        try {
            if (jsonObject.has(ID)) {
                data.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(SUBCAT_ID) && !jsonObject.getString(SUBCAT_ID).isEmpty() && !jsonObject.getString(SUBCAT_ID).equalsIgnoreCase("null")) {
                data.setSubcate_id(jsonObject.getString(SUBCAT_ID));
            }
            if (jsonObject.has(CAT_ID) && !jsonObject.getString(CAT_ID).isEmpty() && !jsonObject.getString(CAT_ID).equalsIgnoreCase("null")) {
                data.setCate_id(jsonObject.getString(CAT_ID));
            }
            if (jsonObject.has(CHILDCAT_ID) && !jsonObject.getString(CHILDCAT_ID).isEmpty() && !jsonObject.getString(CHILDCAT_ID).equalsIgnoreCase("null")) {
                data.setChildcate_id(jsonObject.getString(CHILDCAT_ID));
            }
            if (jsonObject.has(CONTACT) && !jsonObject.getString(CONTACT).isEmpty() && !jsonObject.getString(CONTACT).equalsIgnoreCase("null")) {
                data.setContact(jsonObject.getString(CONTACT));
            }
            if (jsonObject.has(CAT_NAME) && !jsonObject.getString(CAT_NAME).isEmpty() && !jsonObject.getString(CAT_NAME).equalsIgnoreCase("null")) {
                data.setCate_name(jsonObject.getString(CAT_NAME));
            }
            if (jsonObject.has(COUNT) && !jsonObject.getString(COUNT).isEmpty() && !jsonObject.getString(COUNT).equalsIgnoreCase("null")) {
                data.setRowcount(jsonObject.getString(COUNT));
            }
            if (jsonObject.has(FID) && !jsonObject.getString(FID).isEmpty() && !jsonObject.getString(FID).equalsIgnoreCase("null")) {
                data.setF_id(jsonObject.getString(FID));
            }
            if (jsonObject.has(SUBCAT_NAME) && !jsonObject.getString(SUBCAT_NAME).isEmpty() && !jsonObject.getString(SUBCAT_NAME).equalsIgnoreCase("null")) {
                data.setSubcate_name(jsonObject.getString(SUBCAT_NAME));
            }
            if (jsonObject.has(CHILDCAT_NAME) && !jsonObject.getString(CHILDCAT_NAME).isEmpty() && !jsonObject.getString(CHILDCAT_NAME).equalsIgnoreCase("null")) {
                data.setChildcate_name(jsonObject.getString(CHILDCAT_NAME));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                data.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                data.setImage(Constants.getImageBaseURL() + jsonObject.getString(IMAGE));
            }

            if (jsonObject.has(ADD) && !jsonObject.getString(ADD).isEmpty() && !jsonObject.getString(ADD).equalsIgnoreCase("null")) {
                data.setAddress(jsonObject.getString(ADD));
            }
            if (jsonObject.has(LAT) && !jsonObject.getString(LAT).isEmpty() && !jsonObject.getString(LAT).equalsIgnoreCase("null")) {
                data.setLatitude(jsonObject.getString(LAT));
            }
            if (jsonObject.has(LONG) && !jsonObject.getString(LONG).isEmpty() && !jsonObject.getString(LONG).equalsIgnoreCase("null")) {
                data.setLongitude(jsonObject.getString(LONG));
            }

            if (jsonObject.has(DISCOUNT) && !jsonObject.getString(DISCOUNT).isEmpty() && !jsonObject.getString(DISCOUNT).equalsIgnoreCase("null")) {
                data.setDiscount(jsonObject.getString(DISCOUNT));
            }

            if (jsonObject.has(DISCOUNT) && !jsonObject.getString(DISCOUNT).isEmpty() && !jsonObject.getString(DISCOUNT).equalsIgnoreCase("null")) {
                data.setDiscount(jsonObject.getString(DISCOUNT));
            }

            if (jsonObject.has(CITY) && !jsonObject.getString(CITY).isEmpty() && !jsonObject.getString(CITY).equalsIgnoreCase("null")) {
                data.setCity(jsonObject.getString(CITY));
            }
            if (jsonObject.has(STATE) && !jsonObject.getString(STATE).isEmpty() && !jsonObject.getString(STATE).equalsIgnoreCase("null")) {
                data.setState(jsonObject.getString(STATE));
            }
            if (jsonObject.has(PIN) && !jsonObject.getString(PIN).isEmpty() && !jsonObject.getString(PIN).equalsIgnoreCase("null")) {
                data.setPincode(jsonObject.getString(PIN));
            }
            if (jsonObject.has(OWNER_NAME) && !jsonObject.getString(OWNER_NAME).isEmpty() && !jsonObject.getString(OWNER_NAME).equalsIgnoreCase("null")) {
                data.setOwner_name(jsonObject.getString(OWNER_NAME));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;

    }




}
