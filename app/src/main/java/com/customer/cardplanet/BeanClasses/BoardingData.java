package com.customer.cardplanet.BeanClasses;

public class BoardingData {
    String tittle;
    Integer image;
    String desc;

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }



    public BoardingData(String tittle, Integer image, String desc) {
        this.tittle = tittle;
        this.image = image;
        this.desc = desc;
    }



}
