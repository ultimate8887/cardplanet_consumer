package com.customer.cardplanet.BeanClasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CardDetailbean {


    /**
     * id : 1
     * amount : 1188
     * wallet : 990
     * ratio : 5
     * card_no : sa78980712sh
     * amount_limit : 40000
     * profile_status : active
     * det : 1
     * card_holderss : Redeem Voucher(Sanam2021
     * custcashback : 80
     * detuct_wallet_amount : 0
     * custpid : 2
     * custredeem_voucher :
     * custvoucher_status : voucher
     * custvoucher_no : Sanam2021
     */

    private String id;
    private String amount;
    private String wallet;
    private String ratio;
    private String card_no;
    private String amount_limit;
    private String profile_status;
    private int det;
    private String card_holderss;
    private String custcashback;
    private String detuct_wallet_amount;
    private String custpid;
    private String custredeem_voucher;
    private String custvoucher_status;
    private String custvoucher_no;
    /**
     * custdetailid : 23
     */

    private String custdetailid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getRatio() {
        return ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getAmount_limit() {
        return amount_limit;
    }

    public void setAmount_limit(String amount_limit) {
        this.amount_limit = amount_limit;
    }

    public String getProfile_status() {
        return profile_status;
    }

    public void setProfile_status(String profile_status) {
        this.profile_status = profile_status;
    }

    public int getDet() {
        return det;
    }

    public void setDet(int det) {
        this.det = det;
    }

    public String getCard_holderss() {
        return card_holderss;
    }

    public void setCard_holderss(String card_holderss) {
        this.card_holderss = card_holderss;
    }

    public String getCustcashback() {
        return custcashback;
    }

    public void setCustcashback(String custcashback) {
        this.custcashback = custcashback;
    }

    public String getDetuct_wallet_amount() {
        return detuct_wallet_amount;
    }

    public void setDetuct_wallet_amount(String detuct_wallet_amount) {
        this.detuct_wallet_amount = detuct_wallet_amount;
    }

    public String getCustpid() {
        return custpid;
    }

    public void setCustpid(String custpid) {
        this.custpid = custpid;
    }

    public String getCustredeem_voucher() {
        return custredeem_voucher;
    }

    public void setCustredeem_voucher(String custredeem_voucher) {
        this.custredeem_voucher = custredeem_voucher;
    }

    public String getCustvoucher_status() {
        return custvoucher_status;
    }

    public void setCustvoucher_status(String custvoucher_status) {
        this.custvoucher_status = custvoucher_status;
    }

    public String getCustvoucher_no() {
        return custvoucher_no;
    }

    public void setCustvoucher_no(String custvoucher_no) {
        this.custvoucher_no = custvoucher_no;
    }

    public String getCustdetailid() {
        return custdetailid;
    }

    public void setCustdetailid(String custdetailid) {
        this.custdetailid = custdetailid;
    }

    public static ArrayList<CardDetailbean> parseHWArray(JSONArray arrayObj) {
        ArrayList<CardDetailbean> list = new ArrayList<CardDetailbean>();
        try {

            for (int i = 0; i < arrayObj.length(); i++) {
                CardDetailbean p = parseHWObject(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static CardDetailbean parseHWObject(JSONObject jsonObject) {
        CardDetailbean user = new CardDetailbean();
        try {
            if (jsonObject.has("id") && !jsonObject.getString("id").isEmpty() && !jsonObject.getString("id").equalsIgnoreCase("null")) {
                user.setId(jsonObject.getString("id"));
            }

            if (jsonObject.has("amount") && !jsonObject.getString("amount").isEmpty() && !jsonObject.getString("amount").equalsIgnoreCase("null")) {
                user.setAmount(jsonObject.getString("amount"));
            }

            if (jsonObject.has("wallet") && !jsonObject.getString("wallet").isEmpty() && !jsonObject.getString("wallet").equalsIgnoreCase("null")) {
                user.setWallet(jsonObject.getString("wallet"));
            }
            if (jsonObject.has("ratio") && !jsonObject.getString("ratio").isEmpty() && !jsonObject.getString("ratio").equalsIgnoreCase("null")) {
                user.setRatio(jsonObject.getString("ratio"));
            }

            if (jsonObject.has("card_no") && !jsonObject.getString("card_no").isEmpty() && !jsonObject.getString("card_no").equalsIgnoreCase("null")) {
                user.setCard_no(jsonObject.getString("card_no"));
            }

            if (jsonObject.has("amount_limit") && !jsonObject.getString("amount_limit").isEmpty() && !jsonObject.getString("amount_limit").equalsIgnoreCase("null")) {
                user.setAmount_limit(jsonObject.getString("amount_limit"));
            }

            if (jsonObject.has("profile_status") && !jsonObject.getString("profile_status").isEmpty() && !jsonObject.getString("profile_status").equalsIgnoreCase("null")) {
                user.setProfile_status(jsonObject.getString("profile_status"));
            }

            if (jsonObject.has("detuct_wallet_amount") && !jsonObject.getString("detuct_wallet_amount").isEmpty() && !jsonObject.getString("detuct_wallet_amount").equalsIgnoreCase("null")) {
                user.setDetuct_wallet_amount(jsonObject.getString("detuct_wallet_amount"));
            }


            if (jsonObject.has("custdetailid") && !jsonObject.getString("custdetailid").isEmpty() && !jsonObject.getString("custdetailid").equalsIgnoreCase("null")) {
                user.setCustdetailid(jsonObject.getString("custdetailid"));
            }




            if (jsonObject.has("custpid") && !jsonObject.getString("custpid").isEmpty() && !jsonObject.getString("custpid").equalsIgnoreCase("null")) {
                user.setCustpid(jsonObject.getString("custpid"));
            }


            if (jsonObject.has("custcashback") && !jsonObject.getString("custcashback").isEmpty() && !jsonObject.getString("custcashback").equalsIgnoreCase("null")) {
                user.setCustcashback(jsonObject.getString("custcashback"));
            }

            if (jsonObject.has("custredeem_voucher") && !jsonObject.getString("custredeem_voucher").isEmpty() && !jsonObject.getString("custredeem_voucher").equalsIgnoreCase("null")) {
                user.setCustredeem_voucher(jsonObject.getString("custredeem_voucher"));
            }


            if (jsonObject.has("custvoucher_status") && !jsonObject.getString("custvoucher_status").isEmpty() && !jsonObject.getString("custvoucher_status").equalsIgnoreCase("null")) {
                user.setCustvoucher_status(jsonObject.getString("custvoucher_status"));
            }

            if (jsonObject.has("custvoucher_no") && !jsonObject.getString("custvoucher_no").isEmpty() && !jsonObject.getString("custvoucher_no").equalsIgnoreCase("null")) {
                user.setCustvoucher_no(jsonObject.getString("custvoucher_no"));
            }

            if (jsonObject.has("det") && !jsonObject.getString("det").isEmpty() && !jsonObject.getString("det").equalsIgnoreCase("null")) {
                user.setDet(Integer.parseInt(jsonObject.getString("det")));
            }

            if (jsonObject.has("card_holderss") && !jsonObject.getString("card_holderss").isEmpty() && !jsonObject.getString("card_holderss").equalsIgnoreCase("null")) {
                user.setCard_holderss(jsonObject.getString("card_holderss"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

}
