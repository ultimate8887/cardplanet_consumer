package com.customer.cardplanet.BeanClasses;

import com.customer.cardplanet.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Merchant_Category {
    private static String ID = "id";
    private static String TITLE = "name";
    private static String IMAGE = "image";

    /**
     * id : 1
     * name : Category Wise
     * image : images/prod_category/product_wise.png
     */

    private String id;
    private String name;
    private String image;
    /**
     * text_color : #FF5252
     */

    private String text_color;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static ArrayList<Merchant_Category> parseCategoryArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<Merchant_Category>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                Merchant_Category p = parseCategoryBean(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Merchant_Category parseCategoryBean(JSONObject jsonObject) {

        Merchant_Category casteObj = new Merchant_Category();
        try {
            if (jsonObject.has(ID)) {
                casteObj.setId(jsonObject.getString(ID));
            }
            if (jsonObject.has(TITLE) && !jsonObject.getString(TITLE).isEmpty() && !jsonObject.getString(TITLE).equalsIgnoreCase("null")) {
                casteObj.setName(jsonObject.getString(TITLE));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL() + jsonObject.getString(IMAGE));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }
}
