package com.customer.cardplanet.BeanClasses;

import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Customer_Saving {


    /**
     * id : 1
     * wallet : 990
     * today : 128
     * total : 128
     */

    private String id;
    private String wallet;
    private String today;
    private String total;


    public static ArrayList<Customer_Saving> parseCustomer_SavingArrayyy(JSONArray jsonArray) {
        ArrayList<Customer_Saving> list = new ArrayList<Customer_Saving>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Customer_Saving p = parseCustomer_SavingObject(jsonArray.getJSONObject(i));

                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }

    private static Customer_Saving parseCustomer_SavingObject(JSONObject jsonObject) {
        Customer_Saving casteObj = new Customer_Saving();
        try {
            if (jsonObject.has("id")) {
                casteObj.setId(jsonObject.getString("id"));

            }
            if (jsonObject.has("wallet") && !jsonObject.getString("wallet").isEmpty() && !jsonObject.getString("wallet").equalsIgnoreCase("null")) {
                casteObj.setWallet(jsonObject.getString("wallet"));
            }

            if (jsonObject.has("today") && !jsonObject.getString("today").isEmpty() && !jsonObject.getString("today").equalsIgnoreCase("null")) {
                casteObj.setToday(jsonObject.getString("today"));
            }
            if (jsonObject.has("total") && !jsonObject.getString("total").isEmpty() && !jsonObject.getString("total").equalsIgnoreCase("null")) {
                casteObj.setTotal(jsonObject.getString("total"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


}
