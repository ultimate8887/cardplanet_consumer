package com.customer.cardplanet.BeanClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.customer.cardplanet.CardPlanetCustomerApp;
import com.customer.cardplanet.Utility.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class User {

    private static User sInstance;
    private static String PREFRENCES_USER = "user";
    private static String ID = "id";
    private static String C_NAME = "name";
    private static String F_NAME = "father_name";
    private static String EMAIL = "emailid";
    private static String PASSWORD = "password";
    private static String MOBILE_NO = "mobile";
    private static String DEVICE_TOKEN = "device_token";
    private static String COUNTRY = "country";
    private static String C_GENDER = "gender";
    private static String C_DOB = "dob";
    private static String O_ADD = "optionaladdress";
    private static String AADHARCARD = "aadharno";
    private static String CITY = "city";
    private static String STATE = "state";
    private static String PIN = "pincode";
    private static String REG_DATE = "dateofgenetatecard";
    private static String RENEWAL_DATE = "renewal_date";
    private static String IMAGE = "profilepic";
    private static String ADD = "address";
    private static String STATUS ="status";
    private static String P_STATUS ="profile_status";
    private static String TPYE ="card_type";
    private static String DOC ="doc_upload";
    private static String BG ="bloodgroup";
    private static String D_TPYE ="doc_type";

    private static String S_TPYE = "service_type";
    private static String CARDNO = "card_no";
    private static String AMMOUNT_LMT ="amount_limit";
    private static String RATIO ="ratio";
    private static String AMMOUNT ="amount";
    private static String WALLET ="wallet";


    private static String FAMILY_NAME1 = "family1name";
    private static String FAMILY_DOB1 ="family1dob";
    private static String FAMILY_MOB1 ="family1mobile";
    private static String FAMILY_GENDER1 ="family1gender";
    private static String FAMILY_REL1 ="family1relation";

    private static String FAMILY_NAME2 = "family2name";
    private static String FAMILY_DOB2 ="family2dob";
    private static String FAMILY_MOB2 ="family2mobile";
    private static String FAMILY_GENDER2 ="family2gender";
    private static String FAMILY_REL2 ="family2relation";

    private static String FAMILY_NAME3 = "family3name";
    private static String FAMILY_DOB3 ="family3dob";
    private static String FAMILY_MOB3 ="family3mobile";
    private static String FAMILY_GENDER3 ="family3gender";
    private static String FAMILY_REL3 ="family3relation";
    private static String LAT = "latitude";
    private static String LONG = "longitude";
    private static String D_PIN = "device_pin";

    /**
     * id : 2
     * name : Hariom Tank
     * father_name : RAMSHANKAR TANK
     * password : 123456789
     * mobile : 9871487254
     * emailid : hariomtank@gmail.com
     * gender : Male
     * dob : 1999-01-01
     * state : Rajasthan
     * country : india
     * pincode : 302020
     * address : 110 Sumer Nagar Mansarovar
     * city : Mansarovar
     * optionaladdress :
     * aadharno : 123456789100
     * doc_type : Aadhar
     * doc_upload : 1617811168fb cover page.png
     * bloodgroup : AB+
     * family1name : ABC ABC
     * family1dob : 1999-11-01
     * family1mobile : 9871487254
     * family1gender : Male
     * family1relation : Father
     * family2name :
     * family2dob : 0000-00-00
     * family2mobile :
     * family2gender :
     * family2relation :
     * family3name :
     * family3dob : 0000-00-00
     * family3mobile :
     * family3gender :
     * family3relation :
     * dateofgenetatecard : 2021-04-07
     * renewal_date : 2022-04-07
     * card_type : basic
     * amount : 1188
     * wallet : 1188
     * ratio : 6
     * service_type :
     * card_no : Ha56789100RA
     * amount_limit : 40000
     * profile_status : active
     * status : paid
     * profilepic : ../images/members/1617811168fb cover page.png
     */

    private String id;
    private String name;
    private String father_name;
    private String password;
    private String mobile;
    private String emailid;
    private String gender;
    private String dob;
    private String state;
    private String country;
    private String pincode;
    private String address;
    private String city;
    private String optionaladdress;
    private String aadharno;
    private String doc_type;
    private String doc_upload;
    private String bloodgroup;
    private String family1name;
    private String family1dob;
    private String family1mobile;
    private String family1gender;
    private String family1relation;
    private String family2name;
    private String family2dob;
    private String family2mobile;
    private String family2gender;
    private String family2relation;
    private String family3name;
    private String family3dob;
    private String family3mobile;
    private String family3gender;
    private String family3relation;
    private String dateofgenetatecard;
    private String renewal_date;
    private String card_type;
    private String amount;
    private String wallet;
    private String ratio;
    private String service_type;
    private String card_no;
    private String amount_limit;
    private String profile_status;
    private String status;
    private String profilepic;
    private String latitude;

    public String getDevice_pin() {
        return device_pin;
    }

    public void setDevice_pin(String device_pin) {
        this.device_pin = device_pin;
    }

    private String device_pin;


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    private String longitude;

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    private String device_token;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOptionaladdress() {
        return optionaladdress;
    }

    public void setOptionaladdress(String optionaladdress) {
        this.optionaladdress = optionaladdress;
    }

    public String getAadharno() {
        return aadharno;
    }

    public void setAadharno(String aadharno) {
        this.aadharno = aadharno;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getDoc_upload() {
        return doc_upload;
    }

    public void setDoc_upload(String doc_upload) {
        this.doc_upload = doc_upload;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getFamily1name() {
        return family1name;
    }

    public void setFamily1name(String family1name) {
        this.family1name = family1name;
    }

    public String getFamily1dob() {
        return family1dob;
    }

    public void setFamily1dob(String family1dob) {
        this.family1dob = family1dob;
    }

    public String getFamily1mobile() {
        return family1mobile;
    }

    public void setFamily1mobile(String family1mobile) {
        this.family1mobile = family1mobile;
    }

    public String getFamily1gender() {
        return family1gender;
    }

    public void setFamily1gender(String family1gender) {
        this.family1gender = family1gender;
    }

    public String getFamily1relation() {
        return family1relation;
    }

    public void setFamily1relation(String family1relation) {
        this.family1relation = family1relation;
    }

    public String getFamily2name() {
        return family2name;
    }

    public void setFamily2name(String family2name) {
        this.family2name = family2name;
    }

    public String getFamily2dob() {
        return family2dob;
    }

    public void setFamily2dob(String family2dob) {
        this.family2dob = family2dob;
    }

    public String getFamily2mobile() {
        return family2mobile;
    }

    public void setFamily2mobile(String family2mobile) {
        this.family2mobile = family2mobile;
    }

    public String getFamily2gender() {
        return family2gender;
    }

    public void setFamily2gender(String family2gender) {
        this.family2gender = family2gender;
    }

    public String getFamily2relation() {
        return family2relation;
    }

    public void setFamily2relation(String family2relation) {
        this.family2relation = family2relation;
    }

    public String getFamily3name() {
        return family3name;
    }

    public void setFamily3name(String family3name) {
        this.family3name = family3name;
    }

    public String getFamily3dob() {
        return family3dob;
    }

    public void setFamily3dob(String family3dob) {
        this.family3dob = family3dob;
    }

    public String getFamily3mobile() {
        return family3mobile;
    }

    public void setFamily3mobile(String family3mobile) {
        this.family3mobile = family3mobile;
    }

    public String getFamily3gender() {
        return family3gender;
    }

    public void setFamily3gender(String family3gender) {
        this.family3gender = family3gender;
    }

    public String getFamily3relation() {
        return family3relation;
    }

    public void setFamily3relation(String family3relation) {
        this.family3relation = family3relation;
    }

    public String getDateofgenetatecard() {
        return dateofgenetatecard;
    }

    public void setDateofgenetatecard(String dateofgenetatecard) {
        this.dateofgenetatecard = dateofgenetatecard;
    }

    public String getRenewal_date() {
        return renewal_date;
    }

    public void setRenewal_date(String renewal_date) {
        this.renewal_date = renewal_date;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getRatio() {
        return ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getAmount_limit() {
        return amount_limit;
    }

    public void setAmount_limit(String amount_limit) {
        this.amount_limit = amount_limit;
    }

    public String getProfile_status() {
        return profile_status;
    }

    public void setProfile_status(String profile_status) {
        this.profile_status = profile_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public static User getCurrentUser() {
        if (sInstance == null) {
            SharedPreferences preferences = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            String userString = preferences.getString(PREFRENCES_USER, null);
            if (userString != null) {
                Gson gson = new Gson();
                sInstance = gson.fromJson(userString, User.class);
                return sInstance;
            } else {
                return null;
            }
        } else {
            return sInstance;
        }
    }


    public static User setCurrentUser(JSONObject jsonObject) throws JSONException {

        User user = getCurrentUser();
        try {

            if (user == null) {
                user = new User();
            }

            if (jsonObject.has(ID)) {
                user.setId(jsonObject.getString(ID));
            }

            if (jsonObject.has(PASSWORD) && !jsonObject.getString(PASSWORD).isEmpty() && !jsonObject.getString(PASSWORD).equalsIgnoreCase("null")) {
                user.setPassword(jsonObject.getString(PASSWORD));
            }
            if (jsonObject.has(MOBILE_NO) && !jsonObject.getString(MOBILE_NO).isEmpty() && !jsonObject.getString(MOBILE_NO).equalsIgnoreCase("null")) {
                user.setMobile(jsonObject.getString(MOBILE_NO));
            }

            if (jsonObject.has(EMAIL) && !jsonObject.getString(EMAIL).isEmpty() && !jsonObject.getString(EMAIL).equalsIgnoreCase("null")) {
                user.setEmailid(jsonObject.getString(EMAIL));
            }

            if (jsonObject.has(D_PIN) && !jsonObject.getString(D_PIN).isEmpty() && !jsonObject.getString(D_PIN).equalsIgnoreCase("null")) {
                user.setDevice_pin(jsonObject.getString(D_PIN));
            }


            if (jsonObject.has(F_NAME) && !jsonObject.getString(F_NAME).isEmpty() && !jsonObject.getString(F_NAME).equalsIgnoreCase("null")) {
                user.setFather_name(jsonObject.getString(F_NAME));
            }

            if (jsonObject.has(DEVICE_TOKEN) && !jsonObject.getString(DEVICE_TOKEN).isEmpty() && !jsonObject.getString(DEVICE_TOKEN).equalsIgnoreCase("null")) {
                user.setDevice_token(jsonObject.getString(DEVICE_TOKEN));
            }


            if (jsonObject.has(ADD) && !jsonObject.getString(ADD).isEmpty() && !jsonObject.getString(ADD).equalsIgnoreCase("null")) {
                user.setAddress((jsonObject.getString(ADD)));
            }

            if (jsonObject.has(LAT) && !jsonObject.getString(LAT).isEmpty() && !jsonObject.getString(LAT).equalsIgnoreCase("null")) {
                user.setLatitude(jsonObject.getString(LAT));
            }
            if (jsonObject.has(LONG) && !jsonObject.getString(LONG).isEmpty() && !jsonObject.getString(LONG).equalsIgnoreCase("null")) {
                user.setLongitude(jsonObject.getString(LONG));
            }

            if (jsonObject.has(C_NAME) && !jsonObject.getString(C_NAME).isEmpty() && !jsonObject.getString(C_NAME).equalsIgnoreCase("null")) {
                user.setName(jsonObject.getString(C_NAME));
            }
            if (jsonObject.has(C_GENDER) && !jsonObject.getString(C_GENDER).isEmpty() && !jsonObject.getString(C_GENDER).equalsIgnoreCase("null")) {
                user.setGender(jsonObject.getString(C_GENDER));
            }

            if (jsonObject.has(O_ADD) && !jsonObject.getString(O_ADD).isEmpty() && !jsonObject.getString(O_ADD).equalsIgnoreCase("null")) {
                user.setOptionaladdress(jsonObject.getString(O_ADD));
            }
            if (jsonObject.has(COUNTRY) && !jsonObject.getString(COUNTRY).isEmpty() && !jsonObject.getString(COUNTRY).equalsIgnoreCase("null")) {
                user.setCountry(jsonObject.getString(COUNTRY));
            }

            if (jsonObject.has(C_DOB) && !jsonObject.getString(C_DOB).isEmpty() && !jsonObject.getString(C_DOB).equalsIgnoreCase("null")) {
                user.setDob(jsonObject.getString(C_DOB));
            }

            if (jsonObject.has(CITY) && !jsonObject.getString(CITY).isEmpty() && !jsonObject.getString(CITY).equalsIgnoreCase("null")) {
                user.setCity(jsonObject.getString(CITY));
            }

            if (jsonObject.has(STATE) && !jsonObject.getString(STATE).isEmpty() && !jsonObject.getString(STATE).equalsIgnoreCase("null")) {
                user.setState((jsonObject.getString(STATE)));
            }

            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                user.setProfilepic(( Constants.getImageBaseURL() + jsonObject.getString(IMAGE)));
            }

            if (jsonObject.has(PIN) && !jsonObject.getString(PIN).isEmpty() && !jsonObject.getString(PIN).equalsIgnoreCase("null")) {
                user.setPincode(jsonObject.getString(PIN));
            }

            if (jsonObject.has(REG_DATE) && !jsonObject.getString(REG_DATE).isEmpty() && !jsonObject.getString(REG_DATE).equalsIgnoreCase("null")) {
                user.setDateofgenetatecard(jsonObject.getString(REG_DATE));
            }
            if (jsonObject.has(RENEWAL_DATE) && !jsonObject.getString(RENEWAL_DATE).isEmpty() && !jsonObject.getString(RENEWAL_DATE).equalsIgnoreCase("null")) {
                user.setRenewal_date(jsonObject.getString(RENEWAL_DATE));
            }

            if (jsonObject.has(STATUS) && !jsonObject.getString(STATUS).isEmpty() && !jsonObject.getString(STATUS).equalsIgnoreCase("null")) {
                user.setStatus(jsonObject.getString(STATUS));
            }
            if (jsonObject.has(TPYE) && !jsonObject.getString(TPYE).isEmpty() && !jsonObject.getString(TPYE).equalsIgnoreCase("null")) {
                user.setCard_type(jsonObject.getString(TPYE));
            }

            if (jsonObject.has(AADHARCARD) && !jsonObject.getString(AADHARCARD).isEmpty() && !jsonObject.getString(AADHARCARD).equalsIgnoreCase("null")) {
                user.setAadharno(jsonObject.getString(AADHARCARD));
            }
            if (jsonObject.has(P_STATUS) && !jsonObject.getString(P_STATUS).isEmpty() && !jsonObject.getString(P_STATUS).equalsIgnoreCase("null")) {
                user.setProfile_status(jsonObject.getString(P_STATUS));
            }
            if (jsonObject.has(BG) && !jsonObject.getString(BG).isEmpty() && !jsonObject.getString(BG).equalsIgnoreCase("null")) {
                user.setBloodgroup(jsonObject.getString(BG));
            }

            if (jsonObject.has(DOC) && !jsonObject.getString(DOC).isEmpty() && !jsonObject.getString(DOC).equalsIgnoreCase("null")) {
                user.setDoc_upload(Constants.getImageBaseURL() +jsonObject.getString(DOC));
            }

            if (jsonObject.has(D_TPYE) && !jsonObject.getString(D_TPYE).isEmpty() && !jsonObject.getString(D_TPYE).equalsIgnoreCase("null")) {
                user.setDoc_type(jsonObject.getString(D_TPYE));
            }
            if (jsonObject.has(CARDNO) && !jsonObject.getString(CARDNO).isEmpty() && !jsonObject.getString(CARDNO).equalsIgnoreCase("null")) {
                user.setCard_no(jsonObject.getString(CARDNO));
            }
            if (jsonObject.has(S_TPYE) && !jsonObject.getString(S_TPYE).isEmpty() && !jsonObject.getString(S_TPYE).equalsIgnoreCase("null")) {
                user.setService_type(jsonObject.getString(S_TPYE));
            }

            if (jsonObject.has(AMMOUNT_LMT) && !jsonObject.getString(AMMOUNT_LMT).isEmpty() && !jsonObject.getString(AMMOUNT_LMT).equalsIgnoreCase("null")) {
                user.setAmount_limit(jsonObject.getString(AMMOUNT_LMT));
            }
            if (jsonObject.has(AMMOUNT) && !jsonObject.getString(AMMOUNT).isEmpty() && !jsonObject.getString(AMMOUNT).equalsIgnoreCase("null")) {
                user.setAmount(jsonObject.getString(AMMOUNT));
            }
            if (jsonObject.has(WALLET) && !jsonObject.getString(WALLET).isEmpty() && !jsonObject.getString(WALLET).equalsIgnoreCase("null")) {
                user.setWallet(jsonObject.getString(WALLET));
            }
            if (jsonObject.has(RATIO) && !jsonObject.getString(RATIO).isEmpty() && !jsonObject.getString(RATIO).equalsIgnoreCase("null")) {
                user.setRatio(jsonObject.getString(RATIO));
            }



            if (jsonObject.has(FAMILY_NAME1) && !jsonObject.getString(FAMILY_NAME1).isEmpty() && !jsonObject.getString(FAMILY_NAME1).equalsIgnoreCase("null")) {
                user.setFamily1name(jsonObject.getString(FAMILY_NAME1));
            }

            if (jsonObject.has(FAMILY_DOB1) && !jsonObject.getString(FAMILY_DOB1).isEmpty() && !jsonObject.getString(FAMILY_DOB1).equalsIgnoreCase("null")) {
                user.setFamily1dob(jsonObject.getString(FAMILY_DOB1));
            }
            if (jsonObject.has(FAMILY_GENDER1) && !jsonObject.getString(FAMILY_GENDER1).isEmpty() && !jsonObject.getString(FAMILY_GENDER1).equalsIgnoreCase("null")) {
                user.setFamily1gender(jsonObject.getString(FAMILY_GENDER1));
            }
            if (jsonObject.has(FAMILY_MOB1) && !jsonObject.getString(FAMILY_MOB1).isEmpty() && !jsonObject.getString(FAMILY_MOB1).equalsIgnoreCase("null")) {
                user.setFamily1mobile(jsonObject.getString(FAMILY_MOB1));
            }
            if (jsonObject.has(FAMILY_REL1) && !jsonObject.getString(FAMILY_REL1).isEmpty() && !jsonObject.getString(FAMILY_REL1).equalsIgnoreCase("null")) {
                user.setFamily1relation(jsonObject.getString(FAMILY_REL1));
            }


            if (jsonObject.has(FAMILY_NAME2) && !jsonObject.getString(FAMILY_NAME2).isEmpty() && !jsonObject.getString(FAMILY_NAME2).equalsIgnoreCase("null")) {
                user.setFamily2name(jsonObject.getString(FAMILY_NAME2));
            }

            if (jsonObject.has(FAMILY_DOB2) && !jsonObject.getString(FAMILY_DOB2).isEmpty() && !jsonObject.getString(FAMILY_DOB2).equalsIgnoreCase("null")) {
                user.setFamily2dob(jsonObject.getString(FAMILY_DOB2));
            }
            if (jsonObject.has(FAMILY_GENDER2) && !jsonObject.getString(FAMILY_GENDER2).isEmpty() && !jsonObject.getString(FAMILY_GENDER2).equalsIgnoreCase("null")) {
                user.setFamily2gender(jsonObject.getString(FAMILY_GENDER2));
            }
            if (jsonObject.has(FAMILY_MOB2) && !jsonObject.getString(FAMILY_MOB2).isEmpty() && !jsonObject.getString(FAMILY_MOB2).equalsIgnoreCase("null")) {
                user.setFamily2mobile(jsonObject.getString(FAMILY_MOB2));
            }
            if (jsonObject.has(FAMILY_REL2) && !jsonObject.getString(FAMILY_REL2).isEmpty() && !jsonObject.getString(FAMILY_REL2).equalsIgnoreCase("null")) {
                user.setFamily2relation(jsonObject.getString(FAMILY_REL2));
            }


            if (jsonObject.has(FAMILY_NAME3) && !jsonObject.getString(FAMILY_NAME3).isEmpty() && !jsonObject.getString(FAMILY_NAME3).equalsIgnoreCase("null")) {
                user.setFamily2name(jsonObject.getString(FAMILY_NAME3));
            }

            if (jsonObject.has(FAMILY_DOB3) && !jsonObject.getString(FAMILY_DOB3).isEmpty() && !jsonObject.getString(FAMILY_DOB3).equalsIgnoreCase("null")) {
                user.setFamily3dob(jsonObject.getString(FAMILY_DOB3));
            }
            if (jsonObject.has(FAMILY_GENDER3) && !jsonObject.getString(FAMILY_GENDER3).isEmpty() && !jsonObject.getString(FAMILY_GENDER3).equalsIgnoreCase("null")) {
                user.setFamily3gender(jsonObject.getString(FAMILY_GENDER3));
            }
            if (jsonObject.has(FAMILY_MOB3) && !jsonObject.getString(FAMILY_MOB3).isEmpty() && !jsonObject.getString(FAMILY_MOB3).equalsIgnoreCase("null")) {
                user.setFamily3mobile(jsonObject.getString(FAMILY_MOB3));
            }
            if (jsonObject.has(FAMILY_REL3) && !jsonObject.getString(FAMILY_REL3).isEmpty() && !jsonObject.getString(FAMILY_REL3).equalsIgnoreCase("null")) {
                user.setFamily3relation(jsonObject.getString(FAMILY_REL3));
            }






            Gson gson = new Gson();
            String userString = gson.toJson(user, User.class);
            SharedPreferences preferences = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
            preferences.edit().putString(PREFRENCES_USER, userString).commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }


    public static void logout() {
        sInstance = null;
        SharedPreferences pref = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(PREFRENCES_USER);
        editor.commit();
    }





}
