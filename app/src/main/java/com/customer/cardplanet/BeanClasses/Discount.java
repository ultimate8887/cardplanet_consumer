package com.customer.cardplanet.BeanClasses;

import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Discount {




    private String id;
    private String discount;
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public static ArrayList<Discount> parseDiscountArrayyy(JSONArray jsonArray) {
        ArrayList<Discount> list = new ArrayList<Discount>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Discount p = parseDiscountObject(jsonArray.getJSONObject(i));

                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }

    private static Discount parseDiscountObject(JSONObject jsonObject) {
        Discount casteObj = new Discount();
        try {
            if (jsonObject.has("id")) {
                casteObj.setId(jsonObject.getString("id"));

            }
            if (jsonObject.has("discount") && !jsonObject.getString("discount").isEmpty() && !jsonObject.getString("discount").equalsIgnoreCase("null")) {
                casteObj.setDiscount(jsonObject.getString("discount"));
            }
            if (jsonObject.has("image") && !jsonObject.getString("image").isEmpty() && !jsonObject.getString("image").equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString("image"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }

}
