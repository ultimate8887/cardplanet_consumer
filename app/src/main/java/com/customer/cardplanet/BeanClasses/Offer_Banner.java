package com.customer.cardplanet.BeanClasses;

import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Offer_Banner {

    /**
     * id : 26
     * title : India vs England: Rahane, Root welcome return of fans
     * description : Never mind the artificial chants and crowd noises manufactured on TV, there is no substitute for the presence of actual spectators at a sports stadium. Just ask the cricket fans in Chennai, who have been forming long queues over the last two days in stifling heat and humidity to collect a physical copy of their tickets for the second Test.
     * image : imgnews/news.png
     * date : 2021-04-05
     */

    private String id;
    private String title;
    private String c_status;
    private String description;
    private String image;
    private String date;

    public static ArrayList<Offer_Banner> parseOffer_BannerArrayyy(JSONArray jsonArray) {
        ArrayList<Offer_Banner> list = new ArrayList<Offer_Banner>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                Offer_Banner p = parseOffer_BannerObject(jsonArray.getJSONObject(i));

                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }

    private static Offer_Banner parseOffer_BannerObject(JSONObject jsonObject) {
        Offer_Banner casteObj = new Offer_Banner();
        try {
            if (jsonObject.has("id")) {
                casteObj.setId(jsonObject.getString("id"));

            }
            if (jsonObject.has("description") && !jsonObject.getString("description").isEmpty() && !jsonObject.getString("description").equalsIgnoreCase("null")) {
                casteObj.setDescription(jsonObject.getString("description"));
            }

            if (jsonObject.has("title") && !jsonObject.getString("title").isEmpty() && !jsonObject.getString("title").equalsIgnoreCase("null")) {
                casteObj.setTitle(jsonObject.getString("title"));
            }
            if (jsonObject.has("c_status") && !jsonObject.getString("c_status").isEmpty() && !jsonObject.getString("c_status").equalsIgnoreCase("null")) {
                casteObj.setC_status(jsonObject.getString("c_status"));
            }
            if (jsonObject.has("image") && !jsonObject.getString("image").isEmpty() && !jsonObject.getString("image").equalsIgnoreCase("null")) {
                casteObj.setImage(Constants.getImageBaseURL()+jsonObject.getString("image"));
            }

            if (jsonObject.has("date") && !jsonObject.getString("date").isEmpty() && !jsonObject.getString("date").equalsIgnoreCase("null")) {
                casteObj.setDate(Utils.dateFormat(jsonObject.getString("date")));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return casteObj;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getC_status() {
        return c_status;
    }

    public void setC_status(String c_status) {
        this.c_status = c_status;
    }



}
