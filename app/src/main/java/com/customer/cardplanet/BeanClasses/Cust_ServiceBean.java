package com.customer.cardplanet.BeanClasses;



import com.customer.cardplanet.Utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Cust_ServiceBean {

    private static String ID = "id";

    private static String CAT_ID = "cate_id";
    private static String SUBCAT_ID = "subcate_id";
    private static String CHILDCAT_ID = "childcate_id";
    private static String CAT_NAME = "cate_name";
    private static String SUBCAT_NAME = "subcate_name";
    private static String CHILDCAT_NAME = "childcate_name";
    private static String NAME = "name";
    private static String ADD = "address";
    private static String CITY = "city";
    private static String STATE = "state";
    private static String IMAGE = "image";
    private static String PIN = "pincode";
    private static String OWNER_NAME = "owner_name";
    private static String DISCOUNT = "discount";
    private static String COUNT = "rowcount";
    private static String PID = "pid";
    private static String CID ="cid";
    private static String G_DATE ="g_date";
    private static String R_DATE ="r_date";
    private static String P_DISCOUNT ="partner_discount";

    private static String CASHBACK = "cashback";
    private static String REDEEM ="redeem_voucher";
    private static String BILL_AMOUNT ="billing_amount";
    private static String REMARKS ="remarks";
    private static String S_TPYE ="service_type";

    private static String G_TOTAL = "grand_total";
    private static String SERVICEhOLDER_NAME ="serviceholder_name";
    private static String V_STATUS ="voucher_status";
    private static String V_NO ="voucher_no";
    /**
     * id : 7
     * pid : 3
     * cid : 1
     * cate_id : 7
     * subcate_id : 120
     * childcate_id : 2
     * cate_name : Education
     * subcate_name : Others
     * childcate_name : Others
     * name : Marble
     * address : garha road
     * city : Bazar Shahidan
     * state : Punjab
     * pincode : 144001
     * image : images/hospitals/1618652738IMG_0217.JPG.jpg
     * discount : 16
     * owner_name : aman chudhary
     * g_date : 2021-04-20
     * r_date : 0000-00-00
     * partner_discount : 16
     * cashback : 274
     * redeem_voucher :
     * billing_amount : 2000
     * remarks : ok
     * service_type : Others
     * grand_total : 1712
     * serviceholder_name : Shital
     * voucher_status : voucher
     * voucher_no : shita0854
     */

    private String id;
    private String pid;
    private String cid;
    private String cate_id;
    private String subcate_id;
    private String childcate_id;
    private String cate_name;
    private String subcate_name;
    private String childcate_name;
    private String name;
    private String address;
    private String city;
    private String state;
    private String pincode;
    private String image;
    private String discount;
    private String owner_name;
    private String g_date;
    private String r_date;
    private String partner_discount;
    private String cashback;
    private String redeem_voucher;
    private String billing_amount;
    private String remarks;
    private String service_type;
    private String grand_total;
    private String serviceholder_name;
    private String voucher_status;
    private String voucher_no;

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    private String rowcount;

    public static ArrayList<Cust_ServiceBean> parseCust_ServiceBeanArray(JSONArray arrayObj) {
        ArrayList list = new ArrayList<Cust_ServiceBean>();
        try {
            for (int i = 0; i < arrayObj.length(); i++) {
                Cust_ServiceBean p = parseCust_ServiceBean(arrayObj.getJSONObject(i));
                if (p != null) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private static Cust_ServiceBean parseCust_ServiceBean(JSONObject jsonObject) {
        Cust_ServiceBean user = new Cust_ServiceBean();
        try {

        if (jsonObject.has(ID)) {
            user.setId(jsonObject.getString(ID));
        }



        if (jsonObject.has(V_STATUS) && !jsonObject.getString(V_STATUS).isEmpty() && !jsonObject.getString(V_STATUS).equalsIgnoreCase("null")) {
            user.setVoucher_status(jsonObject.getString(V_STATUS));
        }




        if (jsonObject.has(PID) && !jsonObject.getString(PID).isEmpty() && !jsonObject.getString(PID).equalsIgnoreCase("null")) {
            user.setPid(jsonObject.getString(PID));
        }


            if (jsonObject.has(SUBCAT_ID) && !jsonObject.getString(SUBCAT_ID).isEmpty() && !jsonObject.getString(SUBCAT_ID).equalsIgnoreCase("null")) {
                user.setSubcate_id(jsonObject.getString(SUBCAT_ID));
            }
            if (jsonObject.has(CAT_ID) && !jsonObject.getString(CAT_ID).isEmpty() && !jsonObject.getString(CAT_ID).equalsIgnoreCase("null")) {
                user.setCate_id(jsonObject.getString(CAT_ID));
            }
            if (jsonObject.has(CHILDCAT_ID) && !jsonObject.getString(CHILDCAT_ID).isEmpty() && !jsonObject.getString(CHILDCAT_ID).equalsIgnoreCase("null")) {
                user.setChildcate_id(jsonObject.getString(CHILDCAT_ID));
            }
            if (jsonObject.has(CAT_NAME) && !jsonObject.getString(CAT_NAME).isEmpty() && !jsonObject.getString(CAT_NAME).equalsIgnoreCase("null")) {
                user.setCate_name(jsonObject.getString(CAT_NAME));
            }
            if (jsonObject.has(SUBCAT_NAME) && !jsonObject.getString(SUBCAT_NAME).isEmpty() && !jsonObject.getString(SUBCAT_NAME).equalsIgnoreCase("null")) {
                user.setSubcate_name(jsonObject.getString(SUBCAT_NAME));
            }
            if (jsonObject.has(CHILDCAT_NAME) && !jsonObject.getString(CHILDCAT_NAME).isEmpty() && !jsonObject.getString(CHILDCAT_NAME).equalsIgnoreCase("null")) {
                user.setChildcate_name(jsonObject.getString(CHILDCAT_NAME));
            }
            if (jsonObject.has(NAME) && !jsonObject.getString(NAME).isEmpty() && !jsonObject.getString(NAME).equalsIgnoreCase("null")) {
                user.setName(jsonObject.getString(NAME));
            }
            if (jsonObject.has(IMAGE) && !jsonObject.getString(IMAGE).isEmpty() && !jsonObject.getString(IMAGE).equalsIgnoreCase("null")) {
                user.setImage(Constants.getImageBaseURL() + jsonObject.getString(IMAGE));
            }

            if (jsonObject.has(ADD) && !jsonObject.getString(ADD).isEmpty() && !jsonObject.getString(ADD).equalsIgnoreCase("null")) {
                user.setAddress(jsonObject.getString(ADD));
            }

            if (jsonObject.has(DISCOUNT) && !jsonObject.getString(DISCOUNT).isEmpty() && !jsonObject.getString(DISCOUNT).equalsIgnoreCase("null")) {
                user.setDiscount(jsonObject.getString(DISCOUNT));
            }

            if (jsonObject.has(COUNT) && !jsonObject.getString(COUNT).isEmpty() && !jsonObject.getString(COUNT).equalsIgnoreCase("null")) {
                user.setRowcount(jsonObject.getString(COUNT));
            }

            if (jsonObject.has(CITY) && !jsonObject.getString(CITY).isEmpty() && !jsonObject.getString(CITY).equalsIgnoreCase("null")) {
                user.setCity(jsonObject.getString(CITY));
            }
            if (jsonObject.has(STATE) && !jsonObject.getString(STATE).isEmpty() && !jsonObject.getString(STATE).equalsIgnoreCase("null")) {
                user.setState(jsonObject.getString(STATE));
            }
            if (jsonObject.has(PIN) && !jsonObject.getString(PIN).isEmpty() && !jsonObject.getString(PIN).equalsIgnoreCase("null")) {
                user.setPincode(jsonObject.getString(PIN));
            }
            if (jsonObject.has(OWNER_NAME) && !jsonObject.getString(OWNER_NAME).isEmpty() && !jsonObject.getString(OWNER_NAME).equalsIgnoreCase("null")) {
                user.setOwner_name(jsonObject.getString(OWNER_NAME));
            }


        if (jsonObject.has(G_DATE) && !jsonObject.getString(G_DATE).isEmpty() && !jsonObject.getString(G_DATE).equalsIgnoreCase("null")) {
            user.setG_date(jsonObject.getString(G_DATE));
        }
        if (jsonObject.has(R_DATE) && !jsonObject.getString(R_DATE).isEmpty() && !jsonObject.getString(R_DATE).equalsIgnoreCase("null")) {
            user.setR_date(jsonObject.getString(R_DATE));
        }

        if (jsonObject.has(CID) && !jsonObject.getString(CID).isEmpty() && !jsonObject.getString(CID).equalsIgnoreCase("null")) {
            user.setCid(jsonObject.getString(CID));
        }
        if (jsonObject.has(P_DISCOUNT) && !jsonObject.getString(P_DISCOUNT).isEmpty() && !jsonObject.getString(P_DISCOUNT).equalsIgnoreCase("null")) {
            user.setPartner_discount(jsonObject.getString(P_DISCOUNT));
        }


        if (jsonObject.has(CASHBACK) && !jsonObject.getString(CASHBACK).isEmpty() && !jsonObject.getString(CASHBACK).equalsIgnoreCase("null")) {
            user.setCashback(jsonObject.getString(CASHBACK));
        }

        if (jsonObject.has(REDEEM) && !jsonObject.getString(REDEEM).isEmpty() && !jsonObject.getString(REDEEM).equalsIgnoreCase("null")) {
            user.setRedeem_voucher(jsonObject.getString(REDEEM));
        }

        if (jsonObject.has(BILL_AMOUNT) && !jsonObject.getString(BILL_AMOUNT).isEmpty() && !jsonObject.getString(BILL_AMOUNT).equalsIgnoreCase("null")) {
            user.setBilling_amount(jsonObject.getString(BILL_AMOUNT));
        }

        if (jsonObject.has(S_TPYE) && !jsonObject.getString(S_TPYE).isEmpty() && !jsonObject.getString(S_TPYE).equalsIgnoreCase("null")) {
            user.setService_type(jsonObject.getString(S_TPYE));
        }

        if (jsonObject.has(REMARKS) && !jsonObject.getString(REMARKS).isEmpty() && !jsonObject.getString(REMARKS).equalsIgnoreCase("null")) {
            user.setRemarks(jsonObject.getString(REMARKS));
        }
        if (jsonObject.has(G_TOTAL) && !jsonObject.getString(G_TOTAL).isEmpty() && !jsonObject.getString(G_TOTAL).equalsIgnoreCase("null")) {
            user.setGrand_total(jsonObject.getString(G_TOTAL));
        }
        if (jsonObject.has(SERVICEhOLDER_NAME) && !jsonObject.getString(SERVICEhOLDER_NAME).isEmpty() && !jsonObject.getString(SERVICEhOLDER_NAME).equalsIgnoreCase("null")) {
            user.setServiceholder_name(jsonObject.getString(SERVICEhOLDER_NAME));
        }
        if (jsonObject.has(V_NO) && !jsonObject.getString(V_NO).isEmpty() && !jsonObject.getString(V_NO).equalsIgnoreCase("null")) {
            user.setVoucher_no(jsonObject.getString(V_NO));
        }


    } catch (
    JSONException e) {
        e.printStackTrace();
    }
     return user;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCate_id() {
        return cate_id;
    }

    public void setCate_id(String cate_id) {
        this.cate_id = cate_id;
    }

    public String getSubcate_id() {
        return subcate_id;
    }

    public void setSubcate_id(String subcate_id) {
        this.subcate_id = subcate_id;
    }

    public String getChildcate_id() {
        return childcate_id;
    }

    public void setChildcate_id(String childcate_id) {
        this.childcate_id = childcate_id;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public String getSubcate_name() {
        return subcate_name;
    }

    public void setSubcate_name(String subcate_name) {
        this.subcate_name = subcate_name;
    }

    public String getChildcate_name() {
        return childcate_name;
    }

    public void setChildcate_name(String childcate_name) {
        this.childcate_name = childcate_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getG_date() {
        return g_date;
    }

    public void setG_date(String g_date) {
        this.g_date = g_date;
    }

    public String getR_date() {
        return r_date;
    }

    public void setR_date(String r_date) {
        this.r_date = r_date;
    }

    public String getPartner_discount() {
        return partner_discount;
    }

    public void setPartner_discount(String partner_discount) {
        this.partner_discount = partner_discount;
    }

    public String getCashback() {
        return cashback;
    }

    public void setCashback(String cashback) {
        this.cashback = cashback;
    }

    public String getRedeem_voucher() {
        return redeem_voucher;
    }

    public void setRedeem_voucher(String redeem_voucher) {
        this.redeem_voucher = redeem_voucher;
    }

    public String getBilling_amount() {
        return billing_amount;
    }

    public void setBilling_amount(String billing_amount) {
        this.billing_amount = billing_amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getServiceholder_name() {
        return serviceholder_name;
    }

    public void setServiceholder_name(String serviceholder_name) {
        this.serviceholder_name = serviceholder_name;
    }

    public String getVoucher_status() {
        return voucher_status;
    }

    public void setVoucher_status(String voucher_status) {
        this.voucher_status = voucher_status;
    }

    public String getVoucher_no() {
        return voucher_no;
    }

    public void setVoucher_no(String voucher_no) {
        this.voucher_no = voucher_no;
    }
}
