package com.customer.cardplanet.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.airbnb.lottie.LottieAnimationView;
import com.customer.cardplanet.BeanClasses.BoardingData;
import com.customer.cardplanet.R;

import java.util.List;

public class OnBoardingAdapter extends PagerAdapter {

    private Context context;
    List<BoardingData> boardingData;

    public OnBoardingAdapter(Context context, List<BoardingData> boardingData) {
        this.context = context;
        this.boardingData = boardingData;
    }

    @Override
    public int getCount() {
        return boardingData.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view= LayoutInflater.from(context).inflate(R.layout.boarding_item_view,null);
        LottieAnimationView image;
        TextView title,desc;

        image=(LottieAnimationView) view.findViewById(R.id.image);
        title=(TextView) view.findViewById(R.id.title);
        desc=(TextView) view.findViewById(R.id.desc);
        //  setting up data......
        image.setAnimation(boardingData.get(position).getImage());
        //  image.setImageResource(boardingData.get(position).getImage());
        image.loop(true);
        title.setText(boardingData.get(position).getTittle());
        desc.setText(boardingData.get(position).getDesc());
        //  setting up container......
        container.addView(view);

        return view;
    }
}
