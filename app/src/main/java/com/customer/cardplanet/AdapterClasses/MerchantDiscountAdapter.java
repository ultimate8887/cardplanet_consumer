package com.customer.cardplanet.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MerchantDiscountAdapter extends RecyclerView.Adapter<MerchantDiscountAdapter.ViewHolder> implements Filterable {

    Context context;
    ArrayList<MerchantBean> merchantBeans,arrayList;
    private ProdMethodCallBack prodMethodCallBack;
    private Animation animation;
    DiscountFilter filter;

    public MerchantDiscountAdapter(ArrayList<MerchantBean> merchantBeans, Context context, ProdMethodCallBack prodMethodCallBack) {
        this.merchantBeans=merchantBeans;
        this.context=context;
        this.arrayList=merchantBeans;
        this.prodMethodCallBack=prodMethodCallBack;
    }

    public void setMerchantBeans(ArrayList<MerchantBean> merchantBeans) {
        this.merchantBeans = merchantBeans;
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new DiscountFilter(merchantBeans,this);
        }
        return filter;
    }

    public interface ProdMethodCallBack{
        void prodMethod(MerchantBean productBean);
        void prodAddCartMethod(MerchantBean productBean);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_img)
        ImageView product_img;

        @BindView(R.id.brand_name)
        TextView brand_name;
        @BindView(R.id.discount)
        TextView discount;
        @BindView(R.id.category_name)
        TextView category_name;

        @BindView(R.id.product_short_desc)
        TextView product_short_desc;

        @BindView(R.id.linearlayout)
        LinearLayout linearlayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @NonNull
    @Override
    public MerchantDiscountAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_prod_adapter, parent, false);
        MerchantDiscountAdapter.ViewHolder viewHolder= new MerchantDiscountAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantDiscountAdapter.ViewHolder holder, int position) {

        MerchantBean data= merchantBeans.get(position);
//
//        if (data.getName() != null) {
//            holder.product_name.setText(data.getName()+" ("+data.getCity()+")");
//        }else {
//            holder.product_name.setText("Not Mentioned");
//        }

        if (data.getName() != null) {
            holder.category_name.setText(data.getName());
        }else {
            holder.category_name.setText("Not Mentioned");
        }
        if (data.getSubcate_name() != null) {
            holder.brand_name.setText(data.getCate_name()+"("+data.getSubcate_name()+")");
        }else {
            holder.brand_name.setText("Not Mentioned");
        }
        if (data.getAddress() != null) {
            holder.product_short_desc.setText(data.getAddress()+", "+data.getCity()+", "+data.getState()+", "+data.getPincode());
        }else if(data.getCity() != null){
            holder.product_short_desc.setText(data.getCity()+", "+data.getState()+", "+data.getPincode());
        }else{
            holder.product_short_desc.setText("Not Mentioned");
        }

        if (data.getDiscount() != null) {
            holder.discount.setText(data.getDiscount() + "%" + " Off the Total Bill");
        }else {
            holder.discount.setText("Not Mentioned");
        }

        if (data.getImage() != null) {
            Picasso.with(context).load(data.getImage()).placeholder(context.getResources().getDrawable(R.drawable.categories)).into(holder.product_img);
        }
        //  Log.d("image",""+data.getCateImage());


        holder.linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                holder.linearlayout.startAnimation(animation);
                prodMethodCallBack.prodMethod(merchantBeans.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return merchantBeans.size();
    }
}
