package com.customer.cardplanet.AdapterClasses;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.customer.cardplanet.BeanClasses.Merchant_Category;
import com.customer.cardplanet.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CateViewAdapter extends RecyclerView.Adapter<CateViewAdapter.ViewHolder> {

    Context context;
    ArrayList<Merchant_Category> subCateList;
    private CateMethodCallBack cateMethodCallBack;
    private Animation animation;

    public CateViewAdapter(ArrayList<Merchant_Category> subCateList, Context context, CateMethodCallBack cateMethodCallBack) {
        this.subCateList=subCateList;
        this.context=context;
        this.cateMethodCallBack=cateMethodCallBack;
    }

    public void setSubCateList(ArrayList<Merchant_Category> subCateList) {
        this.subCateList = subCateList;
    }

    public interface CateMethodCallBack{
        void cateMethod(Merchant_Category subCateBean);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgCate)
        ImageView imgCate;
        @BindView(R.id.cateName)
        TextView cateName;
        @BindView(R.id.linearlayout)
        LinearLayout linearlayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_view_adapter, parent, false);
        ViewHolder viewHolder= new CateViewAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Merchant_Category data = subCateList.get(position);

//        Log.e("code",data.getText_color());



        if (data.getName() != null) {
            holder.cateName.setText(data.getName());
        }else {
            holder.cateName.setText("not Mention");
        }
        if (data.getImage() != null) {
            Picasso.with(context).load(data.getImage()).placeholder(context.getResources().getDrawable(R.drawable.categories)).into(holder.imgCate);
        }
        //  Log.d("image",""+data.getCateImage());

        holder.linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                holder.linearlayout.startAnimation(animation);
                cateMethodCallBack.cateMethod(subCateList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return subCateList.size();
    }

}
