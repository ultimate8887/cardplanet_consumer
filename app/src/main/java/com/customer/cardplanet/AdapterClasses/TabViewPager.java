package com.customer.cardplanet.AdapterClasses;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.customer.cardplanet.Fragments.AllHistoryFragment;
import com.customer.cardplanet.Fragments.RedeemFragment;
import com.customer.cardplanet.Fragments.VoucherFragment;

public class TabViewPager  extends FragmentStatePagerAdapter {
    public TabViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new VoucherFragment();
            case 1:
                return new RedeemFragment();
            case 2:
                return new AllHistoryFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Voucher";
            case 1:
                return "Redeem";
            case 2:
                return "All History";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
