package com.customer.cardplanet.AdapterClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.customer.cardplanet.BeanClasses.Discount;
import com.customer.cardplanet.BeanClasses.Merchant_Category;
import com.customer.cardplanet.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Discount_Adapter extends RecyclerView.Adapter<Discount_Adapter.ViewHolder> {

    Context context;
    ArrayList<Discount> discounts;
    private DiscountMethodCallBack cateMethodCallBack;
    private Animation animation;

    public Discount_Adapter(ArrayList<Discount> discounts, Context context, DiscountMethodCallBack cateMethodCallBack) {
        this.discounts=discounts;
        this.context=context;
        this.cateMethodCallBack=cateMethodCallBack;
    }

    public void setDiscounts(ArrayList<Discount> discounts) {
        this.discounts = discounts;
    }

    public interface DiscountMethodCallBack{
        void discountMethod(Discount discount);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgCate)
        ImageView imgCate;
        @BindView(R.id.linearlayout)
        LinearLayout linearlayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.discount_view_adapter, parent, false);
        ViewHolder viewHolder= new Discount_Adapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Discount data = discounts.get(position);

        //        Log.e("code",data.getText_color());




        if (data.getImage() != null) {
            Picasso.with(context).load(data.getImage()).placeholder(context.getResources().getDrawable(R.drawable.e)).into(holder.imgCate);
        }
        //  Log.d("image",""+data.getCateImage());

        holder.linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                holder.linearlayout.startAnimation(animation);
                cateMethodCallBack.discountMethod(discounts.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return discounts.size();
    }
}
