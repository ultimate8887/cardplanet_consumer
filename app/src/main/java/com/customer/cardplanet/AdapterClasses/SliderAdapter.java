package com.customer.cardplanet.AdapterClasses;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.customer.cardplanet.BeanClasses.NewsData;
import com.customer.cardplanet.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterViewHolder> {

    // list for storing urls of images.
    ArrayList<NewsData> mSliderItems;
    Context mContext;

    // Constructor
    public SliderAdapter( ArrayList<NewsData> mSliderItems,Context mContext) {
        this.mSliderItems = mSliderItems;
        this.mContext = mContext;
    }



    // We are inflating the slider_layout
    // inside on Create View Holder method.
    @Override
    public SliderAdapterViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterViewHolder(inflate);
    }


    public void setmSliderItems(ArrayList<NewsData> mSliderItems) {
        this.mSliderItems = mSliderItems;
    }

    // Inside on bind view holder we will
    // set data to item of Slider View.
    @Override
    public void onBindViewHolder(SliderAdapterViewHolder viewHolder, final int position) {

        final NewsData sliderItem = mSliderItems.get(position);

        // Glide is use to load image
        // from url in your imageview.
        Glide.with(viewHolder.itemView)
                .load(sliderItem.getImage())
                .fitCenter()
                .into(viewHolder.imageViewBackground);

//        viewHolder.title.setText(sliderItem.getTitle());
//        viewHolder.disc.setText(sliderItem.getDescription());
//        viewHolder.disc.setMovementMethod(new ScrollingMovementMethod());

//        Glide.with(viewHolder.title)
//                .load(sliderItem.getTitle())
//                .fitCenter()
//                .into(viewHolder.imageViewBackground);
    }



    // this method will return
    // the count of our list.
    @Override
    public int getCount() {
        return mSliderItems.size();
    }

    static class SliderAdapterViewHolder extends SliderViewAdapter.ViewHolder {
        // Adapter class for initializing
        // the views of our slider view.
        View itemView;
        ImageView imageViewBackground;


        public SliderAdapterViewHolder(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.myimage);
//            title = itemView.findViewById(R.id.title);
//            disc = itemView.findViewById(R.id.disc);
            this.itemView = itemView;
        }
    }
}
