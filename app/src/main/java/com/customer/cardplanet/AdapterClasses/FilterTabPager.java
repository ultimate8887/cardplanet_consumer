package com.customer.cardplanet.AdapterClasses;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.customer.cardplanet.Fragments.AllHistoryFragment;
import com.customer.cardplanet.Fragments.CategoryWiseFragment;
import com.customer.cardplanet.Fragments.CityWiseFragment;
import com.customer.cardplanet.Fragments.DiscountWiseFragment;
import com.customer.cardplanet.Fragments.RedeemFragment;
import com.customer.cardplanet.Fragments.VoucherFragment;

public class FilterTabPager extends FragmentStatePagerAdapter {
    public FilterTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DiscountWiseFragment();
            case 1:
                return new CityWiseFragment();
            case 2:
                return new CategoryWiseFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Discounts";
            case 1:
                return "City";
            case 2:
                return "Category";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
