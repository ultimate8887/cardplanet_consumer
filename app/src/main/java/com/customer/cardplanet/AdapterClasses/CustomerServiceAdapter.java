package com.customer.cardplanet.AdapterClasses;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.Utils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerServiceAdapter extends RecyclerView.Adapter<CustomerServiceAdapter.ViewHolder> implements Filterable {

    Context context;
    ArrayList<Cust_ServiceBean> merchantBeans,arrayList;
    private ProdMethodCallBack prodMethodCallBack;
    private Animation animation;
    CustomerFilter filter;

    public CustomerServiceAdapter(ArrayList<Cust_ServiceBean> merchantBeans, Context context, ProdMethodCallBack prodMethodCallBack) {
        this.merchantBeans=merchantBeans;
        this.context=context;
        this.arrayList=merchantBeans;
        this.prodMethodCallBack=prodMethodCallBack;
    }

    public void setMerchantBeans(ArrayList<Cust_ServiceBean> merchantBeans) {
        this.merchantBeans = merchantBeans;
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomerFilter(merchantBeans,this);
        }
        return filter;
    }

    public interface ProdMethodCallBack{
        void prodMethod(Cust_ServiceBean productBean);
        void prodAddCartMethod(Cust_ServiceBean productBean);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_img)
        CircularImageView product_img;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.gift)
        TextView gift;
        @BindView(R.id.date)
        TextView date;
//        @BindView(R.id.price)
//        ImageView price;
//        @BindView(R.id.voucher)
//        ImageView voucher;
        @BindView(R.id.tagline)
        TextView tagline;
        @BindView(R.id.price)
        TextView price;
//        @BindView(R.id.category_name)
//        TextView category_name;
//        @BindView(R.id.product_short_desc)
//        TextView product_desc;
//        @BindView(R.id.selling_price)
//        ImageView view;
        @BindView(R.id.linearlayout)
        RelativeLayout linearlayout;
//        @BindView(R.id.product_add)
//        Button product_add;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_layout, parent, false);
        ViewHolder viewHolder= new CustomerServiceAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Cust_ServiceBean data= merchantBeans.get(position);

        if (data.getName() != null) {
            holder.name.setText(data.getName()+" ("+data.getCity()+")");
        }else {
            holder.name.setText("Not Mentioned");
        }

//        if (data.getCate_name() != null) {
//            holder.category_name.setText(data.getCate_name()+"("+data.getSubcate_name()+")");
//        }else {
//            holder.category_name.setText("Not Mentioned");
//        }
        if (data.getGrand_total() != null) {
            holder.price.setText(data.getGrand_total()+".00");
        }else {
            holder.price.setText("Not Mentioned");
        }
//        if (data.getAddress() != null) {
//            holder.product_desc.setText(data.getAddress()+", "+data.getCity()+","+", "+data.getState()+","+data.getPincode());
//        }else {
//            holder.product_desc.setText("Not Mentioned");
//        }

//        if (data.getDiscount() != null) {
//            holder.discount.setText(data.getDiscount() + "%");
//        }else {
//            holder.discount.setText("Not Mentioned");
//        }

        if (data.getImage() != null) {
            Picasso.with(context).load(data.getImage()).placeholder(context.getResources().getDrawable(R.drawable.person_1)).into(holder.product_img);
        }

        if (data.getVoucher_status().equalsIgnoreCase("voucher")){
//            holder.redeem.setVisibility(View.GONE);
//            holder.voucher.setVisibility(View.VISIBLE);
            holder.date.setText(data.getG_date());
            holder.tagline.setText("Voucher \n Credited to");
            holder.tagline.setTextColor(Color.parseColor("#1C8B3B"));
            holder.gift.setText("Gift Voucher"+" (Rs."+data.getCashback()+".00)");
//            if (data.getCashback() != null) {
//                holder.amount.setText(data.getCashback()+".00");
//            }else {
//                holder.amount.setText("0");
//            }

        }else{
//            holder.voucher.setVisibility(View.GONE);
//            holder.redeem.setVisibility(View.VISIBLE);
            holder.date.setText(data.getR_date());
            holder.tagline.setText("Voucher \n Debited from");
            holder.tagline.setTextColor(Color.parseColor("#f85164"));
            holder.gift.setText("Redeem Gift Voucher"+" (Rs."+data.getRedeem_voucher()+".00)");
//            if (data.getRedeem_voucher() != null) {
//                holder.amount.setText(data.getRedeem_voucher()+".00");
//            }else {
//                holder.amount.setText("0");
//            }

//            if (data.getRedeem_voucher() != null) {
//                String title = getColoredSpanned("<b>" +"Redeem Amount- "+"</b>", "#5A5C59");
//                String Name = getColoredSpanned(data.getRedeem_voucher() , "#7D7D7D");
//                holder.card_details.setText(Html.fromHtml(title + " " + Name));
//            }else {
//                holder.card_details.setText("0");
//            }
        }



        //  Log.d("image",""+data.getCateImage());
//        holder.product_add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
//                holder.product_add.startAnimation(animation);
//                prodMethodCallBack.prodAddCartMethod(merchantBeans.get(position));
//            }
//        });

//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
//                holder.linearlayout.startAnimation(animation);
//                prodMethodCallBack.prodMethod(merchantBeans.get(position));
//            }
//        });

        holder.linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation = AnimationUtils.loadAnimation(context, R.anim.btn_blink_animation);
                holder.linearlayout.startAnimation(animation);
                prodMethodCallBack.prodMethod(merchantBeans.get(position));
            }
        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return merchantBeans.size();
    }
}
