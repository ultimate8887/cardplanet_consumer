package com.customer.cardplanet.AdapterClasses;

public class Category {

    public String getCategory (float result) {
        String category;
        if (result < 15) {
            category = "Very Severely Underweight";
        } else if (result >=15 && result <= 16) {
            category = "Severely Underweight";
        } else if (result >16 && result <= 18.5) {
            category = "Underweight";
        } else if (result >18.5 && result <= 25) {
            category = "Normal (Healthy Weight)";
        } else if (result >25 && result <= 30) {
            category = "Overweight";
        } else if (result >30 && result <= 35) {
            category = "Moderately Obese";
        } else if (result >35 && result <= 40) {
            category = "Severely Obese";
        } else {
            category ="Very Severely Obese";
        }
        return category;
    }
}
