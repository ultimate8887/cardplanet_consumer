package com.customer.cardplanet.AdapterClasses;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.customer.cardplanet.Fragments.CategoryWiseFragment;
import com.customer.cardplanet.Fragments.CityWiseCateFragment;
import com.customer.cardplanet.Fragments.CityWiseFragment;
import com.customer.cardplanet.Fragments.DiscountCateWiseFragment;
import com.customer.cardplanet.Fragments.DiscountWiseFragment;

public class CateWiseViewPager extends FragmentStatePagerAdapter {
    public CateWiseViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DiscountCateWiseFragment();
            case 1:
                return new CityWiseCateFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Filter Discounts";
            case 1:
                return "Filter City";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
