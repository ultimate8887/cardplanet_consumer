package com.customer.cardplanet.AdapterClasses;

import android.widget.Filter;

import com.customer.cardplanet.BeanClasses.MerchantBean;

import java.util.ArrayList;

public class DiscountFilter extends Filter {
    MerchantDiscountAdapter adapter;
    ArrayList<MerchantBean> filterList;
    public DiscountFilter(ArrayList<MerchantBean> filterList, MerchantDiscountAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;
    }
    //FILTERING OCURS
    @Override
    protected Filter.FilterResults performFiltering(CharSequence constraint) {
        Filter.FilterResults results=new Filter.FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<MerchantBean> filteredPlayers=new ArrayList<>();
            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getDiscount().toUpperCase().contains(constraint) || filterList.get(i).getDiscount().toLowerCase().contains(constraint)  )
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }
    @Override
    protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
        adapter.merchantBeans= (ArrayList<MerchantBean>) results.values;
        //REFRESH
        adapter.notifyDataSetChanged();
    }
}
