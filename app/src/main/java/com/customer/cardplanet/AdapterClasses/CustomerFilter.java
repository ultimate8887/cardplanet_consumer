package com.customer.cardplanet.AdapterClasses;

import android.widget.Filter;

import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;

import java.util.ArrayList;

public class CustomerFilter extends Filter {
    CustomerServiceAdapter adapter;
    ArrayList<Cust_ServiceBean> filterList;
    public CustomerFilter(ArrayList<Cust_ServiceBean> filterList, CustomerServiceAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;
    }
    //FILTERING OCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Cust_ServiceBean> filteredPlayers=new ArrayList<>();
            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getCity().toUpperCase().contains(constraint) || filterList.get(i).getCity().toLowerCase().contains(constraint)  )
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.merchantBeans= (ArrayList<Cust_ServiceBean>) results.values;
        //REFRESH
        adapter.notifyDataSetChanged();
    }
}
