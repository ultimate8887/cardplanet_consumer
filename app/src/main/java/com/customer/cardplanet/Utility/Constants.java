package com.customer.cardplanet.Utility;

import androidx.multidex.BuildConfig;

public class Constants {
    public static final String PREF_NAME ="CardPlanetCustomerApp_pref";
    public static final String OPENED_STATUS ="opened";
    public static final String DEVICE_TOKEN ="device_token";
    public static final String LOGIN_URL ="customer_login.php";
    public static final String USER_DATA ="customer_data";
    public static final String EMAIL ="email";
    public static final String PASSWORD ="password";
    public static final String NEWS_URL ="news.php";
    public static final String PRODUCT_CATE_URL ="cate_wise.php";
    public static final String MERCHANT_URL ="all_merchant.php";
    public static final String OFFER_URL ="offer_banner.php";
    public static final String UPDATE_OFFER_URL ="update_banner.php";
    public static final String SAVING_URL ="customer_saving.php";
    public static final String CUST_URL ="customer_service.php";
    public static final String DISCOUNT_URL = "discount_img.php";
    public static final String CUST_BILLING_URL ="searchdetails.php";
    public static final String ADD_SERVICE_URL ="addservice.php";
    public static final String UPDATE_DATA_URL ="update_user_data.php";
    public static final int READ_PERMISSION =1;

    private static final String API_BASE_EXT ="include/customer_api/";
//    private static String Prod_Base_Url="https://cardplanet.in/";
//    private static String Dev_Base_Url="https://cardplanet.in/";
    private static String Prod_Base_Url="http://cardplanet.xyzultimatesolution.com/";
    private static String Dev_Base_Url="http://cardplanet.xyzultimatesolution.com/";


    public static String getBaseURL() {
        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
            return Prod_Base_Url+API_BASE_EXT;
        } else {
            return Dev_Base_Url+API_BASE_EXT;

        }
    }
    public static String getImageBaseURL() {
        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
            return Prod_Base_Url;
        } else {
            return Dev_Base_Url;
        }
    }
}
