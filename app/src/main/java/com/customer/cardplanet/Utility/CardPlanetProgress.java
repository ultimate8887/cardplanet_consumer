package com.customer.cardplanet.Utility;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.customer.cardplanet.R;

public class CardPlanetProgress extends Dialog {
    private static ProgressDialog progressDialog;

    public CardPlanetProgress(@NonNull Context context) {
        super(context);

        WindowManager.LayoutParams params= getWindow().getAttributes();
        params.gravity= Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(params);
        setCancelable(false);
        setTitle(null);
        setOnCancelListener(null);
        View view= LayoutInflater.from(context).inflate(R.layout.progress_dialog, null);
        setContentView(view);

    }


//    public static void showProgressBar(Context context, String msg) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage(msg);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//    }
//
//    public static void cancelProgressBar() {
//        if (progressDialog != null && progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
//    }
}
