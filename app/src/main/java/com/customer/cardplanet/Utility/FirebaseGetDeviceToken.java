package com.customer.cardplanet.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.customer.cardplanet.CardPlanetCustomerApp;
import com.google.firebase.iid.FirebaseInstanceId;


public class FirebaseGetDeviceToken {
    private static final String TAG = FirebaseGetDeviceToken.class.getName();
    static String deviceToken;


    public static String getDeviceToken(){
        deviceToken = FirebaseInstanceId.getInstance().getToken();
        saveInSharedPref(deviceToken);
        return(deviceToken);
    }

    public static void saveInSharedPref(String deviceToken){
        SharedPreferences preferences = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(Constants.DEVICE_TOKEN, deviceToken).commit();
    }
}
