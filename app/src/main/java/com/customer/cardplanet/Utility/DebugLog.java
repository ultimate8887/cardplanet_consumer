package com.customer.cardplanet.Utility;

import android.util.Log;

class DebugLog {
    public static void printLog(String tag, String message){
        Log.e(tag, message);
    }
}
