package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.AdapterClasses.CustomerServiceAdapter;
import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodaySavingActivity extends AppCompatActivity implements CustomerServiceAdapter.ProdMethodCallBack {

    private Animation animation;
    @BindView(R.id.noData)
    RelativeLayout noData;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.savings)
    TextView save;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<Cust_ServiceBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private CustomerServiceAdapter productAdapter;
    String v_type="",saving="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_saving);
        ButterKnife.bind(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);

        cardPlanetProgress=new CardPlanetProgress(this);


        if (getIntent().getExtras() != null){
            v_type=getIntent().getExtras().getString("v_type","");
           saving=getIntent().getExtras().getString("saving","");
        }
        save.setText(saving+".00");
        if (v_type.equalsIgnoreCase("today")){
            txtTitle.setText("Today Saving ");
        }else{
            txtTitle.setText("Total Saving ");
        }

        //set animation on adapter...
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(this, resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...
        layoutManager = new LinearLayoutManager(this);
        viewRecycleProduct.setLayoutManager(layoutManager);
        productAdapter = new CustomerServiceAdapter(merchantBeans, this,this);
        viewRecycleProduct.setAdapter(productAdapter);
    }
    @Override
    public void prodMethod(Cust_ServiceBean cust_serviceBean) {

        Gson gson = new Gson();
        String prod_data = gson.toJson(cust_serviceBean, Cust_ServiceBean.class);
        Intent intent = new Intent(this, MerchantDetails.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateShrink(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchMerchantList();
    }
    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        finish();
    }


    private void fetchMerchantList() {
        HashMap<String, String> params = new HashMap<>();
        //    Toast.makeText(getActivity(),"id "+ User.getCurrentUser().getId(),Toast.LENGTH_LONG).show();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("v_type", v_type);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CUST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = Cust_ServiceBean.parseCust_ServiceBeanArray(jsonObject.getJSONArray("service_data"));
                        productAdapter.setMerchantBeans(merchantBeans);
                        productAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleProduct.getAdapter().notifyDataSetChanged();
                        viewRecycleProduct.scheduleLayoutAnimation();
                        //-----------end------------
                        noData.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                    productAdapter.setMerchantBeans(merchantBeans);
                    productAdapter.notifyDataSetChanged();
                    noData.setVisibility(View.VISIBLE);

                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, this, params);
    }

    @Override
    public void prodAddCartMethod(Cust_ServiceBean productBean) {

    }
}