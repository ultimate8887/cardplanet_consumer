package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class UseFingerPrintActivity extends AppCompatActivity {


    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    private KeyStore keyStore;
    private Cipher cipher;
    private String KEY_NAME = "AndroidKey";

    private TextView mHeadingLabel;
    private ImageView mFingerprintImage;
    private TextView mParaLabel;
    private TextView btnProceed;
    private RelativeLayout pin_lyt,finger_lyt;

    TextView title;
    CircularImageView imageView;
    private Animation animation,animation1;
    CancellationSignal cancellationSignal;

    @SuppressLint("MissingPermission")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_code);

//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
  //      getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.long_fade_animation);

        imageView = (CircularImageView) findViewById(R.id.myimage);
        title=(TextView) findViewById(R.id.title);
        btnProceed=(TextView) findViewById(R.id.btnProceed);
        finger_lyt=(RelativeLayout) findViewById(R.id.finger_lyt);
        pin_lyt=(RelativeLayout) findViewById(R.id.pin_lyt);

        Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.person_1).into(imageView);


        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnProceed.startAnimation(animation);
                Intent intent = new Intent(UseFingerPrintActivity.this, UsePassPinActivity.class);
                startActivity(intent);

                cancellationSignal.cancel();

                finish();



               // Animatoo.animateZoom(UseFingerPrintActivity.this);

            }
        });

        title.setText("Hi, "+User.getCurrentUser().getName());

    //    mHeadingLabel = (TextView) findViewById(R.id.headingLabel);
        mFingerprintImage = (ImageView) findViewById(R.id.fingerprintImage);
        mParaLabel = (TextView) findViewById(R.id.paraLabel);


//
//        passcodeView.getLocalPasscode();
//
//       // Toast.makeText(getApplicationContext(),"Password"+User.getCurrentUser().getDevice_pin(),Toast.LENGTH_LONG).show();
//        passcodeView.setPasscodeLength(4)
//                .setLocalPasscode(User.getCurrentUser().getDevice_pin())
//                .setListener(new PasscodeView.PasscodeViewListener() {
//            @Override
//            public void onFail() {
//                Toast.makeText(getApplicationContext(),"Password is Wrong",Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onSuccess(String number) {
//                common_code();
//
//            }
//        });





        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if(!fingerprintManager.isHardwareDetected()){

                mParaLabel.setText("Fingerprint Scanner not detected in Device");
            //    Toast.makeText(getApplicationContext(),"Fingerprint Scanner not detected in Device",Toast.LENGTH_LONG).show();

            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED){

                mParaLabel.setText("Permission not granted to use Fingerprint Scanner");
            //    Toast.makeText(getApplicationContext(),"Permission not granted to use Fingerprint Scanner",Toast.LENGTH_LONG).show();

            } else if (!keyguardManager.isKeyguardSecure()){
             //   Toast.makeText(getApplicationContext(),"Add Lock to your Phone in Settings",Toast.LENGTH_LONG).show();
                mParaLabel.setText("Add Lock to your Phone in Settings");

            } else if (!fingerprintManager.hasEnrolledFingerprints()){
              //  Toast.makeText(getApplicationContext(),"You should add atleast 1 Fingerprint to use this Feature",Toast.LENGTH_LONG).show();
                mParaLabel.setText("You should add atleast 1 Fingerprint to use this Feature");

            } else {
             //   Toast.makeText(getApplicationContext(),"Place your Finger on Scanner to Access the App",Toast.LENGTH_LONG).show();
                mParaLabel.setText("Place your Finger on Scanner\n to Access the App.");

                generateKey();

                if (cipherInit()){

                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                    FingerprintHandler fingerprintHandler = new FingerprintHandler(this);
                    fingerprintHandler.startAuth(fingerprintManager, cryptoObject);



                }
            }

        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void generateKey() {

        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();

        } catch (KeyStoreException | IOException | CertificateException
                | NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | NoSuchProviderException e) {

            e.printStackTrace();

        }

    }




    @SuppressLint("NewApi")
    private class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

        private Context context;

        public FingerprintHandler(Context context) {

            this.context = context;

        }

        public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {

             cancellationSignal = new CancellationSignal();
            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);



        }





        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {

            this.update("There was an Auth Error. " + errString, false);

        }

        @Override
        public void onAuthenticationFailed() {

            this.update("Authentication Failed. ", false);

        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {

            this.update("Error: " + helpString, false);

        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

            this.update("You can now access the app.", true);

        }

        private void update(String s, boolean b) {

//            TextView paraLabel = (TextView) ((Activity) context).findViewById(R.id.paraLabel);
//            ImageView imageView = (ImageView) ((Activity) context).findViewById(R.id.fingerprintImage);

            mParaLabel.setText(s);

            if (b == false) {

                mParaLabel.setTextColor(ContextCompat.getColor(context, R.color.red));
                mFingerprintImage.setImageResource(R.drawable.wrong_fingerprint);
            } else {

                mParaLabel.setTextColor(ContextCompat.getColor(context, R.color.present));
                mFingerprintImage.setImageResource(R.drawable.correct_fingerprint);

                Intent intent = new Intent(UseFingerPrintActivity.this, FinalHomeActivity.class);
                startActivity(intent);
                finish();
                Animatoo.animateZoom(UseFingerPrintActivity.this);

            }

        }
    }

        @TargetApi(Build.VERSION_CODES.M)
        public boolean cipherInit() {
            try {
                cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                throw new RuntimeException("Failed to get Cipher", e);
            }


            try {

                keyStore.load(null);

                SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                        null);

                cipher.init(Cipher.ENCRYPT_MODE, key);

                return true;

            } catch (KeyPermanentlyInvalidatedException e) {
                return false;
            } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
                throw new RuntimeException("Failed to init Cipher", e);
            }

        }


    @Override
    protected void onPause() {
        super.onPause();

    }
}