package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.AdapterClasses.CustomerServiceAdapter;
import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MerchantDetailsWithransaction extends AppCompatActivity implements CustomerServiceAdapter.ProdMethodCallBack, OnMapReadyCallback {

//    @BindView(R.id.product_name)
//    TextView product_name;
    @BindView(R.id.brand_name)
    TextView brand_name;
    @BindView(R.id.discount)
    TextView discount;
    @BindView(R.id.category_name)
    TextView category_name;
    @BindView(R.id.cate_namess)
    TextView cate_namess;
    @BindView(R.id.product_short_desc)
    TextView product_desc;
    @BindView(R.id.contact)
    TextView contact;
    Animation animation;
    MerchantBean data;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Window window;
    @BindView(R.id.product_img)
    ImageView product_img;

    @BindView(R.id.viewall)
    TextView viewall;
    @BindView(R.id.noData)
    RelativeLayout noData;
    @BindView(R.id.recycle_lyt)
    RelativeLayout recycle_lyt;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<Cust_ServiceBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private CustomerServiceAdapter productAdapter;

    @BindView(R.id.call)
    TextView call;
    @BindView(R.id.open_google_map)
    TextView open_google_map;

    @BindView(R.id.view_img)
    ImageView view_img;
    @BindView(R.id.share)
    ImageView share;

// map view data
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Marker myMarker = null;
    double lat, lng;
    Handler handler = new Handler();
    Geocoder geo;
    List<Address> addresses = null;
    StringBuilder  str = null;
    LatLng latLng;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_details_withransaction);
        ButterKnife.bind(this);
        geo = new Geocoder(getApplicationContext(), Locale.getDefault());
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_location);
        mapFragment.getMapAsync(this);

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        // holder.product_add.startAnimation(animation);
        cardPlanetProgress=new CardPlanetProgress(this);
        transparentfullscreen();

        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("merchant_data")) {
            Gson gson = new Gson();
            data = gson.fromJson(intent_value.getString("merchant_data"), MerchantBean.class);
            setCustomerData();
        }


        //set animation on adapter...
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(this, resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...
        layoutManager = new LinearLayoutManager(this);
        viewRecycleProduct.setLayoutManager(layoutManager);
        productAdapter = new CustomerServiceAdapter(merchantBeans, this,this);
        viewRecycleProduct.setAdapter(productAdapter);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void transparentfullscreen() {

        Window window=this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        window.setStatusBarColor(Color.TRANSPARENT);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchMerchantList();
    }

    private void fetchMerchantList() {
        HashMap<String, String> params = new HashMap<>();
        //    Toast.makeText(getActivity(),"id "+ User.getCurrentUser().getId(),Toast.LENGTH_LONG).show();
        params.put("p_id", data.getId());
        params.put("user_id", User.getCurrentUser().getId());
        params.put("v_type", "m_history");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CUST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = Cust_ServiceBean.parseCust_ServiceBeanArray(jsonObject.getJSONArray("service_data"));
                        productAdapter.setMerchantBeans(merchantBeans);
                        productAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleProduct.getAdapter().notifyDataSetChanged();
                        viewRecycleProduct.scheduleLayoutAnimation();
                        //-----------end------------
                        noData.setVisibility(View.GONE);
                        viewRecycleProduct.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                    productAdapter.setMerchantBeans(merchantBeans);
                    productAdapter.notifyDataSetChanged();
                    noData.setVisibility(View.VISIBLE);
                    viewRecycleProduct.setVisibility(View.GONE);

                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, this, params);
    }


    private void setCustomerData() {
//                if (data.getName() != null) {
//            product_name.setText(data.getName()+" ("+data.getCity()+")");
//        }else {
//            product_name.setText("Not Mentioned");
//        }

     if (!data.getLatitude().equalsIgnoreCase("0") && !data.getLongitude().equalsIgnoreCase("0")) {
         lat = Double.parseDouble(data.getLatitude());
         lng= Double.parseDouble(data.getLongitude());
         Log.e("latlong",data.getLatitude()+ "   "+data.getLongitude());
     }else{
         open_google_map.setVisibility(View.GONE);
         //   Toast.makeText(getApplicationContext(),"Location Not Found",Toast.LENGTH_SHORT).show();
        }




        if (data.getCate_name() != null) {
            cate_namess.setText(data.getCate_name()+"("+data.getSubcate_name()+")");
        }else {
            cate_namess.setText("Not Mentioned");
        }
        if (data.getOwner_name() != null) {
            brand_name.setText(data.getOwner_name()+"");
        }else {
            brand_name.setText("Not Mentioned");
        }

        if (data.getContact() != null) {
            contact.setText(data.getContact()+"");
        }else {
            contact.setText("Not Mentioned");
        }


        if (data.getName() != null) {
            category_name.setText(data.getName()+"");
        }else {
            category_name.setText("Not Mentioned");
        }
        if (data.getAddress() != null) {
            product_desc.setText(data.getAddress()+", "+data.getCity()+","+", "+data.getState()+","+data.getPincode());
        }else {
            product_desc.setText("Not Mentioned");
        }

        if (data.getDiscount() != null) {
            discount.setText(data.getDiscount() + "%"+ " Off the Total Bill");
        }else {
            discount.setText("Not Mentioned");
        }

        if (data.getImage() != null) {
            Picasso.with(this).load(data.getImage()).placeholder(this.getResources().getDrawable(R.drawable.categories)).into(product_img);
        }
    }


    @OnClick(R.id.call)
    public void call() {
        call.startAnimation(animation);
        // startActivity(new Intent(getActivity(), AllMerchantList.class));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},1);
        } else {
            String number=data.getContact();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("tel:" + number));
            startActivity(intent);
          cardPlanetProgress.dismiss();
        }

        Animatoo.animateShrink(this);
    }

    @OnClick(R.id.open_google_map)
    public void open_google_map() {
        open_google_map.startAnimation(animation);
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", lat,lng);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
        Animatoo.animateShrink(this);
    }


    @OnClick(R.id.share)
    public void share() {
        share.startAnimation(animation);
        if (!data.getLatitude().equalsIgnoreCase("0") && !data.getLongitude().equalsIgnoreCase("0")) {
            lat = Double.parseDouble(data.getLatitude());
            lng= Double.parseDouble(data.getLongitude());
            Log.e("latlong",data.getLatitude()+ "   "+data.getLongitude());

            String uri = "https://www.google.com/maps/?q=" + lat + "," +lng ;
            String ShareSub =User.getCurrentUser().getName()+"("+User.getCurrentUser().getMobile()+")";
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            Log.e("urls",uri);
            Log.e("ShareSub",ShareSub);

            shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,  ShareSub+" \n"+uri);
            shareIntent.setType("text/plain");
            startActivity(Intent.createChooser(shareIntent,"Share with"));

        }else{
            open_google_map.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(),"Location Not Found",Toast.LENGTH_SHORT).show();
        }



    }

    @OnClick(R.id.view_img)
    public void view_img() {
        view_img.startAnimation(animation);
        Dialog dialogLog1 = new Dialog(this);
        dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(true);
        dialogLog1.setContentView(R.layout.imageview_dialog);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        PhotoView imageView = (PhotoView) dialogLog1.findViewById(R.id.myimage);

        //if (!image.equalsIgnoreCase("")) {
        Picasso.with(this).load(data.getImage()).placeholder(R.drawable.categories).into(imageView);
        //   Log.e("image",image);

        RelativeLayout btnNo = (RelativeLayout) dialogLog1.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog1.dismiss();
                finish();
            }
        });
        dialogLog1.show();

    }


    @OnClick(R.id.viewall)
    public void viewall() {
        viewall.startAnimation(animation);
        // startActivity(new Intent(getActivity(), AllMerchantList.class));

        Intent intent= new Intent(MerchantDetailsWithransaction.this, AllMerchantList.class);
        intent.putExtra("cate_id","");
        intent.putExtra("cate_name","");
        startActivity(intent);

        Animatoo.animateShrink(this);
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @Override
    public void prodMethod(Cust_ServiceBean cust_serviceBean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(cust_serviceBean, Cust_ServiceBean.class);
        Intent intent = new Intent(MerchantDetailsWithransaction.this, MerchantDetails.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateShrink(this);
    }

    @Override
    public void prodAddCartMethod(Cust_ServiceBean productBean) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        latLng =new LatLng(lat,lng);
        String tittle=data.getName();
        if (latLng != null){

            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title(tittle));
            marker.showInfoWindow();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                latLng, 17F);
            map.animateCamera(cameraUpdate,300,null);
         //   map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(18);
            map.setMinZoomPreference(10);
        } else {
            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title(tittle));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(17);
            map.setMinZoomPreference(17);
        }

    }
}