package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.AdapterClasses.CustomerServiceAdapter;
import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.BeanClasses.Customer_Saving;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SavingActivity extends AppCompatActivity implements CustomerServiceAdapter.ProdMethodCallBack {


    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<Cust_ServiceBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private CustomerServiceAdapter productAdapter;
    String view_type="";
    @BindView(R.id.noData)
    RelativeLayout noData;
    CardPlanetProgress cancelProgressBar;
    ArrayList<Customer_Saving> customer_savings;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.amount)
    TextView amount;
    private Animation animation;
    Window window;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saving);
        ButterKnife.bind(this);

        cancelProgressBar=new CardPlanetProgress(this);

        window=this.getWindow();
        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));

        view_type = getIntent().getExtras().getString("v_type","");


        if (view_type.equalsIgnoreCase("today")){
            txtTitle.setText("Today Saving");
        }else {
            txtTitle.setText("Total Saving");
        }


        //set animation on adapter...
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(this, resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...
        layoutManager = new LinearLayoutManager(this);
        viewRecycleProduct.setLayoutManager(layoutManager);
        productAdapter = new CustomerServiceAdapter(merchantBeans, this,this);
        viewRecycleProduct.setAdapter(productAdapter);
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchMerchantList();
        fetWalletPrice();
    }

    private void fetchMerchantList() {
        HashMap<String, String> params = new HashMap<>();
        //    Toast.makeText(getActivity(),"id "+ User.getCurrentUser().getId(),Toast.LENGTH_LONG).show();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("v_type", view_type);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CUST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = Cust_ServiceBean.parseCust_ServiceBeanArray(jsonObject.getJSONArray("service_data"));
                        productAdapter.setMerchantBeans(merchantBeans);
                        productAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleProduct.getAdapter().notifyDataSetChanged();
                        viewRecycleProduct.scheduleLayoutAnimation();
                        //-----------end------------
                        noData.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                    productAdapter.setMerchantBeans(merchantBeans);
                    productAdapter.notifyDataSetChanged();
                    noData.setVisibility(View.VISIBLE);

                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, this, params);
    }




    private void fetWalletPrice() {
        cancelProgressBar.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SAVING_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (customer_savings != null)
                            customer_savings.clear();
                        customer_savings = Customer_Saving.parseCustomer_SavingArrayyy(jsonObject.getJSONArray("saving_data"));

                        //  Log.e("title" , offer_banners.get(0).getC_status());

                      if (view_type.equalsIgnoreCase("today")){

                        if (!customer_savings.get(0).getToday().equalsIgnoreCase("0")){
                            amount.setText(customer_savings.get(0).getToday()+".00");
                        }else{
                            amount.setText("0");
                        }
                      }else{
                          if (!customer_savings.get(0).getTotal().equalsIgnoreCase("0")){
                                  amount.setText(customer_savings.get(0).getTotal()+".00");
                          }else{
                                amount.setText("0");
                          }
                      }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }


        }, this, params);
    }

    @Override
    public void prodMethod(Cust_ServiceBean cust_serviceBean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(cust_serviceBean, Cust_ServiceBean.class);
        Intent intent = new Intent(SavingActivity.this, MerchantDetails.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateShrink(SavingActivity.this);
    }

    @Override
    public void prodAddCartMethod(Cust_ServiceBean productBean) {

    }
}