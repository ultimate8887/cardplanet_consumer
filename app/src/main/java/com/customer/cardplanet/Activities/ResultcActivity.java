package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.customer.cardplanet.AdapterClasses.Category;
import com.customer.cardplanet.AdapterClasses.Condition;
import com.customer.cardplanet.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResultcActivity extends AppCompatActivity {

    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;
    @BindView(R.id.img3)
    ImageView img3;
    private Animation animation;

    @BindView(R.id.lyt)
    LinearLayout result_card;
    @BindView(R.id.root)
    NestedScrollView root;

    @BindView(R.id.tagline)
    TextView tagline;
    @BindView(R.id.tips1)
    TextView tips1;
    @BindView(R.id.tips2)
    TextView tips2;
    @BindView(R.id.tips3)
    TextView tips3;
    @BindView(R.id.gender)
    TextView genderss;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Window window;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultc);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        window=this.getWindow();
        float BMI  = Math.round((intent.getFloatExtra("BMI", 0) * 100) / 100);
        String age_value = intent.getStringExtra("age");
        String gender = intent.getStringExtra("gender");
        TextView your_bmi = findViewById(R.id.your_bmi);
        your_bmi.setText(String.valueOf(BMI));
        genderss.setText(gender);
        TextView age = findViewById(R.id.age);
        age.setText(age_value);

        TextView category = findViewById(R.id.category);
        Category category1 = new Category();
        category.setText(category1.getCategory(BMI));

        TextView condition = findViewById(R.id.condition);
        Condition condition1 = new Condition();
        condition.setText(condition1.getCategory(BMI));
//
//        Button recalculate = findViewById(R.id.recalculate);
//        recalculate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                updateUI();
//            }
//        });

        if (BMI < 18.5) {
         //   Toast.makeText(getApplicationContext(),"less",Toast.LENGTH_LONG).show();
            result_card.setBackgroundResource(R.color.yellow);
            Picasso.with(this).load(R.drawable.warning).into(img);
            root.setBackgroundResource(R.color.yellow);
            window.setStatusBarColor(this.getResources().getColor(R.color.yellow));
          //  img.setImageResource(R.drawable.warning);
            tagline.setText("Here are some advices To help you increase your weight");
            Picasso.with(this).load(R.drawable.nowater).into(img1);
          //  img1.setImageResource(R.drawable.nowater);
            tips1.setText("Don't drink water before meals");
         //   img2.setImageResource(R.drawable.bigmeal);
            Picasso.with(this).load(R.drawable.bigmeal).into(img2);
            tips2.setText("Use bigger plates");
          //  img3.setImageResource(R.drawable.moon);
            Picasso.with(this).load(R.drawable.moon).into(img3);
            tips3.setText("Get quality sleep");

        } else if (BMI > 25) {
            condition.setTextColor(R.color.dark_black);
        //    Toast.makeText(getApplicationContext(),"more",Toast.LENGTH_LONG).show();
            result_card.setBackgroundResource(R.color.red);
            root.setBackgroundResource(R.color.red);
            window.setStatusBarColor(this.getResources().getColor(R.color.red));
            Picasso.with(this).load(R.drawable.alarmssss).into(img);
         //   img.setImageResource(R.drawable.alarm);
            tagline.setText("Here are some advices To help you decrease your weight");
          //  img1.setImageResource(R.drawable.water);
            Picasso.with(this).load(R.drawable.water).into(img1);
            tips1.setText("Drink water a half hour before meals");
          //  img2.setImageResource(R.drawable.twoeggs);
            Picasso.with(this).load(R.drawable.twoeggs).into(img2);
            tips2.setText("Eat only two meals per day and make sure that they contains a high protein");
           // img3.setImageResource(R.drawable.nosugar);
            Picasso.with(this).load(R.drawable.nosugar).into(img3);
            tips3.setText("Drink coffee or tea and Avoid sugary food");

            } else{
     //       Toast.makeText(getApplicationContext(),"normal",Toast.LENGTH_LONG).show();
            result_card.setBackgroundResource(R.color.light_green);
            root.setBackgroundResource(R.color.light_green);
            window.setStatusBarColor(this.getResources().getColor(R.color.light_green));
            Picasso.with(this).load(R.drawable.check_mark).into(img);
        //    img.setImageResource(R.drawable.check_mark);
            tagline.setText("Here are some advices To help you keep your weight");
         //   img1.setImageResource(R.drawable.yoga);
            Picasso.with(this).load(R.drawable.yoga).into(img1);
            tips1.setText("Stay active and keep your body fit");
           // img2.setImageResource(R.drawable.spaghetti);
            Picasso.with(this).load(R.drawable.spaghetti).into(img2);
            tips2.setText("Choose the right foods and cook by yourself");
          //  img3.setImageResource(R.drawable.moon);
            Picasso.with(this).load(R.drawable.moon).into(img3);
            tips3.setText("Focus on relaxation and sleep");

        }



    }
    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        finish();
    }

    private void updateUI() {
//        Intent intent1 = new Intent(ResultActivity.this,MainActivity.class);
//        startActivity(intent1);
       // fileList();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        updateUI();

    }
}