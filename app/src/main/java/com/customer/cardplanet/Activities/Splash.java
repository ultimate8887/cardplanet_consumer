package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Splash extends AppCompatActivity {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.tittle)
    TextView title;
    boolean link = true;
    private Animation animation;
    Window window;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Animation top_curve_anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_animation);
        title.startAnimation(top_curve_anim);
//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.white));
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 1000) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = null;

                    if (User.getCurrentUser() == null) {
                        intent = new Intent(Splash.this,
                                BoardingScreen.class);

                    } else{
                        if (User.getCurrentUser().getDevice_pin() == null) {
                             intent = new Intent(Splash.this, PinSetupActivity.class);

                        } else{
                             intent = new Intent(Splash.this, UseFingerPrintActivity.class);

                        }
//                        intent = new Intent(Splash.this,
//                                PassCodeActivity.class);
                    }

                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    finish();
                    Animatoo.animateZoom(Splash.this);

                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    Splash.this.finish();
                }
            }

        };
        splashTread.start();

    }


}