package com.customer.cardplanet.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.BuildConfig;
import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.Customer_Saving;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

public class UserMenuActivity extends AppCompatActivity implements OnMapReadyCallback {

    CircularImageView navi_profile;
    TextView stu_name,amount;
    @BindView(R.id.search_location)
    TextView search_location;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.logout)
    ImageView logout;
    @BindView(R.id.txtAcc)
    TextView txtAcc;
    @BindView(R.id.txtPass)
    TextView txtPass;
    @BindView(R.id.txtPP)
    TextView txtPP;
    @BindView(R.id.txtInfo)
    TextView txtInfo;
    Animation animation;
    BottomSheetDialog mBottomSheetDialog;
    String password="", c_password="";
    private int VERIFY_NUMBER = 1000;
    ImageView show_pass_btn1,show_pass_btn2;
    EditText pass,c_pass;
    String get_number="";
    CardPlanetProgress cancelProgressBar;
    ArrayList<Customer_Saving> customer_savings;
    // map view data
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Marker myMarker = null;
    double lat, lng;
    Handler handler = new Handler();
    Geocoder geo;
    List<Address> addresses = null;
    StringBuilder  str = null;
    LatLng latLng;

    private JSONObject user_obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_menu);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        geo = new Geocoder(getApplicationContext(), Locale.getDefault());
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_location);
        mapFragment.getMapAsync(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        cancelProgressBar=new CardPlanetProgress(this);
        stu_name = (TextView) findViewById(R.id.stu_name);
        amount = (TextView) findViewById(R.id.amount);
        navi_profile = (CircularImageView) findViewById(R.id.navi_profile);
        fetWalletPrice();
        setData();
    }

    private void fetWalletPrice() {

        HashMap<String, String> params = new HashMap<>();
        params.put("userid",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SAVING_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (customer_savings != null)
                            customer_savings.clear();
                        customer_savings = Customer_Saving.parseCustomer_SavingArrayyy(jsonObject.getJSONArray("saving_data"));

                        //  Log.e("title" , offer_banners.get(0).getC_status());
                        if (!customer_savings.get(0).getWallet().equalsIgnoreCase("0")){
                            amount.setText(customer_savings.get(0).getWallet()+".00");
                        }else{
                            amount.setText("0");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }


        }, this, params);
    }

    private void setData() {

        User data=User.getCurrentUser();

        if (!data.getLatitude().equalsIgnoreCase("0") && !data.getLongitude().equalsIgnoreCase("0")) {
            lat = Double.parseDouble(data.getLatitude());
            lng= Double.parseDouble(data.getLongitude());
            Log.e("latlong",data.getLatitude()+ "   "+data.getLongitude());
        }else{
               Toast.makeText(getApplicationContext(),"Location Not Found",Toast.LENGTH_SHORT).show();
        }

        stu_name.setText(User.getCurrentUser().getName());

        if (User.getCurrentUser().getProfilepic()!=null) {
            Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(this.getResources().getDrawable(R.drawable.categories)).into(navi_profile);
        }

    }

    @OnClick(R.id.search_location)
    public void search_locationssss() {
        search_location.startAnimation(animation);
        startActivity(new Intent(getApplicationContext(),LocationActivity.class));
        finish();
        Animatoo.animateShrink(UserMenuActivity.this);

    }

    @OnClick(R.id.logout)
    public void logouttt() {

        logout.startAnimation(animation);

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        cancelProgressBar.show();
                        User.getCurrentUser().logout();
                        //     Toast.makeText(CustomerHomeActivity.this, "Logged out successfully", Toast.LENGTH_SHORT).show();
                        Intent loginpage = new Intent(UserMenuActivity.this, LoginActivity.class);
                        loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(loginpage);
                        cancelProgressBar.dismiss();
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(UserMenuActivity.this);
        builder.setMessage("Are you sure, You want to Logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }
    @OnClick(R.id.txtAcc)
    public void txtAcccc() {
        txtAcc.startAnimation(animation);
        Intent intent=new Intent(UserMenuActivity.this,ProfileActivity.class);
        startActivity(intent);
        Animatoo.animateZoom(UserMenuActivity.this);

    }
    @OnClick(R.id.txtPass)
    public void txtPassss() {
        txtPass.startAnimation(animation);

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        open_layout();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(UserMenuActivity.this);
        builder.setMessage("Are you sure, You want to Reset your Password?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }

    private void open_layout() {
        cancelProgressBar.show();
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());

// Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setTheme(R.style.phoneTheme)
                        .setLogo(R.drawable.logo)
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false,
                                true)
                        .build(),
                VERIFY_NUMBER);

        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        //              Toast.makeText(LoginActivity.this,"SignOut",Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cancelProgressBar.dismiss();
        if (requestCode == VERIFY_NUMBER) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                get_number=response.getPhoneNumber();

                //  Toast.makeText(LoginActivity.this,"Number "+get_number,Toast.LENGTH_SHORT).show();
                Log.e("number",get_number);
                numberCheck(get_number);
                 } else {
                Toast.makeText(UserMenuActivity.this,"Verification Failed, Try again.",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void numberCheck(String get_number) {
        cancelProgressBar.show();
        String num="+91"+ User.getCurrentUser().getMobile();
        if (num.equalsIgnoreCase(get_number)){
            cancelProgressBar.dismiss();
         //   Toast.makeText(UserMenuActivity.this,"Registered Number Matched",Toast.LENGTH_SHORT).show();

            mBottomSheetDialog = new BottomSheetDialog(UserMenuActivity.this);
            final View sheetView = getLayoutInflater().inflate(R.layout.reset_password, null);
            mBottomSheetDialog.setContentView(sheetView);
            final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
            mBottomSheetDialog.setCancelable(false);
            CircularImageView imgStud1  = (CircularImageView) sheetView.findViewById(R.id.myimage);
            TextView text = (TextView) sheetView.findViewById(R.id.title);
            text.setText("Hey, "+User.getCurrentUser().getName()+"\n Create New Password");
            if (User.getCurrentUser().getProfilepic() != null) {
                Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.person_1).into(imgStud1);
            } else {
                Picasso.with(this).load(R.drawable.person_1).into(imgStud1);
            }

            final EditText edtPassword1 = (EditText) sheetView.findViewById(R.id.edtPassword1);
            final EditText edtPassword2 = (EditText) sheetView.findViewById(R.id.edtPassword2);

            RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBottomSheetDialog.dismiss();
                }
            });
            TextView btnYes= (TextView) sheetView.findViewById(R.id.btnYes);
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnYes.startAnimation(animation);
                    if (edtPassword1.getText().toString().trim().length() <= 0){
                        //    edtPassword1.setError("Enter Password");
                        Toast.makeText(UserMenuActivity.this,"Enter Password!",Toast.LENGTH_SHORT).show();
                    } else if (edtPassword2.getText().toString().trim().length() <= 0){
                        // edtPassword2.setError("Re-Enter Password");
                        Toast.makeText(UserMenuActivity.this,"Re-Enter Password!",Toast.LENGTH_SHORT).show();
                    } else {
                        password=edtPassword1.getText().toString();
                        c_password=edtPassword2.getText().toString();
                        if (password.equalsIgnoreCase(c_password)){
                            change_password_api();
                        } else {
                            //  edtPassword2.setError("Re-Enter Password Not Matched!");
                            Toast.makeText(UserMenuActivity.this,"Re-Enter Password Not Matched!",Toast.LENGTH_SHORT).show();
                        }
                    }
                    // Toast.makeText(getActivity(),"Reset Password",Toast.LENGTH_SHORT).show();
                }
            });

            mBottomSheetDialog.show();

        }else{
            cancelProgressBar.dismiss();
            Toast.makeText(UserMenuActivity.this,"Registered Number Not Matched, Try again",Toast.LENGTH_SHORT).show();

        }
    }

    private void change_password_api() {
        cancelProgressBar.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("u_type", "password");
        params.put("password", c_password);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_DATA_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
              cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                        User.setCurrentUser(user_obj);
                        //    Session_Bean.setCurrentSessiondetail(RecruiterUser.getCurrentRecruiterUser().getId(), 2);
                        Toast.makeText(UserMenuActivity.this, "Password Reset Successfully!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(UserMenuActivity.this, FinalHomeActivity.class));
                        finish();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(UserMenuActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);


    }


    @OnClick(R.id.txtPP)
    public void txtpppp() {
        txtPP.startAnimation(animation);

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(UserMenuActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.privacy_policy_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        // final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
        final boolean[] loadingFinished = {true};
        final boolean[] redirect = {false};
        WebView webView = (WebView) sheetView.findViewById(R.id.privacy);

        cancelProgressBar.show();
        if (!loadingFinished[0]) {
            redirect[0] = true;
        }
        loadingFinished[0] = false;
        webView.getSettings().setJavaScriptEnabled(true);
        String url = "https://cardplanet.in/include/customer_api/terms_condition.html";
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                if(!redirect[0]){
                    loadingFinished[0] = true;
                }
                if(loadingFinished[0] && !redirect[0]){
                    cancelProgressBar.dismiss();
                } else{
                    redirect[0] = false;
                }
            }
        });

        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();


    }

    @OnClick(R.id.txtInfo)
    public void txtInfooo() {
        txtInfo.startAnimation(animation);

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        final View sheetView = getLayoutInflater().inflate(R.layout.app_info_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);

        TextView txtVersion = (TextView) sheetView.findViewById(R.id.txtVersion);
        FloatingActionButton whatsapp = (FloatingActionButton) sheetView.findViewById(R.id.whatsapp);
        FloatingActionButton email = (FloatingActionButton) sheetView.findViewById(R.id.email);
        FloatingActionButton phone = (FloatingActionButton) sheetView.findViewById(R.id.phone);
        String number="9871487254";
        String versionName ="";
        try {
            versionName = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int code= BuildConfig.VERSION_CODE;
        txtVersion.setText("App Version Code: "+code+"\n"+"App Version Name: "+String.valueOf(versionName));

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
//                UltimateProgress.showProgressBar(getActivity(), "Please Wait.....!");
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                //simplydirect email for setting email,,,
                intent.setData(Uri.parse("mailto:app.cardplanet@gmail.com")); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
              //  UltimateProgress.cancelProgressBar();

            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mBottomSheetDialog.dismiss();
                Uri uri = Uri.parse("smsto:" + number);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(i, "Chat Now!"));
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mBottomSheetDialog.dismiss();
                // for permission granted....
                if (ActivityCompat.checkSelfPermission(UserMenuActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UserMenuActivity.this, new String[]{Manifest.permission.CALL_PHONE},1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                    //UltimateProgress.cancelProgressBar();
                }
            }
        });


        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        common();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        common();
    }

    private void common() {
        startActivity(new Intent(UserMenuActivity.this, FinalHomeActivity.class));
        finish();
      //  Animatoo.animateZoom(UserMenuActivity.this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        latLng =new LatLng(lat,lng);
        String tittle="";

        if (User.getCurrentUser().getOptionaladdress()==null) {
            tittle=User.getCurrentUser().getAddress();
        } else if (User.getCurrentUser().getOptionaladdress().equalsIgnoreCase("empty")){
            tittle=User.getCurrentUser().getAddress();
        }else{
            tittle=User.getCurrentUser().getOptionaladdress();
        }

        if (latLng != null){

            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title(tittle));
            marker.showInfoWindow();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    latLng, 15F);
            map.animateCamera(cameraUpdate,300,null);
            //   map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(18);
            map.setMinZoomPreference(10);
        } else {
            Marker marker=map.addMarker(new MarkerOptions().position(latLng).title(tittle));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,17F));
            map.setMaxZoomPreference(17);
            map.setMinZoomPreference(17);
        }

    }
}