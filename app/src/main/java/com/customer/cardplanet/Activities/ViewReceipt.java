package com.customer.cardplanet.Activities;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewReceipt extends AppCompatActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.discount)
    TextView discount;
    @BindView(R.id.amount)
    EditText amount;

    Animation animation;
    MerchantBean data;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Window window;
    @BindView(R.id.myimage)
    CircularImageView myimage;
    @BindView(R.id.m_image)
    CircularImageView m_image;

    @BindView(R.id.billing)
    TextView billing;
    @BindView(R.id.cashback)
    TextView cashback;
    @BindView(R.id.b_date)
    TextView b_date;
    @BindView(R.id.redeem)
    TextView redeem;
    @BindView(R.id.r_date)
    TextView r_date;
    CardPlanetProgress cardPlanetProgress;

    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.remarks)
    TextView remarks;
    @BindView(R.id.s_holder)
    TextView s_holder;
    @BindView(R.id.s_name)
    TextView s_name;
    @BindView(R.id.redeem_lyt)
    LinearLayout redeem_lyt;
    ArrayList<Cust_ServiceBean> merchantBeans = new ArrayList<>();
    String r_id="";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_details);
        ButterKnife.bind(this);
        if (getIntent().getExtras()!=null){
            r_id=getIntent().getExtras().getString("r_id") ;
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
       // Toast.makeText(getApplicationContext(),"r_id "+ r_id,Toast.LENGTH_LONG).show();
        fetchMerchantList();
        cardPlanetProgress=new CardPlanetProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        // holder.product_add.startAnimation(animation);
    }

    private void fetchMerchantList() {
        HashMap<String, String> params = new HashMap<>();
        //    Toast.makeText(getActivity(),"id "+ User.getCurrentUser().getId(),Toast.LENGTH_LONG).show();
        params.put("r_id", r_id);
        params.put("v_type", "receipt");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CUST_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = Cust_ServiceBean.parseCust_ServiceBeanArray(jsonObject.getJSONArray("service_data"));
                        Cust_ServiceBean user=merchantBeans.get(0);
                        setCustomerData(user);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    //  Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, this, params);
    }

    private void setCustomerData(Cust_ServiceBean user) {
        amount.setEnabled(false);
        amount.setText(user.getGrand_total());



        if (user.getImage() != null) {
            Picasso.with(getApplicationContext()).load(user.getImage()).placeholder(this.getResources().getDrawable(R.drawable.person_1)).into(m_image);
        }
        if (User.getCurrentUser().getProfilepic() != null) {
            Picasso.with(getApplicationContext()).load(User.getCurrentUser().getProfilepic() ).placeholder(this.getResources().getDrawable(R.drawable.person_1)).into(myimage);
        }

        title.setText("Rupees Paid to "+"'"+user.getName() +"'"+" Successfully");

        if (user.getServiceholder_name()!=null){
            //   name2.setText(user.getFamily2name());
            String title = getColoredSpanned("<b>" +"Service Holder- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getServiceholder_name() , "#7D7D7D");
            s_holder.setText(Html.fromHtml(title + " " + Name));
        }else{
            s_holder.setText("Not Mentioned");
        }

        if (user.getDiscount()!=null){
            //   name2.setText(user.getFamily2name());
            String title = getColoredSpanned("<b>" +"Discount- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getDiscount()  + "%", "#7D7D7D");
            discount.setText(Html.fromHtml(title + " " + Name));
        }else{
            discount.setText("Not Mentioned");
        }

        if (user.getService_type()!=null){
            // dob2.setText(user.getFamily2dob());
            String title = getColoredSpanned("<b>" +"Service Type- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned( user.getService_type(), "#7D7D7D");
            s_name.setText(Html.fromHtml(title + " " + Name));
        }else{
            s_name.setText("Not Mentioned");
        }
//        if (user.getPartner_discount()!=null){
//            // gender2.setText(user.getFamily2gender());
//            String title = getColoredSpanned("<b>" +"Discount- "+"</b>", "#5A5C59");
//            String Name = getColoredSpanned(user.getPartner_discount()+"%" , "#7D7D7D");
//            discount.setText(Html.fromHtml(title + " " + Name));
//        }else{
//            discount.setText("Not Mentioned");
//        }
        if (user.getBilling_amount()!=null){
            //  mobile2.setText(user.getFamily2mobile());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getBilling_amount() , "#7D7D7D");
            billing.setText(Html.fromHtml(title + " " + Name));
        }else{
            billing.setText("Not Mentioned");
        }
        if (user.getCashback()!=null){
            // relation2.setText(user.getFamily2relation());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getCashback() , "#7D7D7D");
            cashback.setText(Html.fromHtml(title + " " + Name));
        }else{
            cashback.setText("Not Mentioned");
        }


        if (user.getG_date()!=null){
            //   name3.setText(user.getFamily3name());
            String title = getColoredSpanned("<b>" +"Billing Date- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getG_date() , "#7D7D7D");
            b_date.setText(Html.fromHtml(title + " " + Name));
        }else{
            b_date.setText("Not Mentioned");
        }


        if (user.getR_date()!=null){
            //   name3.setText(user.getFamily3name());
            String title = getColoredSpanned("<b>" +"Redeem Date- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getR_date() , "#7D7D7D");
            r_date.setText(Html.fromHtml(title + " " + Name));
        }else{
            r_date.setText("Not Mentioned");
        }

        if (user.getRedeem_voucher()!=null){
            //   gender3.setText(user.getFamily3gender());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getRedeem_voucher() , "#7D7D7D");
            redeem.setText(Html.fromHtml(title + " " + Name));
        }else{
            redeem.setText("Not Mentioned");
        }
        if (user.getRemarks()!=null){
            //    mobile3.setText(user.getFamily3mobile());
            String title = getColoredSpanned("<b>" +"Remarks- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getRemarks() , "#7D7D7D");
            remarks.setText(Html.fromHtml(title + " " + Name));
        }else{
            remarks.setText("Not Mentioned");
        }
        if (user.getGrand_total()!=null){
            // relation3.setText(user.getFamily3relation());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getGrand_total() , "#7D7D7D");
            total.setText(Html.fromHtml(title + " " + Name));
        }else{
            total.setText("Not Mentioned");
        }


        if (user.getVoucher_status().equalsIgnoreCase("voucher")){
            redeem_lyt.setVisibility(View.GONE);
            r_date.setVisibility(View.GONE);
        }else{
            redeem_lyt.setVisibility(View.VISIBLE);
            r_date.setVisibility(View.VISIBLE);
        }


    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
}


