package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.Utils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MerchantDetails extends AppCompatActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.discount)
    TextView discount;
    @BindView(R.id.amount)
     EditText amount;
//
//    @BindView(R.id.category_name)
//    TextView category_name;
//    @BindView(R.id.product_short_desc)
//    TextView product_desc;
    Animation animation;
    MerchantBean data;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Window window;
    @BindView(R.id.myimage)
    CircularImageView myimage;
    @BindView(R.id.m_image)
    CircularImageView m_image;

    @BindView(R.id.billing)
    TextView billing;
    @BindView(R.id.cashback)
    TextView cashback;
    @BindView(R.id.b_date)
    TextView b_date;
    @BindView(R.id.redeem)
    TextView redeem;
    @BindView(R.id.r_date)
    TextView r_date;

    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.remarks)
    TextView remarks;
    @BindView(R.id.s_holder)
    TextView s_holder;
    @BindView(R.id.s_name)
    TextView s_name;
    @BindView(R.id.redeem_lyt)
    LinearLayout redeem_lyt;

    Cust_ServiceBean user;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_details);
        ButterKnife.bind(this);

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
       // holder.product_add.startAnimation(animation);

//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));

        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("merchant_data")) {
            Gson gson = new Gson();
            user = gson.fromJson(intent_value.getString("merchant_data"), Cust_ServiceBean.class);
            setCustomerData();
        }

    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
        finish();
    }

    private void setCustomerData() {

        amount.setEnabled(false);
        amount.setText(user.getGrand_total());

        if (user.getImage() != null) {
            Picasso.with(this).load(user.getImage()).placeholder(this.getResources().getDrawable(R.drawable.person_1)).into(m_image);
        }
        if (User.getCurrentUser().getProfilepic() != null) {
            Picasso.with(this).load(User.getCurrentUser().getProfilepic() ).placeholder(this.getResources().getDrawable(R.drawable.person_1)).into(myimage);
        }

        title.setText("Rupees Paid to "+"'"+user.getName() +"'"+" Successfully");

        if (user.getServiceholder_name()!=null){
            //   name2.setText(user.getFamily2name());
            String title = getColoredSpanned("<b>" +"Service Holder- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getServiceholder_name() , "#7D7D7D");
            s_holder.setText(Html.fromHtml(title + " " + Name));
        }else{
            s_holder.setText("Not Mentioned");
        }

        if (user.getDiscount()!=null){
            //   name2.setText(user.getFamily2name());
            String title = getColoredSpanned("<b>" +"Discount- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getDiscount()  + "%", "#7D7D7D");
            discount.setText(Html.fromHtml(title + " " + Name));
        }else{
            discount.setText("Not Mentioned");
        }

        if (user.getService_type()!=null){
            // dob2.setText(user.getFamily2dob());
            String title = getColoredSpanned("<b>" +"Service Type- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned( user.getService_type(), "#7D7D7D");
            s_name.setText(Html.fromHtml(title + " " + Name));
        }else{
            s_name.setText("Not Mentioned");
        }
//        if (user.getPartner_discount()!=null){
//            // gender2.setText(user.getFamily2gender());
//            String title = getColoredSpanned("<b>" +"Discount- "+"</b>", "#5A5C59");
//            String Name = getColoredSpanned(user.getPartner_discount()+"%" , "#7D7D7D");
//            discount.setText(Html.fromHtml(title + " " + Name));
//        }else{
//            discount.setText("Not Mentioned");
//        }
        if (user.getBilling_amount()!=null){
            //  mobile2.setText(user.getFamily2mobile());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getBilling_amount() , "#7D7D7D");
            billing.setText(Html.fromHtml(title + " " + Name));
        }else{
            billing.setText("Not Mentioned");
        }
        if (user.getCashback()!=null){
            // relation2.setText(user.getFamily2relation());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getCashback() , "#7D7D7D");
            cashback.setText(Html.fromHtml(title + " " + Name));
        }else{
            cashback.setText("Not Mentioned");
        }


        if (user.getG_date()!=null){
            //   name3.setText(user.getFamily3name());
            String title = getColoredSpanned("<b>" +"Billing Date- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getG_date() , "#7D7D7D");
            b_date.setText(Html.fromHtml(title + " " + Name));
        }else{
            b_date.setText("Not Mentioned");
        }


        if (user.getR_date()!=null){
            //   name3.setText(user.getFamily3name());
            String title = getColoredSpanned("<b>" +"Redeem Date- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getR_date() , "#7D7D7D");
            r_date.setText(Html.fromHtml(title + " " + Name));
        }else{
            r_date.setText("Not Mentioned");
        }

        if (user.getRedeem_voucher()!=null){
            //   gender3.setText(user.getFamily3gender());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getRedeem_voucher() , "#7D7D7D");
            redeem.setText(Html.fromHtml(title + " " + Name));
        }else{
            redeem.setText("Not Mentioned");
        }
        if (user.getRemarks()!=null){
            //    mobile3.setText(user.getFamily3mobile());
            String title = getColoredSpanned("<b>" +"Remarks- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getRemarks() , "#7D7D7D");
            remarks.setText(Html.fromHtml(title + " " + Name));
        }else{
            remarks.setText("Not Mentioned");
        }
        if (user.getGrand_total()!=null){
            // relation3.setText(user.getFamily3relation());
            String title = getColoredSpanned("", "#5A5C59");
            String Name = getColoredSpanned(user.getGrand_total() , "#7D7D7D");
            total.setText(Html.fromHtml(title + " " + Name));
        }else{
            total.setText("Not Mentioned");
        }


        if (user.getVoucher_status().equalsIgnoreCase("voucher")){
            redeem_lyt.setVisibility(View.GONE);
            r_date.setVisibility(View.GONE);
        }else{
            redeem_lyt.setVisibility(View.VISIBLE);
            r_date.setVisibility(View.VISIBLE);
        }

    }
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
}