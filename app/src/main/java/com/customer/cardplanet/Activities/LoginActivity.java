package com.customer.cardplanet.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.CardPlanetCustomerApp;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.FirebaseGetDeviceToken;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;
    @BindView(R.id.sign_in_btn)
    TextView sign_in_btn;
    @BindView(R.id.viewDocument)
    TextView viewDocument;
    @BindView(R.id.show_pass_btn)
    ImageView show_pass_btn;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.googleLogin)
    FloatingActionButton googleLogin;
    @BindView(R.id.phoneLogin)
    FloatingActionButton phoneLogin;
    @BindView(R.id.sign_in_username)
    EditText edtUsrName;
    @BindView(R.id.sign_in_pwd)
    EditText edtPassword;
//    @BindView(R.id.input_layout_name)
//    TextInputLayout input_layout_name;
//    @BindView(R.id.input_layout_pwd)
//    TextInputLayout input_layout_pwd;
    SharedPreferences pref;
    private String deviceid;
    LinearLayout parent;
    private int deviceIdChecked = 0;
    private JSONObject user_obj;
    String type="",email_type="email",number_type="mobile";
    String get_email="",get_number="";
    private int VERIFY_NUMBER = 1000;
    FirebaseAuth mAuth;
    private Animation animation,animation1;
    CardPlanetProgress cancelProgressBar;
    SharedPreferences sharedPreferences;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.checkedddd)
    ImageView checkedddd;
    boolean link = true;
    Window window;
    Pinview pin,confirm_pinview;
    String longtude="",lattude="",address="",area="",city="",state="",pincode="";
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_login);
        ButterKnife.bind(this);
//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.colorAccent));
        cancelProgressBar=new CardPlanetProgress(this);

        pin = new Pinview(this);
        confirm_pinview=new Pinview(this);
        //Intent intent= getIntent();
        if(getIntent().getExtras() != null) {
            address = getIntent().getExtras().getString("address", "");
            lattude = getIntent().getExtras().getString("lat", "");
            longtude = getIntent().getExtras().getString("long", "");
            area = getIntent().getExtras().getString("opt_address", "");
            city = getIntent().getExtras().getString("city", "");
            state = getIntent().getExtras().getString("state", "");
            pincode = getIntent().getExtras().getString("pincode", "");
        }
    //    Toast.makeText(getApplicationContext(), "check "+city+"  "+state+"  "+pincode, Toast.LENGTH_SHORT).show();

        flyIn();
        mAuth = FirebaseAuth.getInstance();
        getDeviceTocken();

        edtUsrName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkedddd.setVisibility(View.GONE);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (edtUsrName.getText().toString().trim().length() >= 10) {
                    checkedddd.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtUsrName.getText().toString().isEmpty()) {
                    checkedddd.setVisibility(View.GONE);
                }
            }
        });

    }

    private void getDeviceTocken() {

        pref = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");
        if (deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
        }
        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }



    private void checkDeviceIDValid() {

        if (deviceid == null && deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceTocken();
            } else {
                //   showSnackBar("Please wait.....");
                Toast.makeText(LoginActivity.this, "Please wait.....", Toast.LENGTH_SHORT).show();
                FirebaseGetDeviceToken.getDeviceToken();
            }
        } else {
            Toast.makeText(LoginActivity.this, "Error to getting device token.....", Toast.LENGTH_SHORT).show();
        }
    }

//    @OnClick(R.id.signUp)
//    public void signUpppp(){
//        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
//    }


    @OnClick(R.id.googleLogin)
    public void googleLoginnn() {

        if (checkbox.isChecked()){
            cancelProgressBar.show();
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.GoogleBuilder().build());

// Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setTheme(R.style.phoneTheme)
                        .setLogo(R.drawable.logo)
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false,
                                true)
                        .build(),
                RC_SIGN_IN);

        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        //              Toast.makeText(LoginActivity.this,"SignOut",Toast.LENGTH_SHORT).show();
                    }
                });
        }else{

            commonCodemsg();
        }

    }

    @OnClick(R.id.phoneLogin)
    public void phoneLoginnn() {

        if (checkbox.isChecked()){
            cancelProgressBar.show();
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());

// Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setTheme(R.style.phoneTheme)
                        .setLogo(R.drawable.logo)
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false,
                                true)
                        .build(),
                VERIFY_NUMBER);

        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        //              Toast.makeText(LoginActivity.this,"SignOut",Toast.LENGTH_SHORT).show();
                    }
                });
        }else{

            commonCodemsg();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cancelProgressBar.dismiss();
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                get_email=response.getEmail();
                //  Toast.makeText(LoginActivity.this,"Email "+get_email,Toast.LENGTH_SHORT).show();
                Log.e("email",get_email);
                emailCheck(get_email);
            } else {
                Toast.makeText(LoginActivity.this,"Sign in failed",Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == VERIFY_NUMBER) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                get_number=response.getPhoneNumber();
                //  Toast.makeText(LoginActivity.this,"Number "+get_number,Toast.LENGTH_SHORT).show();
                Log.e("number",get_number);
                numberCheck(get_number);
            } else {
                Toast.makeText(LoginActivity.this,"Sign in failed",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void numberCheck(String get_number) {

        cancelProgressBar.show();

//        CardPlanetProgress.showProgressBar(LoginActivity.this,"Please Wait.....!");
        HashMap<String,String> params = new HashMap<>();
        params.put("number", get_number);
        params.put("type",number_type);
        params.put("address", address);
        params.put("city", city);
        params.put("state", state);
        params.put("pincode", pincode);
        params.put("opt_address", area);
        params.put("latitude",lattude);
        params.put("longitude",longtude);
        params.put("device_token",deviceid);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.LOGIN_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
//                CardPlanetProgress.cancelProgressBar();
                cancelProgressBar.dismiss();
                if (error == null){
                    try {
                        user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                        User.setCurrentUser(user_obj);
                        commonCode();
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    if (error.getStatusCode() == 401) {
//                        //       if(type.equalsIgnoreCase("1")) {
//                        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
//                        intent.putExtra("number", get_number);
//                        startActivity(intent);
//                        finish();
                        Toast.makeText(LoginActivity.this, "Number does Not Exists, Kindly Enter Your Registered Number.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, this, params);
    }

    private void flyIn() {

        animation = AnimationUtils.loadAnimation(this, R.anim.logo_animation);
        animation1 = AnimationUtils.loadAnimation(this, R.anim.enter_from_right_animation);
        googleLogin.startAnimation(animation);
        phoneLogin.startAnimation(animation);
        logo.startAnimation(animation);
      //  title.startAnimation(animation1);
    }

    private void emailCheck(String get_email) {

        cancelProgressBar.show();
        HashMap<String,String> params=new HashMap<>();
        params.put(Constants.EMAIL, get_email);
        params.put("type",email_type);
        params.put("address", address);
        params.put("city", city);
        params.put("state", state);
        params.put("pincode", pincode);
        params.put("opt_address", area);
        params.put("latitude",lattude);
        params.put("longitude",longtude);
        params.put("device_token",deviceid);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.LOGIN_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null){
                    try {
                        user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                        User.setCurrentUser(user_obj);
                        commonCode();
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    if (error.getStatusCode() == 401) {

                        Toast.makeText(LoginActivity.this, "Email does Not Exists, Kindly Enter Your Registered Email Id", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, this, params);
    }


    @OnClick(R.id.show_pass_btn)
    public void show_pass_btnnnnn() {

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        show_pass_btn.startAnimation(animation);

        if(edtPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            show_pass_btn.setImageResource(R.drawable.eye_show);

            //Show Password
            edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            show_pass_btn.setImageResource(R.drawable.eye_visibility);

            //Hide Password
            edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }

    }


    @OnClick(R.id.viewDocument)
    public void viewDocumentttt() {

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        viewDocument.startAnimation(animation);

        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(LoginActivity.this);
        final View sheetView = getLayoutInflater().inflate(R.layout.privacy_policy_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
       // final ViewGroup transitions_container = (ViewGroup) sheetView.findViewById(R.id.transitions_container);
        final boolean[] loadingFinished = {true};
        final boolean[] redirect = {false};
        WebView webView = (WebView) sheetView.findViewById(R.id.privacy);

        cancelProgressBar.show();
        if (!loadingFinished[0]) {
            redirect[0] = true;
        }
        loadingFinished[0] = false;
        webView.getSettings().setJavaScriptEnabled(true);
        String url = "https://cardplanet.in/include/customer_api/terms_condition.html";
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                if(!redirect[0]){
                    loadingFinished[0] = true;
                }
                if(loadingFinished[0] && !redirect[0]){
                    cancelProgressBar.dismiss();
                } else{
                    redirect[0] = false;
                }
            }
        });

        RelativeLayout btnNo= (RelativeLayout) sheetView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();


    }


    public void savePrefData(){
        sharedPreferences=getApplicationContext().getSharedPreferences("location_pref",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit",true);
        editor.apply();
    }


    @OnClick(R.id.sign_in_btn)
    public void btnSignIn() {
      //  Toast.makeText(LoginActivity.this, "Save data", Toast.LENGTH_SHORT).show();

        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        sign_in_btn.startAnimation(animation);
       // savePrefData();
        if (checkValidation()) {
            login();
        }
    }
    private void login() {
        if (checkbox.isChecked()) {
            cancelProgressBar.show();
            HashMap<String, String> params = new HashMap<>();
            params.put(Constants.EMAIL, edtUsrName.getText().toString());
            params.put(Constants.PASSWORD, edtPassword.getText().toString());
            params.put("type", type);
            params.put("address", address);
            params.put("city", city);
            params.put("state", state);
            params.put("pincode", pincode);
            params.put("opt_address", area);
            params.put("latitude",lattude);
            params.put("longitude",longtude);
            params.put("device_token", deviceid);

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.LOGIN_URL, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                    cancelProgressBar.dismiss();
                    if (error == null) {
                        try {
                            user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                            User.setCurrentUser(user_obj);
                            //    Session_Bean.setCurrentSessiondetail(RecruiterUser.getCurrentRecruiterUser().getId(), 2);
                            commonCode();
                          //  setUpPin();
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);
        }else{

            commonCodemsg();
        }
    }

    private void setUpPin() {

        Dialog dialogLog1 = new Dialog(this,android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        //  dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(true);
        dialogLog1.setContentView(R.layout.pin_dialog);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final String[] final_pin = new String[1];
        final String[] final_pins = new String[1];
        CircularImageView imageView = (CircularImageView) dialogLog1.findViewById(R.id.my_image);
        TextView c_name = (TextView) dialogLog1.findViewById(R.id.c_name);
        c_name.setText("WELCOME, \n"+User.getCurrentUser().getName());
        if (User.getCurrentUser().getProfilepic() != null) {
            Picasso.with(getApplicationContext()).load(User.getCurrentUser().getProfilepic() ).
                    placeholder(this.getResources().getDrawable(R.drawable.person_1)).into(imageView);
        }

        pin = (Pinview) dialogLog1.findViewById(R.id.pinview);
        confirm_pinview = (Pinview) dialogLog1.findViewById(R.id.confirm_pinview);
        pin.showCursor(true);
//        pin.setCursorShape(R.drawable.example_cursor);
        confirm_pinview.showCursor(true);

        confirm_pinview.setCursorColor(Color.parseColor("#ffffff"));

        pin.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
//                Toast.makeText(FinalHomeActivity.this, pinview.getValue(), Toast.LENGTH_SHORT).show();
                final_pin[0] =pinview.getValue();
            }
        });
        confirm_pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
//                Toast.makeText(FinalHomeActivity.this, pinview.getValue(), Toast.LENGTH_SHORT).show();


                final_pins[0]=pinview.getValue();

                if (final_pin[0].equalsIgnoreCase(final_pins[0])){
                    Toast.makeText(LoginActivity.this, "Matched Pin "+final_pins[0], Toast.LENGTH_SHORT).show();
                    confirm_pinview.setPinBackgroundRes(R.drawable.r_pin_lyt);
                    pin.setPinBackgroundRes(R.drawable.r_pin_lyt);
                } else{
                    Toast.makeText(LoginActivity.this, "Re-Enter Pin Not Matched", Toast.LENGTH_SHORT).show();
                    confirm_pinview.setPinBackgroundRes(R.drawable.w_pin_lyt);
                }
            }
        });

        ImageView btnNo = (ImageView) dialogLog1.findViewById(R.id.imgBackmsg);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog1.dismiss();

            }
        });
        dialogLog1.show();
    }

    private void commonCodemsg() {
        Toast.makeText(getApplicationContext(),"Kindly View & Agree to Term & Condition First!",Toast.LENGTH_LONG).show();
    }

    private void commonCode() {



        if (!address.equalsIgnoreCase("" ) && !lattude.equalsIgnoreCase("")){
            savePrefData();
            if (User.getCurrentUser().getDevice_pin() == null) {
                Intent intent = new Intent(LoginActivity.this, PinSetupActivity.class);
                startActivity(intent);
            } else{
                Intent intent = new Intent(LoginActivity.this, FinalHomeActivity.class);
                startActivity(intent);
            }
        }else{
            if (User.getCurrentUser().getDevice_pin() == null) {
                Intent intent = new Intent(LoginActivity.this, PinSetupActivity.class);
                startActivity(intent);
            } else{
                Intent intent = new Intent(LoginActivity.this, FinalHomeActivity.class);
                startActivity(intent);
            }
        }

        Animatoo.animateZoom(LoginActivity.this);


    }

    private boolean checkValidation() {

        boolean isValid= true;

        if (edtUsrName.getText().toString().isEmpty()){
            edtUsrName.setError("* Enter valid Mobile Number");
            isValid=false;
        }
        if (edtPassword.getText().toString().trim().length() < 5){
            edtPassword.setError("* Minimum 5 character required");
            isValid=false;
        }
        return isValid;
    }
}