package com.customer.cardplanet.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.CardPlanetCustomerApp;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.FirebaseGetDeviceToken;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    FusedLocationProviderClient mFusedLocationClient;
    private RippleBackground rippleBg;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.head)
    RelativeLayout head;
    @BindView(R.id.confirm_head) RelativeLayout confirm_head;
    @BindView(R.id.add) TextView add;
    String address,area,city,state,pincode;
    int count=0;
    private Animation animation,animation1;
    String longtude,lattude;

    BottomSheetDialog mBottomSheetDialog;
    EditText edtName,edtAddress,edtPhone;
    RelativeLayout veryfy_no;
    Button submitted;
    TextInputLayout inpPhone;



    // mobile number verifications
    private static final int RC_SIGN_IN = 123;
    private int VERIFY_NUMBER = 1000;
    FirebaseAuth mAuth;
    String get_number="";
    @BindView(R.id.search_location)
    TextView search_location;
    SharedPreferences sharedPreferences;
    //preferences for device token
    SharedPreferences pref;
    private String deviceid;
    private int deviceIdChecked = 0;
    private JSONObject user_obj;

    @BindView(R.id.get_location)
    TextView get_location;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
       if (User.getCurrentUser()==null){
        if (restorePrefData()){
            startActivity(new Intent(LocationActivity.this, LoginActivity.class));
            finish();
            //  Animatoo.animateShrink(this);
        }}

        getDeviceTocken();
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);
        transparentfullscreen();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        rippleBg = findViewById(R.id.ripple_bg);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.long_fade_animation);
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.gone_fade_animation);
        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
    }

    public void CheckGpsStatus(){

        LocationManager locationManager ;
        boolean GpsStatus;
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(GpsStatus == true) {
            //   location_text.setText("GPS Is Enabled");
            Toast.makeText(LocationActivity.this, "GPS Is Enabled", Toast.LENGTH_SHORT).show();

        } else {
            //  location_text.setText("GPS Is Disabled");
        //    Toast.makeText(LocationActivity.this, "Error to fetch location, kindly turn on your GPS First", Toast.LENGTH_SHORT).show();
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent1);

                           // finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            Toast.makeText(LocationActivity.this, "Error to fetch location, Try again later.", Toast.LENGTH_SHORT).show();
                            finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(LocationActivity.this);
            builder.setCancelable(false).setMessage("Kindly, Enable Your GPS First!").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (User.getCurrentUser()!=null) {
            CheckGpsStatus();
        }
    }

    private void getDeviceTocken() {

        pref = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");
        if (deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
        }
        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }


    private void checkDeviceIDValid() {

        if (deviceid == null && deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceTocken();
            } else {
                //   showSnackBar("Please wait.....");
                Toast.makeText(LocationActivity.this, "Please wait.....", Toast.LENGTH_SHORT).show();
                FirebaseGetDeviceToken.getDeviceToken();
            }
        } else {
            Toast.makeText(LocationActivity.this, "Error to getting device token.....", Toast.LENGTH_SHORT).show();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void transparentfullscreen() {

        Window window=this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        window.setStatusBarColor(Color.TRANSPARENT);
    }

    @OnClick(R.id.search_location)
    public void search_locationssss() {

        search_location.startAnimation(animation);
        confirm_head.startAnimation(animation1);
        mapFrag.getMapAsync(this);
        //Toast.makeText(getApplicationContext(),"address"+address,Toast.LENGTH_SHORT).show();
        CheckGpsStatus();
    }

    @OnClick(R.id.get_location)
    public void get_locationsss() {
        get_location.startAnimation(animation);
        if (title.getText().toString().equalsIgnoreCase("Address")){
            CheckGpsStatus();
        //    Toast.makeText(LocationActivity.this, "Error to fetch location, kindly turn on your GPS", Toast.LENGTH_SHORT).show();

        }   else {

            if (User.getCurrentUser() != null) {
                updateLocation();
            } else {

                get_location.startAnimation(animation);
                //     Toast.makeText(getApplicationContext(),"address"+lattude+ "\n" +longtude,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LocationActivity.this, LoginActivity.class);

                if (address!=null) {
                    intent.putExtra("address", address);
                }else{
                    intent.putExtra("address", "address");
                }
                if (area!=null) {
                    intent.putExtra("opt_address", area);
                }else{
                    intent.putExtra("opt_address", "empty");
                }

                if (city!=null) {
                    intent.putExtra("city", city);
                }else{
                    intent.putExtra("city", "city");
                }
                intent.putExtra("state", state);
                intent.putExtra("pincode", pincode);
                intent.putExtra("lat", lattude);
                intent.putExtra("long", longtude);
                startActivity(intent);
                Animatoo.animateZoom(LocationActivity.this);
                finish();
            }
        }
    }

    private void updateLocation() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        params.put("address", address);
        params.put("opt_address", area);
        params.put("u_type", "location");
        params.put("latitude", lattude);
        params.put("longitude", longtude);
        params.put("device_token", deviceid);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_DATA_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
//                UltimateProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                        User.setCurrentUser(user_obj);
                        //    Session_Bean.setCurrentSessiondetail(RecruiterUser.getCurrentRecruiterUser().getId(), 2);
                        Toast.makeText(LocationActivity.this, "Location Updated!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LocationActivity.this, FinalHomeActivity.class));
                        finish();
                        Animatoo.animateZoom(LocationActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(LocationActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(LocationActivity.this, FinalHomeActivity.class));
//        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    private boolean restorePrefData(){
        sharedPreferences=getApplicationContext().getSharedPreferences("location_pref",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit",false);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(80000); // two minute interval
        mLocationRequest.setFastestInterval(80000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                        Looper.myLooper());
                mGoogleMap.setMyLocationEnabled(true);
                head.setVisibility(View.VISIBLE);
                confirm_head.setVisibility(View.GONE);
                head.startAnimation(animation);

                mGoogleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        LatLng ltlng=new LatLng(location.getLatitude(),location.getLongitude());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                                ltlng, 18F);
                        mGoogleMap.animateCamera(cameraUpdate);
                        String fetch= getAddress(ltlng);
                        // Toast.makeText(getApplicationContext(),"Current address"+fetch,Toast.LENGTH_SHORT).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rippleBg.setVisibility(View.VISIBLE);
                                rippleBg.startRippleAnimation();
                            }
                        }, 2000);
                        title.setText(area);
                        add.setText(fetch);

                    }
                });

                //  Location location = mGoogleMap.getMyLocation();
//                mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                    @Override
//                    public void onMapClick(LatLng latLng) {
//
//                        MarkerOptions markerOptions = new MarkerOptions();
//                        markerOptions.position(latLng);
//                        //for adding custom marker....
////                        Marker marker;
////                        marker= mGoogleMap.addMarker(markerOptions.title(getAddress(latLng))
////                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).draggable(true).visible(true));
////                        marker.isInfoWindowShown();
//                        markerOptions.title(getAddress(latLng));
//                        mGoogleMap.clear();
//                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
//                                latLng, 19F);
//                        mGoogleMap.animateCamera(location);
//                        mGoogleMap.addMarker(markerOptions);
//                    }
//                });

            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                    Looper.myLooper());
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    private String getAddress(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(LocationActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            lattude= String.valueOf(latLng.latitude);
            longtude= String.valueOf(latLng.longitude);
            address = addresses.get(0).getAddressLine(0);
            area=addresses.get(0).getSubLocality();
            city=addresses.get(0).getLocality();
            state=addresses.get(0).getAdminArea();
            pincode=addresses.get(0).getPostalCode();

           //  Toast.makeText(getApplicationContext(),"address "+city+ "\n" +state+ "\n" +pincode,Toast.LENGTH_SHORT).show();
            return address;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"error"+e.getMessage(),Toast.LENGTH_SHORT).show();
            return e.getMessage();
        }

    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //move map camera
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latLng.latitude, latLng.longitude)).zoom(16).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    };

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(LocationActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        mGoogleMap.setMyLocationEnabled(true);

                    }

                } else {
                    // if not allow a permission, the application will exit
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                }
            }
        }
    }

}