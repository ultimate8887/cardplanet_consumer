package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.customer.cardplanet.R;
import com.razorpay.Checkout;
import com.razorpay.Order;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity implements PaymentResultWithDataListener {
    EditText amount;


    RazorpayClient razorpayClient;
    Order order;
    Checkout checkout;

    private String order_receipt_no = "Receipt No. " +  System.currentTimeMillis()/1000;
    private String order_reference_no = "Reference No. #" +  System.currentTimeMillis()/1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Checkout.preload(getApplicationContext());
        Button pay= (Button) findViewById(R.id.pay);

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        amount= (EditText) findViewById(R.id.amount);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Initialize client
                try {
                    razorpayClient = new RazorpayClient(getResources().getString(R.string.razorpay_key_id), getResources().getString(R.string.razorpay_secret_key));
                    Checkout.preload(getApplicationContext());
                    checkout = new Checkout();
                } catch (RazorpayException e) {
                    e.printStackTrace();
                }

                Map<String, String> headers = new HashMap<String, String>();
                razorpayClient.addHeaders(headers);

                try {
                    JSONObject orderRequest = new JSONObject();

                    String pay=amount.getText().toString();
                    double amm= Double.parseDouble(pay);
                    int total= (int) (amm * 100);

                    orderRequest.put("amount", total); // amount in the smallest currency unit
                    orderRequest.put("currency", "INR");
                    orderRequest.put("receipt", order_receipt_no);
                    orderRequest.put("payment_capture", true);

                    order = razorpayClient.Orders.create(orderRequest);

                    startpayments(order);


                } catch (RazorpayException e) {
                    // Handle Exception
                    System.out.println(e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

               // startpayments(order);



            }
        });
    }

    private void startpayments(Order order) {
        checkout.setKeyID(getResources().getString(R.string.razorpay_key_id));

        Checkout checkout = new Checkout();

        /**
         * Set your logo here
         */
       // checkout.setImage(R.drawable.rzp_name_logo);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        try {

        JSONObject options = new JSONObject();
        options.put("name", "Card Planet Testing");
       // options.put("description", "Reference No. #123456");
            options.put("description", "Testing Payments");
        options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
        options.put("order_id", order.get("id"));//from response of step 3.
        options.put("theme.color", "#3399cc");

        options.put("currency", "INR");

        String pay=amount.getText().toString();
        double amm= Double.parseDouble(pay);
        int total= (int) (amm * 100);
        options.put("amount", total);//pass amount in currency subunits

            JSONObject prefill = new JSONObject();
            prefill.put("email", "uic.16mca8186@gmail.com");
            prefill.put("contact","7018357513");
//        JSONObject retryObj = new JSONObject();
//        retryObj.put("enabled", true);
//        retryObj.put("max_count", 4);
        options.put("prefill", prefill);

        checkout.open(activity, options);

    } catch(Exception e) {
        Log.e("TAG", "Error in starting Razorpay Checkout", e);
            Toast.makeText(getApplicationContext(),"Error "+ e.getMessage(),Toast.LENGTH_LONG).show();

    }
    }



    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Toast.makeText(getApplicationContext()," order_id "+ paymentData.getOrderId()+ "  pay_id "+paymentData.getPaymentId(),Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
        amount.setText("Payment ID: " + s);
        amount.append("\nOrder ID: " + order.get("id"));
        amount.append("\n" + order_reference_no);
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        Toast.makeText(getApplicationContext()," "+ s,Toast.LENGTH_LONG).show();
        amount.setText("Error: " + s);
    }
}