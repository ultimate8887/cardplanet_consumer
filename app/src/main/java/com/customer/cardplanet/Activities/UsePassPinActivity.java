package com.customer.cardplanet.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.hanks.passcodeview.PasscodeView;

import java.util.Arrays;
import java.util.List;

public class UsePassPinActivity extends AppCompatActivity {

    PasscodeView passcodeView;
    private TextView headingLabel,attempt;
    Window window;
    int check=0;
    private int VERIFY_NUMBER = 1000;
    TextView btnProceed;
    private Animation animation,animation1;
    private LinearLayout fingerprintss;
    CardPlanetProgress cancelProgressBar;
    String get_number="";
    public static final String TAG = "PinLockView";



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_finger_print);

//
        cancelProgressBar=new CardPlanetProgress(this);
        window=getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.sender));
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.long_fade_animation);

        passcodeView=findViewById(R.id.passcodeView);
     //   getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        headingLabel = (TextView) findViewById(R.id.headingLabel);
        headingLabel.setText("Hi, "+ User.getCurrentUser().getName());

        attempt = (TextView) findViewById(R.id.attempt);
        btnProceed = (TextView) findViewById(R.id.btnProceed);

        fingerprintss=(LinearLayout) findViewById(R.id.fingerprintss);


        passcodeView.getLocalPasscode();
        passcodeView.setActivated(false);

        // Toast.makeText(getApplicationContext(),"Password"+User.getCurrentUser().getDevice_pin(),Toast.LENGTH_LONG).show();

//      if (check<=3) {

        passcodeView.setPasscodeLength(4)
                  .setLocalPasscode(User.getCurrentUser().getDevice_pin())
                  .setListener(new PasscodeView.PasscodeViewListener() {


                      @Override
                      public void onFail() {


                          passcodeView.setWrongInputTip("wwww");
                          Log.e("wrong", String.valueOf(check));
                        //  headingLabel.setText(check);
                         // Toast.makeText(getApplicationContext(), "Password is Wrong" + check, Toast.LENGTH_LONG).show();
                      }

                      @Override
                      public void onSuccess(String number) {
                          Intent intent = new Intent(UsePassPinActivity.this, FinalHomeActivity.class);
                          startActivity(intent);
                          finish();
                          Animatoo.animateZoom(UsePassPinActivity.this);
                      //    passcodeView.setWrongInputTip("aaaa");
                    //      Toast.makeText(getApplicationContext(), "Check" + check, Toast.LENGTH_LONG).show();


                      }


                  });

          passcodeView.getLocalPasscode();
          passcodeView.getWrongInputTip();

//      }else{
       //  Toast.makeText(getApplicationContext(), "Password is Wrong" + check, Toast.LENGTH_LONG).show();
//
//      }


        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnProceed.startAnimation(animation);
                cancelProgressBar.show();
                List<AuthUI.IdpConfig> providers = Arrays.asList(
                        new AuthUI.IdpConfig.PhoneBuilder().build());

// Create and launch sign-in intent
                startActivityForResult(
                        AuthUI.getInstance()
                                .createSignInIntentBuilder()
                                .setTheme(R.style.phoneTheme)
                                .setLogo(R.drawable.logo)
                                .setAvailableProviders(providers)
                                .setIsSmartLockEnabled(false,
                                        true)
                                .build(),
                        VERIFY_NUMBER);

                AuthUI.getInstance()
                        .signOut(UsePassPinActivity.this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                //              Toast.makeText(LoginActivity.this,"SignOut",Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cancelProgressBar.dismiss();
        if (requestCode == VERIFY_NUMBER) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                get_number=response.getPhoneNumber();

                //  Toast.makeText(LoginActivity.this,"Number "+get_number,Toast.LENGTH_SHORT).show();
                Log.e("number",get_number);
                numberCheck(get_number);
            } else {
                Toast.makeText(UsePassPinActivity.this,"Verification Failed, Try again.",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void numberCheck(String get_number) {
        cancelProgressBar.show();
        String num="+91"+ User.getCurrentUser().getMobile();
        if (num.equalsIgnoreCase(get_number)) {
            cancelProgressBar.dismiss();
            Intent intent = new Intent(UsePassPinActivity.this, PinSetupActivity.class);
            startActivity(intent);
            finish();
        } else{
        cancelProgressBar.dismiss();
        Toast.makeText(UsePassPinActivity.this,"Registered Number Not Matched, Try again",Toast.LENGTH_SHORT).show();

    }
    }


}