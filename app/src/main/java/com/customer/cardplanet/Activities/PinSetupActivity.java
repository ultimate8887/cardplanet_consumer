package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.CardPlanetCustomerApp;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.FirebaseGetDeviceToken;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.goodiebag.pinview.Pinview;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PinSetupActivity extends AppCompatActivity {


    Pinview pin,confirm_pinview;
    final String[] final_pin = new String[1];
    final String[] final_pins = new String[1];
    private JSONObject user_obj;
    private String deviceid;
    private int deviceIdChecked = 0;
    SharedPreferences pref;
    CardPlanetProgress cancelProgressBar;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    private Animation animation,animation1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_setup);
        ButterKnife.bind(this);
        cancelProgressBar=new CardPlanetProgress(this);
        pin = new Pinview(this);
        confirm_pinview=new Pinview(this);
        getDeviceTocken();
        CircularImageView imageView = (CircularImageView) findViewById(R.id.my_image);
        TextView c_name = (TextView) findViewById(R.id.c_name);
        c_name.setText("Hi, "+ User.getCurrentUser().getName());
        if (User.getCurrentUser().getProfilepic() != null) {
            Picasso.with(getApplicationContext()).load(User.getCurrentUser().getProfilepic() ).
                    placeholder(this.getResources().getDrawable(R.drawable.person_1)).into(imageView);
        }

        pin = (Pinview) findViewById(R.id.pinview);
        confirm_pinview = (Pinview) findViewById(R.id.confirm_pinview);
        pin.showCursor(true);
//        pin.setCursorShape(R.drawable.example_cursor);
        confirm_pinview.showCursor(true);

        confirm_pinview.setCursorColor(Color.parseColor("#ffffff"));

        pin.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
//                Toast.makeText(FinalHomeActivity.this, pinview.getValue(), Toast.LENGTH_SHORT).show();
                final_pin[0] =pinview.getValue();
            }
        });
        confirm_pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
//                Toast.makeText(FinalHomeActivity.this, pinview.getValue(), Toast.LENGTH_SHORT).show();


                final_pins[0]=pinview.getValue();

                if (final_pin[0].equalsIgnoreCase(final_pins[0])){
//                    Toast.makeText(PinSetupActivity.this, "Matched Pin "+final_pins[0], Toast.LENGTH_SHORT).show();
               //    c_name.setText(final_pins[0]);
                  //  Toast.makeText(PinSetupActivity.this, "Matched Pin "+c_name.getText().toString(), Toast.LENGTH_SHORT).show();

                    confirm_pinview.setPinBackgroundRes(R.drawable.r_pin_lyt);
                    pin.setPinBackgroundRes(R.drawable.r_pin_lyt);
                    updatePin();
                } else{
                    Toast.makeText(PinSetupActivity.this, "Re-Enter Pin Not Matched", Toast.LENGTH_SHORT).show();
                    confirm_pinview.setPinBackgroundRes(R.drawable.w_pin_lyt);
                }
            }
        });
    }

    private void getDeviceTocken() {

        pref = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");
        if (deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
        }
        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }


    private void checkDeviceIDValid() {

        if (deviceid == null && deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceTocken();
            } else {
                //   showSnackBar("Please wait.....");
                Toast.makeText(PinSetupActivity.this, "Please wait.....", Toast.LENGTH_SHORT).show();
                FirebaseGetDeviceToken.getDeviceToken();
            }
        } else {
            Toast.makeText(PinSetupActivity.this, "Error to getting device token.....", Toast.LENGTH_SHORT).show();
        }
    }

    private void updatePin() {
          cancelProgressBar.show();
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", User.getCurrentUser().getId());
            params.put("pin", final_pin[0]);
            params.put("u_type", "pin");
            params.put("device_token", deviceid);

            ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_DATA_URL, new ApiHandler.ApiCallback() {
                @Override
                public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
                    cancelProgressBar.dismiss();
                    if (error == null) {
                        try {
                            user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                            User.setCurrentUser(user_obj);
                            //    Session_Bean.setCurrentSessiondetail(RecruiterUser.getCurrentRecruiterUser().getId(), 2);
                            Toast.makeText(PinSetupActivity.this, "Pin Setup Successfully!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(PinSetupActivity.this, FinalHomeActivity.class));
                            finish();
                            Animatoo.animateZoom(PinSetupActivity.this);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(PinSetupActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, this, params);

    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        commonBack();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure, You want to exit?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

}