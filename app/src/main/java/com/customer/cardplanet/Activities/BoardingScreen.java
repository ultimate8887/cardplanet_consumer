package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.AdapterClasses.OnBoardingAdapter;
import com.customer.cardplanet.BeanClasses.BoardingData;
import com.customer.cardplanet.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class BoardingScreen extends AppCompatActivity {

    OnBoardingAdapter onBoardingAdapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    TextView next;
    int position;
    SharedPreferences sharedPreferences;
    private Animation animation;
    Window window;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (restorePrefData()){
            startActivity(new Intent(BoardingScreen.this, LocationActivity.class));
            finish();
          //  Animatoo.animateShrink(this);
        }

        setContentView(R.layout.activity_boarding_screen);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.btn_blink_animation);
        viewPager=findViewById(R.id.pager);
        tabLayout=findViewById(R.id.tab_layout);
        next=findViewById(R.id.next);
//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.white));
        List<BoardingData> boardingDataList=new ArrayList<>();

        boardingDataList.add(new BoardingData("Card",R.raw.onboarding,"Select which card is right for you. Everyone" +
                " take advantage of Card benefits and discounts."));
        boardingDataList.add(new BoardingData("Easy Access",R.raw.scanner,"We learned the art to invest smart, " +
                "just like us, you can kick start with Card Planet Click 2 Wealth."));
        boardingDataList.add(new BoardingData("Discounts",R.raw.discount,"We promise to discount without insurance claim. " +
                "Our aim to provide best-in-class service to our customers."));
        setOnBoardingAdapter(boardingDataList);

        position=viewPager.getCurrentItem();
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next.startAnimation(animation);
                if (position < boardingDataList.size()){
                    position++;
                    viewPager.setCurrentItem(position);
                }
                if (position == boardingDataList.size()){
                    savePrefData();
                    startActivity(new Intent(BoardingScreen.this, LocationActivity.class));
                    finish();
                    Animatoo.animateZoom(BoardingScreen.this);
                }
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                position=tab.getPosition();
                if(position == boardingDataList.size() - 1){
                    next.setText("Get Started");
                } else{
                    next.setText("Next");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setOnBoardingAdapter(List<BoardingData> boardingData){

        onBoardingAdapter=new OnBoardingAdapter(this,boardingData);
        viewPager.setAdapter(onBoardingAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void savePrefData(){
        sharedPreferences=getApplicationContext().getSharedPreferences("boarding_pref",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("IsFirstTimeVisit",true);
        editor.apply();
    }

    private boolean restorePrefData(){
        sharedPreferences=getApplicationContext().getSharedPreferences("boarding_pref",MODE_PRIVATE);
        return sharedPreferences.getBoolean("IsFirstTimeVisit",false);
    }
}