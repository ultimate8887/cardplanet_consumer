package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.AdapterClasses.MerchantAdapter;
import com.customer.cardplanet.AdapterClasses.SecondCategoryAdapter;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.Merchant_Category;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewAllCategoryActivity extends AppCompatActivity implements SecondCategoryAdapter.CateMethodCallBack {

    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.noData)
    RelativeLayout noData;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleCate;
    ArrayList<Merchant_Category> subCateList = new ArrayList<>();
    private SecondCategoryAdapter cateViewAdapter;
    private GridLayoutManager gridLayoutManager;
    private Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_category);
        ButterKnife.bind(this);
        txtTitle.setText("Filter Category Wise");
        //set animation on adapter...
        cardPlanetProgress=new CardPlanetProgress(this);
        int resId1 = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, resId1);
        viewRecycleCate.setLayoutAnimation(controller);
        //end animation...
        gridLayoutManager = new GridLayoutManager(this,3);
        viewRecycleCate.setLayoutManager(gridLayoutManager);
        cateViewAdapter = new SecondCategoryAdapter(subCateList, this,this);
        viewRecycleCate.setAdapter(cateViewAdapter);
        fetchJCateList();

    }


    private void fetchJCateList() {
        cardPlanetProgress.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("view","");
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.PRODUCT_CATE_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cardPlanetProgress.dismiss();
                if (error== null){
                    try {
                        if (subCateList != null)
                            subCateList.clear();

                        subCateList =Merchant_Category.parseCategoryArray(jsonObject.getJSONArray("ProdCate_data"));
                        cateViewAdapter.setSubCateList(subCateList);
                        cateViewAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleCate.getAdapter().notifyDataSetChanged();
                        viewRecycleCate.scheduleLayoutAnimation();
                        //-----------end------------

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    subCateList.clear();
                    cateViewAdapter.setSubCateList(subCateList);
                    cateViewAdapter.notifyDataSetChanged();
                 //   Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        }, this, params);
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @Override
    public void cateMethod(Merchant_Category subCateBean) {
        Intent intent= new Intent(ViewAllCategoryActivity.this, FilterCateWiseActivity.class);
        intent.putExtra("cate_id",subCateBean.getId());
        intent.putExtra("cate_name",subCateBean.getName());
        startActivity(intent);
        Animatoo.animateShrink(this);
    }


}