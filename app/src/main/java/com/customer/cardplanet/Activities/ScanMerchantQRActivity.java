package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanMerchantQRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {


    CardPlanetProgress cancelProgressBar;
    private ZXingScannerView mScannerView;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.image)
    ImageView images;
    @BindView(R.id.hover)
    TextView hover;
    @BindView(R.id.lightButton)
    ImageView flashImageView;
    private Animation animation;
    private boolean flashState = false;
    ArrayList<MerchantBean> merchantBeans = new ArrayList<>();
    String m_name="",m_id="",image="",discount="",f_id="";

    Dialog dialogLog,dialogLog1;
    ImageView imageViewBackground;
    TextView title;
    TextView disc;
    EditText amount;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_merchant_q_r);
        ButterKnife.bind(this);
        transparentfullscreen();
      //  adapter = new CardDetailAdapter(this, mData);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        ActivityCompat.requestPermissions(ScanMerchantQRActivity.this,
                new String[]{Manifest.permission.CAMERA},
                1);
       // adapter = new CardDetailAdapter(this, mData);
        cancelProgressBar=new CardPlanetProgress(this);
        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(ScanMerchantQRActivity.this);
        contentFrame.addView(mScannerView);

        flashImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScannerView.setFlash(true);
                if(flashState==false) {
                    v.setBackgroundResource(R.drawable.flash);
                    Toast.makeText(getApplicationContext(), "Flashlight turned on", Toast.LENGTH_SHORT).show();
                    mScannerView.setFlash(true);
                    flashState = true;
                }else if(flashState) {
                    v.setBackgroundResource(R.drawable.turn_off);
                    Toast.makeText(getApplicationContext(), "Flashlight turned off", Toast.LENGTH_SHORT).show();
                    mScannerView.setFlash(false);
                    flashState = false;
                }
            }
        });


    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        finish();
    }

    @OnClick(R.id.image)
    public void imagessss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        images.startAnimation(animation);

        Intent pickIntent = new Intent(Intent.ACTION_PICK);
        pickIntent.setDataAndType( android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

        startActivityForResult(pickIntent, 111);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            //the case is because you might be handling multiple request codes here
            case 111:
                if (data == null || data.getData() == null) {
                    Log.e("TAG", "The uri is null, probably the user cancelled the image selection process using the back button.");
                    return;
                }
                Uri uri = data.getData();
                InputStream inputStream = null;
                try {
                    inputStream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                if (bitmap == null) {
                    Log.e("TAG", "uri is not a bitmap," + uri.toString());
                    return;
                }
                int width = bitmap.getWidth(), height = bitmap.getHeight();
                int[] pixels = new int[width * height];
                bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
                bitmap.recycle();
                bitmap = null;
                RGBLuminanceSource source = new RGBLuminanceSource(width, height, pixels);
                BinaryBitmap bBitmap = new BinaryBitmap(new HybridBinarizer(source));
                MultiFormatReader reader = new MultiFormatReader();
                Result result = null;
                try {
                    result = reader.decode(bBitmap);
                } catch (NotFoundException e) {
                    e.printStackTrace();
                }
                //searchStudent(result.getText());
                //  Toast.makeText(this, "The content of the QR image is: " + result.getText(), Toast.LENGTH_SHORT).show();

                if(result!=null) {
                    searchMerchant(result.getText());
                }else{
                    Toast.makeText(this, "Incorrect Image, Kindly Choose Correct QR Image", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void transparentfullscreen() {

        Window window=this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        window.setStatusBarColor(Color.TRANSPARENT);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
      //  Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        hover.setText(rawResult.getText());
        searchMerchant(rawResult.getText());
    }

    private void searchMerchant(String id) {
        cancelProgressBar.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("cate_id", "");
        params.put("view", "scan");
        params.put("m_id", id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MERCHANT_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (merchantBeans != null)
                            merchantBeans.clear();
                        merchantBeans = MerchantBean.parseMerchantBeanArray(jsonObject.getJSONArray("merchant_data"));
                      //  Toast.makeText(getApplicationContext(), merchantBeans.get(0).getName(), Toast.LENGTH_SHORT).show();
                       m_name=merchantBeans.get(0).getName();
                       m_id=merchantBeans.get(0).getId();
                       image=merchantBeans.get(0).getImage();
                        discount=merchantBeans.get(0).getDiscount();
                        f_id=merchantBeans.get(0).getF_id();
                       openDialog();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    merchantBeans.clear();
                        Toast.makeText(getApplicationContext(), "This Merchant Is Not Exists!", Toast.LENGTH_SHORT).show();
                }

            }
        }, this, params);


    }

    private void openDialog() {

        dialogLog1 = new Dialog(this);
        dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(false);
        dialogLog1.setContentView(R.layout.proceed_dialog);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        CircularImageView imageView = (CircularImageView) dialogLog1.findViewById(R.id.myimage);
        title=(TextView) dialogLog1.findViewById(R.id.title);
        title.setText(m_name);
        //if (!image.equalsIgnoreCase("")) {
            Picasso.with(this).load(image).placeholder(R.drawable.person_1).into(imageView);
     //   Log.e("image",image);

        amount = (EditText) dialogLog1.findViewById(R.id.amount);
        TextView btnProceed = (TextView) dialogLog1.findViewById(R.id.btnProceed);
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!amount.getText().toString().equalsIgnoreCase("0") && !amount.getText().toString().isEmpty()) {
                    dialogLog1.dismiss();
                    Intent intent = new Intent(ScanMerchantQRActivity.this, PayToMerchantActivity.class);
                    intent.putExtra("m_name", m_name);
                    intent.putExtra("m_id", m_id);
                    intent.putExtra("image", image);
                    intent.putExtra("discount", discount);
                    intent.putExtra("f_id", f_id);

                    intent.putExtra("amount", amount.getText().toString());
                    startActivity(intent);
                    Animatoo.animateZoom(ScanMerchantQRActivity.this);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Kindly Enter Amount",Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageView btnNo = (ImageView) dialogLog1.findViewById(R.id.imgBackmsg);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog1.dismiss();
                finish();
            }
        });
        dialogLog1.show();
    }
}