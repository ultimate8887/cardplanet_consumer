package com.customer.cardplanet.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.Utils;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {


    @BindView(R.id.name)
    TextView cust_name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.password)
    TextView password;

    @BindView(R.id.common_bg)
    ImageView basic_lyt;

    @BindView(R.id.m1)
    LinearLayout m1;
    @BindView(R.id.m2)
    LinearLayout m2;
    @BindView(R.id.m3)
    LinearLayout m3;


    @BindView(R.id.card_number)
    TextView card_number;
    @BindView(R.id.card_type)
    TextView card_type;
    @BindView(R.id.c_holder)
    TextView c_holder;
    @BindView(R.id.qr_code_show)
    ImageView qr_code_show;


    @BindView(R.id.name1)
    TextView name1;
    @BindView(R.id.dob1)
    TextView dob1;
    @BindView(R.id.gender1)
    TextView gender1;
    @BindView(R.id.mobile1)
    TextView mobile1;
    @BindView(R.id.relation1)
    TextView relation1;

    @BindView(R.id.name2)
    TextView name2;
    @BindView(R.id.dob2)
    TextView dob2;
    @BindView(R.id.gender2)
    TextView gender2;
    @BindView(R.id.mobile2)
    TextView mobile2;
    @BindView(R.id.relation2)
    TextView relation2;

    @BindView(R.id.name3)
    TextView name3;
    @BindView(R.id.dob3)
    TextView dob3;
    @BindView(R.id.gender3)
    TextView gender3;
    @BindView(R.id.mobile3)
    TextView mobile3;
    @BindView(R.id.relation3)
    TextView relation3;
    ImageView imageViewBackground;
    TextView title;
    TextView disc;
    private Bitmap bitmap;
    @BindView(R.id.profile)
    CircularImageView profile;
    private Animation animation;
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    Window window;
    private QRGEncoder qrgEncoder;
    RelativeLayout btnNo;
    Dialog dialogLog,dialogLog1;
    @BindView(R.id.validUpTo)
    TextView validUpTo;
    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    String inputValue;

    @BindView(R.id.document_no)
    TextView document_no;
    @BindView(R.id.document_link)
    TextView document_link;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        User user=User.getCurrentUser();
//        window=this.getWindow();
//        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        cust_name.setTextColor(Color.parseColor("#000000"));

        if (User.getCurrentUser().getGender().equalsIgnoreCase("male")) {
            cust_name.setText(user.getName()+"\n s/o \n"+user.getFather_name());
        }else{
            cust_name.setText(user.getName()+"\n d/o \n"+user.getFather_name());
        }


        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        if (User.getCurrentUser().getProfilepic() != null) {
            Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.person_1).into(profile);
        } else {
            Picasso.with(this).load(R.drawable.person_1).into(profile);
        }

        if (user.getEmailid()!=null){
            email.setText(user.getEmailid());
        }else {
            email.setText("Not Mentioned");
        }


        if (user.getDoc_type()!=null){
            document_no.setText(user.getAadharno()+"("+user.getDoc_type()+")");
        }else {
            document_no.setText("Not Mentioned");
        }

        if (user.getDoc_upload()!=null){
            document_link.setText("Click Here to View Document File");
        }else {
            document_link.setText("Not Mentioned");
        }


        if (user.getEmailid()!=null){
            mobile.setText(user.getMobile());
        }else {
            mobile.setText("Not Mentioned");
        }

        if (user.getAddress()!=null){
            location.setText(user.getAddress()+", "+user.getCity()+", "+user.getState()+", "+user.getCountry()+", "+user.getPincode());
        }else {
            location.setText("Not Mentioned");
        }

        if (User.getCurrentUser() !=null){
            inputValue=User.getCurrentUser().getId();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();
            // Setting Bitmap to ImageView
            qr_code_show.setImageBitmap(bitmap);

        }

            if (user.getFamily1name()!=null){
            name1.setText(user.getFamily1name());
            String title = getColoredSpanned("<b>" +"Name- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getFamily1name() , "#7D7D7D");
            name1.setText(Html.fromHtml(title + " " + Name));
        }else{
            name1.setText("Not Mentioned");
        }
        if (user.getFamily1dob()!=null){
           // dob1.setText(user.getFamily1dob());
            String title = getColoredSpanned("<b>" +"DOB- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned( Utils.dateFormat(user.getFamily1dob()), "#7D7D7D");
            dob1.setText(Html.fromHtml(title + " " + Name));
        }else{
            dob1.setText("Not Mentioned");
        }
        if (user.getFamily1gender()!=null){
           // gender1.setText(user.getFamily1gender());
            String title = getColoredSpanned("<b>" +"Gender- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getFamily1gender() , "#7D7D7D");
            gender1.setText(Html.fromHtml(title + " " + Name));
        }else{
            gender1.setText("Not Mentioned");
        }
        if (user.getFamily1mobile()!=null){
          //  mobile1.setText(user.getFamily1mobile());
            String title = getColoredSpanned("<b>" +"Mobile- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getFamily1mobile() , "#7D7D7D");
            mobile1.setText(Html.fromHtml(title + " " + Name));
        }else{
            mobile1.setText("Not Mentioned");
        }
        if (user.getFamily1relation()!=null){
            // relation1.setText(user.getFamily1relation());
            String title = getColoredSpanned("<b>" +"Relation- "+"</b>", "#5A5C59");
            String Name = getColoredSpanned(user.getFamily1relation() , "#7D7D7D");
            relation1.setText(Html.fromHtml(title + " " + Name));
        }else{
            relation1.setText("Not Mentioned");
        }

        if (user.getName()!=null){
            // relation1.setText(user.getFamily1relation());
            String title = getColoredSpanned("<b>" +"Name- "+"</b>", "#ffffff");
            String Name = getColoredSpanned(user.getName() , "#ffffff");
            c_holder.setText(Html.fromHtml(title + " " + Name));
        }else{
            c_holder.setText("Not Mentioned");
        }

        if (user.getRenewal_date()!=null){
            // relation1.setText(user.getFamily1relation());
            String title = getColoredSpanned("<b>" +"Valid UpTo- "+"</b>", "#ffffff");
            String Name = getColoredSpanned(Utils.dateFormat(user.getRenewal_date()) , "#ffffff");
            validUpTo.setText(Html.fromHtml(title + " " + Name));
        }else{
            validUpTo.setText("Not Mentioned");
        }
        card_number.setText(user.getCard_no());

        username.setText(user.getMobile());
        password.setText(user.getPassword());

        if (user.getCard_type().equalsIgnoreCase("basic")){
            m1.setVisibility(View.VISIBLE);
            m2.setVisibility(View.GONE);
            m2.setVisibility(View.GONE);

            basic_lyt.setBackgroundResource(R.drawable.basic_card);

                // relation1.setText(user.getFamily1relation());
                String title = getColoredSpanned("<b>" +"Card Type- "+"</b>", "#ffffff");
                String Name = getColoredSpanned("Basic", "#ffffff");
                card_type.setText(Html.fromHtml(title + " " + Name));


         //validUpTo.setText(Utils.dateFormat(user.getDateofgenetatecard())+" to "+Utils.dateFormat(user.getRenewal_date()));

        }else{
            m1.setVisibility(View.VISIBLE);
            m2.setVisibility(View.VISIBLE);
            m2.setVisibility(View.VISIBLE);

            basic_lyt.setBackgroundResource(R.drawable.standard_card);
                String title1 = getColoredSpanned("<b>" +"Card Type- "+"</b>", "#ffffff");
                String Name1 = getColoredSpanned("Standard", "#ffffff");
                card_type.setText(Html.fromHtml(title1 + " " + Name1));


        //    validUpTo1.setText(Utils.dateFormat(user.getDateofgenetatecard())+" to "+Utils.dateFormat(user.getRenewal_date()));

            if (user.getFamily2name()!=null){
             //   name2.setText(user.getFamily2name());
                String title = getColoredSpanned("<b>" +"Name- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily2name() , "#7D7D7D");
                name2.setText(Html.fromHtml(title + " " + Name));
            }else{
                name2.setText("Not Mentioned");
            }
            if (user.getFamily2dob()!=null){
               // dob2.setText(user.getFamily2dob());
                String title = getColoredSpanned("<b>" +"DOB- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned( Utils.dateFormat(user.getFamily2dob()), "#7D7D7D");
                dob2.setText(Html.fromHtml(title + " " + Name));
            }else{
                dob2.setText("Not Mentioned");
            }
            if (user.getFamily2gender()!=null){
               // gender2.setText(user.getFamily2gender());
                String title = getColoredSpanned("<b>" +"Gender- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily2gender() , "#7D7D7D");
                gender2.setText(Html.fromHtml(title + " " + Name));
            }else{
                gender2.setText("Not Mentioned");
            }
            if (user.getFamily2mobile()!=null){
              //  mobile2.setText(user.getFamily2mobile());
                String title = getColoredSpanned("<b>" +"Mobile- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily2mobile() , "#7D7D7D");
                mobile2.setText(Html.fromHtml(title + " " + Name));
            }else{
                mobile2.setText("Not Mentioned");
            }
            if (user.getFamily2relation()!=null){
               // relation2.setText(user.getFamily2relation());
                String title = getColoredSpanned("<b>" +"Relation- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily2relation() , "#7D7D7D");
                relation2.setText(Html.fromHtml(title + " " + Name));
            }else{
                relation2.setText("Not Mentioned");
            }


            if (user.getFamily3name()!=null){
             //   name3.setText(user.getFamily3name());
                String title = getColoredSpanned("<b>" +"Name- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily3name() , "#7D7D7D");
                name3.setText(Html.fromHtml(title + " " + Name));
            }else{
                name3.setText("Not Mentioned");
            }
            if (user.getFamily3dob()!=null){
              //  dob3.setText(user.getFamily3dob());
                String title = getColoredSpanned("<b>" +"DOB- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned( Utils.dateFormat(user.getFamily3dob()), "#7D7D7D");
                dob3.setText(Html.fromHtml(title + " " + Name));
            }else{
                dob3.setText("Not Mentioned");
            }
            if (user.getFamily3gender()!=null){
             //   gender3.setText(user.getFamily3gender());
                String title = getColoredSpanned("<b>" +"Gender- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily3gender() , "#7D7D7D");
                gender3.setText(Html.fromHtml(title + " " + Name));
            }else{
                gender3.setText("Not Mentioned");
            }
            if (user.getFamily3mobile()!=null){
            //    mobile3.setText(user.getFamily3mobile());
                String title = getColoredSpanned("<b>" +"Mobile- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily3mobile() , "#7D7D7D");
                mobile3.setText(Html.fromHtml(title + " " + Name));
            }else{
                mobile3.setText("Not Mentioned");
            }
            if (user.getFamily3relation()!=null){
               // relation3.setText(user.getFamily3relation());
                String title = getColoredSpanned("<b>" +"Relation- "+"</b>", "#5A5C59");
                String Name = getColoredSpanned(user.getFamily3relation() , "#7D7D7D");
                relation3.setText(Html.fromHtml(title + " " + Name));
            }else{
                relation3.setText("Not Mentioned");
            }

        }

    }

    @OnClick(R.id.edit_profile)
    public void edit_profile() {
        edit_profile.startAnimation(animation);
        startActivity(new Intent(ProfileActivity.this,UpdateUserDetailsActivity.class));
        Animatoo.animateZoom(this);
        finish();
    }

    @OnClick(R.id.document_link)
    public void document_linkkk() {
        document_link.startAnimation(animation);
        Dialog dialogLog1 = new Dialog(this);
        dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(true);
        dialogLog1.setContentView(R.layout.imageview_dialog);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        PhotoView imageView = (PhotoView) dialogLog1.findViewById(R.id.myimage);

        //if (!image.equalsIgnoreCase("")) {
        Picasso.with(this).load(User.getCurrentUser().getDoc_upload()).placeholder(R.drawable.categories).into(imageView);
        //   Log.e("image",image);

        RelativeLayout btnNo = (RelativeLayout) dialogLog1.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog1.dismiss();
                finish();
            }
        });
        dialogLog1.show();

    }

    @OnClick(R.id.qr_code_show)
    public void qr_code_showwww() {
        qr_code_show.startAnimation(animation);
        Animatoo.animateShrink(this);

        dialogLog1 = new Dialog(this);
        dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(true);
        dialogLog1.setContentView(R.layout.qr_dialoge);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView imageView = (ImageView) dialogLog1.findViewById(R.id.myimage);

        if (User.getCurrentUser() !=null){
             inputValue=User.getCurrentUser().getId();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();
            // Setting Bitmap to ImageView
            imageView.setImageBitmap(bitmap);
        }else {
            dialogLog1.dismiss();
        }
        btnNo = (RelativeLayout) dialogLog1.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog1.dismiss();

            }
        });
        dialogLog1.show();
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        common();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        common();
    }

    private void common() {
        startActivity(new Intent(ProfileActivity.this, UserMenuActivity.class));
        finish();
      //  Animatoo.animateZoom(ProfileActivity.this);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
}