package com.customer.cardplanet.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.Utils;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateUserDetailsActivity extends AppCompatActivity {


    @BindView(R.id.profile)
    CircularImageView myimage;

    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    @BindView(R.id.doc_image)
    ImageView doc_image;
    @BindView(R.id.edit_document)
    ImageView edit_document;

    @BindView(R.id.spinner)
    Spinner spinner;
    String result="",type="",get_number="";
    User user;

    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.edtFather)
    EditText edtFather;
    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @BindView(R.id.edtDocNo)
    EditText edtDocNo;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtName)
    EditText edtName;
    private JSONObject user_obj;
    Dialog dialogLog,dialogLog1;
    @BindView(R.id.submit)
    TextView submit;
    private Animation animation;
    private int selection;
    private Bitmap userBitmap=null, userBitmapProf=null;
    private int VERIFY_NUMBER = 1000;
    CardPlanetProgress cancelProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user_details);
        ButterKnife.bind(this);
        cancelProgressBar=new CardPlanetProgress(this);
        user=User.getCurrentUser();
        setUserData();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        ArrayList<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("Aadhaar");
        spinnerArray.add("Passport");
        spinnerArray.add("Pan Card");
        spinnerArray.add("Voter Card");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        spinner.setAdapter(spinnerArrayAdapter);
        type=user.getDoc_type();
       if (type.equalsIgnoreCase("Aadhar")){
           spinner.setSelection(0, true);
       }
       else if (type.equalsIgnoreCase("Passport")) {
            spinner.setSelection(1, true);
        } else if (type.equalsIgnoreCase("Pan Card")){
           spinner.setSelection(2, true);
       }else{
           spinner.setSelection(3, true);
       }



        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 result = spinner.getSelectedItem().toString();
                //result=spinnerArray.get(i);
             //   Toast.makeText(getApplicationContext(),"Select "+result,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_profile)
    public void edit_profile() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_profile.startAnimation(animation);

        selection=1;

        if (CropImage.isExplicitCameraPermissionRequired(UpdateUserDetailsActivity.this)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            if (!Utils.checkPermission(UpdateUserDetailsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                UpdateUserDetailsActivity.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.READ_PERMISSION);
            } else {
                CropImage.startPickImageActivity(UpdateUserDetailsActivity.this);
            }
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.edit_document)
    public void edit_document() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        edit_document.startAnimation(animation);

        selection=2;

        if (CropImage.isExplicitCameraPermissionRequired(UpdateUserDetailsActivity.this)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            if (!Utils.checkPermission(UpdateUserDetailsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                UpdateUserDetailsActivity.this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.READ_PERMISSION);
            } else {
                CropImage.startPickImageActivity(UpdateUserDetailsActivity.this);
            }
        }

    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        cancelProgressBar.dismiss();
        if (requestCode == VERIFY_NUMBER) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                get_number=response.getPhoneNumber();
                Toast.makeText(UpdateUserDetailsActivity.this,"Number "+get_number,Toast.LENGTH_SHORT).show();
                String substring=get_number.substring(3);
                edtPhone.setText(substring);
            } else {
                Toast.makeText(UpdateUserDetailsActivity.this, "Sign in failed", Toast.LENGTH_SHORT).show();
            }

        }

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
                    Uri imageUri = CropImage.getPickImageResultUri(UpdateUserDetailsActivity.this, data);
                    CropImage.activity(imageUri).setCropShape(CropImageView.CropShape.RECTANGLE).setAspectRatio(1, 1)
                            .start(UpdateUserDetailsActivity.this);
                    break;

                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    if (selection==1){
                        userBitmap = Utils.getBitmapFromUri(UpdateUserDetailsActivity.this, resultUri, 2048);
                        myimage.setImageBitmap(userBitmap);
                    } if (selection==2){
                     userBitmapProf = Utils.getBitmapFromUri(UpdateUserDetailsActivity.this, resultUri, 2048);
                     doc_image.setImageBitmap(userBitmapProf);
                }
            }
        }




    }

    private void updateUser() {
       cancelProgressBar.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        String msg="";

        if (!edtFather.getText().toString().isEmpty()) {
            params.put("f_name", edtFather.getText().toString());
        }else{
          msg="Kindly Enter Father Name";
        }


        if (!edtEmail.getText().toString().isEmpty()) {
            params.put("email", edtEmail.getText().toString());
        }else{
            msg="Kindly Enter Email Id";
        }
        params.put("number", edtPhone.getText().toString());


        params.put("d_type", spinner.getSelectedItem().toString());

        params.put("u_type", "profile");

        if (!edtDocNo.getText().toString().isEmpty()) {
            params.put("d_no", edtDocNo.getText().toString());
        }else{
            msg="Kindly Enter Document Number";
        }
        if (userBitmap != null) {
            String encoded = Utils.encodeToBase64(userBitmap, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("profile", encoded);
        }else{
            params.put("profile", "");
        }
        if (userBitmapProf != null) {
            String encoded = Utils.encodeToBase64(userBitmapProf, Bitmap.CompressFormat.JPEG, 90);
            encoded = String.format("data:image/jpeg;base64,%s", encoded);
            params.put("d_image", encoded);
        }else{
            params.put("d_image", "");
        }
//        params.put("d_image", deviceid);
//        params.put("profile", deviceid);
//        Log.e("profile", String.valueOf(userBitmap));
//        Log.e("d_image", String.valueOf(userBitmapProf));

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_DATA_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
               cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                        User.setCurrentUser(user_obj);
                        //    Session_Bean.setCurrentSessiondetail(RecruiterUser.getCurrentRecruiterUser().getId(), 2);
                        Toast.makeText(UpdateUserDetailsActivity.this, "Profile Updated!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(UpdateUserDetailsActivity.this, ProfileActivity.class));
                        finish();
                      //  Animatoo.animateZoom(UpdateUserDetailsActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(UpdateUserDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        imgBackmsg.startAnimation(animation);
       commonBack();
    }

    private void commonBack() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        startActivity(new Intent(UpdateUserDetailsActivity.this, ProfileActivity.class));
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure, You want to exit?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    @OnClick(R.id.submit)
    public void submitt() {
        submit.startAnimation(animation);
        updateUser();
    }

    @OnClick(R.id.edtPhone)
    public void edtPhoneeee() {
        edtPhone.startAnimation(animation);

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        cancelProgressBar.show();
                        List<AuthUI.IdpConfig> providers = Arrays.asList(
                                new AuthUI.IdpConfig.PhoneBuilder().build());
                        startActivityForResult(
                                AuthUI.getInstance()
                                        .createSignInIntentBuilder()
                                        .setTheme(R.style.phoneTheme)
                                        .setLogo(R.drawable.logo)
                                        .setAvailableProviders(providers)
                                        .setIsSmartLockEnabled(false,
                                                true)
                                        .build(),
                                VERIFY_NUMBER);

                        AuthUI.getInstance()
                                .signOut(UpdateUserDetailsActivity.this)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    public void onComplete(@NonNull Task<Void> task) {
                                        //              Toast.makeText(LoginActivity.this,"SignOut",Toast.LENGTH_SHORT).show();
                                    }
                                });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Want to Update Your Current Number?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();



       // updateUser();
    }

    @OnClick(R.id.edtName)
    public void edtNameee() {
        edtName.startAnimation(animation);
        Toast.makeText(getApplicationContext(),"Name Is Not Allowed to Update",Toast.LENGTH_SHORT).show();
    }

    private void setUserData() {

        edtName.setText(user.getName());

        if (user.getFather_name()!=null) {
            edtFather.setText(user.getFather_name());
        }else{
            edtFather.setText("Not Mentioned");
        }

        if (user.getEmailid()!=null) {
            edtEmail.setText(user.getEmailid());
        }else{
            edtEmail.setText("Not Mentioned");
        }

        edtPhone.setText(user.getMobile());

        if (user.getAadharno()!=null) {
            edtDocNo.setText(user.getAadharno());
        }else{
            edtDocNo.setText("Not Mentioned");
        }


        if (User.getCurrentUser().getProfilepic() != null) {
            Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.person_1).into(myimage);
        } else {
            Picasso.with(this).load(R.drawable.person_1).into(myimage);
        }

        if (User.getCurrentUser().getDoc_upload() != null) {
            Picasso.with(this).load(User.getCurrentUser().getDoc_upload()).placeholder(R.drawable.categories).into(doc_image);
        } else {
            Picasso.with(this).load(R.drawable.categories).into(doc_image);
        }


    }
}