package com.customer.cardplanet.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.CardPlanetCustomerApp;
import com.customer.cardplanet.Fragments.BMIFragment;
import com.customer.cardplanet.Fragments.HistoryFragment;
import com.customer.cardplanet.Fragments.HomeFragment;
import com.customer.cardplanet.Fragments.SearchFragment;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.FirebaseGetDeviceToken;
import com.customer.cardplanet.Utility.Utils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class FinalHomeActivity extends AppCompatActivity implements OnMapReadyCallback {


    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.location_text)
    TextView location_text;
    @BindView(R.id.profile)
    CircleImageView profile;
    private FloatingNavigationView mFloatingNavigationView;
    BottomNavigationView bottomNavigationView;
    FragmentManager fragmentManager;
    @BindView(R.id.main_container)
    FrameLayout frameLayout;
    Fragment fragment;
    private int mMenuItemSelected;
    private static final String SELECTED_ITEM = "selected_item";
    private static final String BACK_STACK_ROOT_TAG = "root_home_fragment";
    TextView stu_name,amount;
    CircularImageView navi_profile;
    CardPlanetProgress cancelProgressBar;
    private Animation animation;
    private JSONObject user_obj;
    @BindView(R.id.layt)
    LinearLayout layt;
    int update=0;
    RelativeLayout btnNo;
   @BindView(R.id.qr_code)
    ImageView qr_code;
    Dialog dialogLog,dialogLog1;
    ImageView imageViewBackground;
    TextView title;
    TextView disc;
    private Bitmap bitmap;
    private QRGEncoder qrgEncoder;
    String inputValue;
    String c_add="";
    String address,area,longtude,lattude,city,state,pincode;;


    SharedPreferences sharedPreferences;
    //preferences for device token
    SharedPreferences pref;
    private String deviceid;
    private int deviceIdChecked = 0;

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    FusedLocationProviderClient mFusedLocationClient;

    @RequiresApi(api = Build.VERSION_CODES.R)
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_home);

        ButterKnife.bind(this);
        //CheckGpsStatus();
        Utils.hideKeyboard(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        CheckGpsStatus();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
        getDeviceTocken();

        if (User.getCurrentUser().getProfilepic()!=null){
            Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.userss).into(profile);
        }
        createPermissionListeners();

        Dexter.withContext(getApplicationContext())
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_CONTACTS,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {

            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

            }
        }).check();


        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        cancelProgressBar = new CardPlanetProgress(this);
        fragmentManager = getSupportFragmentManager();

        if (findViewById(R.id.main_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
        }

        setFragment(new HomeFragment());

        // fab.setBackgroundColor(R.color.white);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setBackground(null);
        bottomNavigationView.getMenu().getItem(2).setEnabled(false);

        mFloatingNavigationView = (FloatingNavigationView) findViewById(R.id.floating_navigation_view);

        /* Side Drawer and Hrader files set data here*/
        View headerView = mFloatingNavigationView.getHeaderView(0);
        stu_name = (TextView) headerView.findViewById(R.id.stu_name);
        amount = (TextView) headerView.findViewById(R.id.amount);
        navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);

        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mFloatingNavigationView.open();

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                cancelProgressBar.show();
                            User.getCurrentUser().logout();
                            //     Toast.makeText(CustomerHomeActivity.this, "Logged out successfully", Toast.LENGTH_SHORT).show();
                            Intent loginpage = new Intent(FinalHomeActivity.this, LoginActivity.class);
                            loginpage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginpage);
                            cancelProgressBar.dismiss();
                            finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(FinalHomeActivity.this);
                builder.setMessage("Are you sure, You want to Logout?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });

     //   bottomNavigationView.setItemIconTintList(null);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
           @Override
           public boolean onNavigationItemSelected(@NonNull MenuItem item) {
               fragment=null;

               switch (item.getItemId()) {

                   case R.id.home:
                       //   bottomNavigationView.setItemBackgroundResource(R.color.one);
                       fragment = new HomeFragment();
                       setFragment(fragment);
                       return true;

                   case R.id.search:
                       //   bottomNavigationView.setItemBackgroundResource(R.color.two);
                       fragment = new SearchFragment();
                       setFragment(fragment);
                       return true;

                   case R.id.bmi:
                       //  bottomNavigationView.setItemBackgroundResource(R.color.three);
                       fragment = new BMIFragment();
                       setFragment(fragment);
                       return true;
                   default:
                       fragment = new HistoryFragment();
                       setFragment(fragment);
                       return true;


               }
           }
       });
    }

    private void createPermissionListeners() {


    }

    @Override
    protected void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {

        if (User.getCurrentUser().getOptionaladdress()==null) {
            c_add=User.getCurrentUser().getAddress();
        } else if (User.getCurrentUser().getOptionaladdress().equalsIgnoreCase("empty")){
            c_add=User.getCurrentUser().getAddress();
        }else{
            c_add=User.getCurrentUser().getOptionaladdress();
        }

        location_text.setText(c_add);
    }

    public void CheckGpsStatus(){

        LocationManager locationManager ;
        boolean GpsStatus;
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(GpsStatus == true) {
            //   location_text.setText("GPS Is Enabled");
         //   Toast.makeText(FinalHomeActivity.this, "GPS Is Enabled", Toast.LENGTH_SHORT).show();

        } else {
            //  location_text.setText("GPS Is Disabled");
            //    Toast.makeText(LocationActivity.this, "Error to fetch location, kindly turn on your GPS First", Toast.LENGTH_SHORT).show();
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent1);
                            // finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            Toast.makeText(FinalHomeActivity.this, "Error to fetch location, Try again later.", Toast.LENGTH_SHORT).show();
                            finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(FinalHomeActivity.this);
            builder.setCancelable(false).setMessage("Kindly, Enable Your GPS First!").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }
    }


    private void getDeviceTocken() {

        pref = CardPlanetCustomerApp.getInstance().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        deviceid = pref.getString(Constants.DEVICE_TOKEN, "");
        if (deviceid.isEmpty()) {
            FirebaseGetDeviceToken.getDeviceToken();
        }
        if (deviceIdChecked != 0) {
            if (deviceIdChecked < 4) {
                checkDeviceIDValid();
            }
        }
    }


    private void checkDeviceIDValid() {

        if (deviceid == null && deviceid.isEmpty()) {
            if (deviceIdChecked < 3) {
                deviceIdChecked++;
                getDeviceTocken();
            } else {
                //   showSnackBar("Please wait.....");
                Toast.makeText(FinalHomeActivity.this, "Please wait.....", Toast.LENGTH_SHORT).show();
                FirebaseGetDeviceToken.getDeviceToken();
            }
        } else {
            Toast.makeText(FinalHomeActivity.this, "Error to getting device token.....", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.fab)
    public void create_qr_scanner() {
       // fab.startAnimation(animation);
        startActivity(new Intent(getApplicationContext(),ScanMerchantQRActivity.class));
        Animatoo.animateZoom(this);
    }

    @OnClick(R.id.profile)
    public void profilesssss() {
        profile.startAnimation(animation);
        startActivity(new Intent(getApplicationContext(),UserMenuActivity.class));
        Animatoo.animateFade(this);
    }




    @OnClick(R.id.layt)
    public void layt() {
        location_text.startAnimation(animation);

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                       // startActivity(new Intent(getApplicationContext(),LocationActivity.class));
                      //  Animatoo.animateShrink(FinalHomeActivity.this);
                       // finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };




        AlertDialog.Builder builder = new AlertDialog.Builder(FinalHomeActivity.this);
        builder.setMessage("Your Current address is "+"'"+c_add+"'"+ "").setPositiveButton("OK", dialogClickListener)
                .show();


    }

    @OnClick(R.id.qr_code)
    public void create_qr_code() {
        qr_code.startAnimation(animation);
        Animatoo.animateShrink(this);

        dialogLog1 = new Dialog(this);
        dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(true);
        dialogLog1.setContentView(R.layout.qr_dialoge);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView imageView = (ImageView) dialogLog1.findViewById(R.id.myimage);

        if (User.getCurrentUser() !=null){
            inputValue=User.getCurrentUser().getId();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();
            // Setting Bitmap to ImageView
            imageView.setImageBitmap(bitmap);
        }else {
            dialogLog1.dismiss();
        }
        btnNo = (RelativeLayout) dialogLog1.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog1.dismiss();

            }
        });
        dialogLog1.show();
    }


    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction= getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment);
        fragmentTransaction.commit();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(80000); // two minute interval
        mLocationRequest.setFastestInterval(80000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                        Looper.myLooper());
                mGoogleMap.setMyLocationEnabled(true);


                mGoogleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        LatLng ltlng=new LatLng(location.getLatitude(),location.getLongitude());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                                ltlng, 18F);
                        mGoogleMap.animateCamera(cameraUpdate);
                        String fetch= getAddress(ltlng);
                        // Toast.makeText(getApplicationContext(),"Current address "+fetch,Toast.LENGTH_SHORT).show();

                       if (update==0) {
                           if (address!=null) {
                               updateLocation();
                           }
                       }
                      //  title.setText(area);
//                        add.setText(fetch);

                    }
                });



            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                    Looper.myLooper());
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    private void updateLocation() {
        update=1;
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", User.getCurrentUser().getId());
        if (address!=null) {
            params.put("address", address);
        }else{
            params.put("address", "address");
        }
        Log.e("address", address);
        if (area!=null) {
            params.put("opt_address", area);
        }else{
            params.put("opt_address", "empty");
        }

        if (city!=null) {
            params.put("city", city);
        }else{
            params.put("city", "city");
        }

        params.put("state", state);
        Log.e("state", state);
        params.put("pincode", pincode);
        Log.e("pincode", pincode);

       // Log.e("opt_address", area);
        params.put("u_type", "location");
        params.put("latitude", lattude);
        Log.e("latitude", lattude);
        params.put("longitude", longtude);
        Log.e("longitude", longtude);
        params.put("device_token", deviceid);
        Log.e("device_token", deviceid);

        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_DATA_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error ) {
//                UltimateProgress.cancelProgressBar();
                if (error == null) {
                    try {
                        user_obj = jsonObject.getJSONObject(Constants.USER_DATA);
                        User.setCurrentUser(user_obj);
                        //    Session_Bean.setCurrentSessiondetail(RecruiterUser.getCurrentRecruiterUser().getId(), 2);
                   //     Toast.makeText(FinalHomeActivity.this, "Location Updated!", Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                  //  Toast.makeText(FinalHomeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this, params);
    }

    private String getAddress(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(FinalHomeActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            lattude= String.valueOf(latLng.latitude);
            longtude= String.valueOf(latLng.longitude);
            address = addresses.get(0).getAddressLine(0);
            area=addresses.get(0).getSubLocality();
            city=addresses.get(0).getLocality();
            state=addresses.get(0).getAdminArea();
            pincode=addresses.get(0).getPostalCode();
          //  Toast.makeText(getApplicationContext(),"address"+area+ "\n" +longtude,Toast.LENGTH_SHORT).show();
            return address;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"error"+e.getMessage(),Toast.LENGTH_SHORT).show();
            return e.getMessage();
        }

    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //move map camera
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latLng.latitude, latLng.longitude)).zoom(16).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    };

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(FinalHomeActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        mGoogleMap.setMyLocationEnabled(true);

                    }

                } else {
                    // if not allow a permission, the application will exit
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                }
            }
        }
    }
}