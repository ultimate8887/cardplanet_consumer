package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.AdapterClasses.MerchantDiscountAdapter;
import com.customer.cardplanet.BeanClasses.Cust_ServiceBean;
import com.customer.cardplanet.BeanClasses.Discount;
import com.customer.cardplanet.BeanClasses.MerchantBean;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DiscountWiseActivity extends AppCompatActivity implements MerchantDiscountAdapter.ProdMethodCallBack {
    @BindView(R.id.noData)
    RelativeLayout noData;
    CardPlanetProgress cardPlanetProgress;
    @BindView(R.id.viewRecycleProduct)
    RecyclerView viewRecycleProduct;
    ArrayList<MerchantBean> merchantBeans = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private MerchantDiscountAdapter productAdapter;
    @BindView(R.id.searchView)
    SearchView searchView;
    private Animation animation;
    String es_cate_id="";
    Discount discount;
    @BindView(R.id.top_layout)
    RelativeLayout top_layout;

    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.txtTitle)
    TextView txtTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_common_filter);
        ButterKnife.bind(this);
        cardPlanetProgress=new CardPlanetProgress(this);
        searchView.setQueryHint("Enter discount ...");
        top_layout.setVisibility(View.VISIBLE);

        Bundle intent_value = getIntent().getExtras();
        if (intent_value == null) {
            return;
        }
        if (intent_value.containsKey("discount_data")) {
            Gson gson = new Gson();
            discount = gson.fromJson(intent_value.getString("discount_data"), Discount.class);

        }

        txtTitle.setText("Get Discount Upto "+discount.getDiscount()+"%");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                //FILTER AS YOU TYPE
                productAdapter.getFilter().filter(query);

                return false;
            }
        });




        //set animation on adapter...
        int resId = R.anim.layout_animation_up_to_down;
        final LayoutAnimationController controller1 =
                AnimationUtils.loadLayoutAnimation(this, resId);
        viewRecycleProduct.setLayoutAnimation(controller1);
        //end animation...

        // Merchant list...
        layoutManager = new LinearLayoutManager(this);
        viewRecycleProduct.setLayoutManager(layoutManager);
        productAdapter = new MerchantDiscountAdapter(merchantBeans, this,this);
        viewRecycleProduct.setAdapter(productAdapter);
//        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        fetchMerchantList();
    }
    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        finish();
    }

    private void fetchMerchantList() {
    HashMap<String, String> params = new HashMap<>();

        params.put("cate_id", "");
        params.put("view", "discount");
        params.put("m_id", discount.getDiscount());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.MERCHANT_URL, new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            cardPlanetProgress.dismiss();
            if (error == null) {
                try {
                    if (merchantBeans != null)
                        merchantBeans.clear();

                    merchantBeans = MerchantBean.parseMerchantBeanArray(jsonObject.getJSONArray("merchant_data"));

                    if (merchantBeans.size() > 0){
                        productAdapter.setMerchantBeans(merchantBeans);
                        productAdapter.notifyDataSetChanged();
                        //setanimation on adapter...
                        viewRecycleProduct.getAdapter().notifyDataSetChanged();
                        viewRecycleProduct.scheduleLayoutAnimation();

                        //  txtTitle.setText(productAdapter.getItemCount());
                        //-----------end------------
                        noData.setVisibility(View.GONE);
                    }else{
                        productAdapter.setMerchantBeans(merchantBeans);
                        productAdapter.notifyDataSetChanged();
                        noData.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                merchantBeans.clear();
                productAdapter.setMerchantBeans(merchantBeans);
                productAdapter.notifyDataSetChanged();
                noData.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
    }, this, params);
}
    @Override
    public void prodMethod(MerchantBean cust_serviceBean) {
        Gson gson = new Gson();
        String prod_data = gson.toJson(cust_serviceBean, MerchantBean.class);
        Intent intent = new Intent(DiscountWiseActivity.this, MerchantDetailsWithransaction.class);
        intent.putExtra("merchant_data", prod_data);
        startActivity(intent);
        Animatoo.animateShrink(this);
    }

    @Override
    public void prodAddCartMethod(MerchantBean productBean) {

    }
}