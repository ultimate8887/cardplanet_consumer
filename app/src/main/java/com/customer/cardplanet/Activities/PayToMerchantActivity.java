package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.CardDetailbean;
import com.customer.cardplanet.BeanClasses.Customer_Saving;
import com.customer.cardplanet.BeanClasses.Merchant_Category;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.customer.cardplanet.Utility.Utils;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.textfield.TextInputLayout;
import com.razorpay.Checkout;
import com.razorpay.Order;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class PayToMerchantActivity extends AppCompatActivity implements PaymentResultWithDataListener {


    @BindView(R.id.edtbillingamount)
    EditText edtbillingamount;
//    @BindView(R.id.edtreddemvoucher)
//    EditText edtreddemvoucher;
    @BindView(R.id.edtsubTotal)
    EditText edtsubTotal;
    @BindView(R.id.edtdiscount)
    EditText edtdiscount;
    @BindView(R.id.edtcashbackvouch)
    EditText edtcashbackvouch;
    @BindView(R.id.edtremarks)
    EditText edtremarks;
    @BindView(R.id.edtgrandtotal)
    EditText edtgrandtotal;
    @BindView(R.id.edtwallet)
    EditText edtwallet;
    @BindView(R.id.myimage)
    CircularImageView myimage;
    @BindView(R.id.m_image)
    CircularImageView m_image;
    Dialog dialogLog,dialogLog1;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.submit)
    TextView submit;
    @BindView(R.id.amount)
    EditText pay_amount;
//    EditText amount;
    @BindView(R.id.wallet)
    TextInputLayout wallet;
    int billingamnt;
    int custcashback;
    int cashbackvoucher;
    CardPlanetProgress cancelProgressBar;
    ArrayList<CardDetailbean> customer_savings = new ArrayList<>();
    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    User user;
    private Animation animation;

    //RazorPay
    RazorpayClient razorpayClient;
    Order order;
    Checkout checkout;
    private String order_receipt_no = "Receipt No. " +  System.currentTimeMillis()/1000;
    private String order_reference_no = "Reference No. #" +  System.currentTimeMillis()/1000;
    //end.......

    String m_name="",m_id="",image="",discount="",amount="",
            wallet_amount="",voucher="",det="",ratio="",cust_wallet="",custdetailid="",f_id="",detuct_wallet_amount="",r_id="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_to_merchant);
        ButterKnife.bind(this);
        //RazorPay code start here....
        Checkout.preload(getApplicationContext());
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //end.......

        user=User.getCurrentUser();
        cancelProgressBar=new CardPlanetProgress(this);
        if (getIntent().getExtras()!=null){
            m_name=getIntent().getExtras().getString("m_name");
            m_id=getIntent().getExtras().getString("m_id");
            image=getIntent().getExtras().getString("image");
            discount=getIntent().getExtras().getString("discount");
            amount=getIntent().getExtras().getString("amount");
            f_id=getIntent().getExtras().getString("f_id");
        }

      //  Toast.makeText(getApplicationContext(),"f_id "+f_id,Toast.LENGTH_SHORT).show();
        edtremarks.setText("Done");
        pay_amount.setEnabled(false);
        pay_amount.setText(amount);
        fetCustDetails();
        edtbillingamount.setText(amount);
        billingamnt= Integer.parseInt(amount);

    }

    @Override
    protected void onResume() {
        super.onResume();
        fetCustDetails();
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        commonBack();
     //   finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        commonBack();
    }

    private void commonBack() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure, You want to exit?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void fetCustDetails() {

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id",User.getCurrentUser().getId());
        params.put("m_id",m_id);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.CUST_BILLING_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (customer_savings != null)
                            customer_savings.clear();
                        customer_savings = CardDetailbean.parseHWArray(jsonObject.getJSONArray("card_list"));

                        voucher=customer_savings.get(0).getCustcashback();
                        wallet_amount= customer_savings.get(0).getWallet();
                        det= String.valueOf(customer_savings.get(0).getDet());
                        ratio=customer_savings.get(0).getRatio();
                        custdetailid=customer_savings.get(0).getCustdetailid();
                        detuct_wallet_amount=customer_savings.get(0).getDetuct_wallet_amount();
                        Log.e("data",voucher+"   "+wallet_amount+"   "+det);

                        Log.e("custcashback", String.valueOf(custcashback));

                        if (det.equalsIgnoreCase("0")){
                            custcashback= Integer.parseInt(detuct_wallet_amount);
                            edtwallet.setText(String.valueOf(custcashback));
                            wallet.setHint(customer_savings.get(0).getCard_holderss());
                        }else{
                            custcashback= Integer.parseInt(voucher);
                            edtwallet.setText(String.valueOf(custcashback));
                            wallet.setHint(customer_savings.get(0).getCard_holderss());
                        }

                        Log.e("custcashback1", String.valueOf(custcashback));

                        if (!edtbillingamount.getText().toString().equalsIgnoreCase("0") ) {
                        edtdiscount.setText(discount);
                         int subtotal = billingamnt - custcashback;
                        int percent = Integer.parseInt(edtdiscount.getText().toString());
                        cashbackvoucher = (subtotal * percent) / 100;
                        edtcashbackvouch.setText(String.valueOf(cashbackvoucher));
                        edtsubTotal.setText(String.valueOf(subtotal));
                         edtgrandtotal.setText(String.valueOf(subtotal));
                        }

                        title.setText("Paying to "+m_name);
                        //if (!image.equalsIgnoreCase("")) {

                       if (image!="") {
                           Picasso.with(PayToMerchantActivity.this).load(image).placeholder(R.drawable.person_1).into(m_image);
                       }
                       if (User.getCurrentUser().getProfilepic()!=null){
                           Picasso.with(PayToMerchantActivity.this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.person_1).into(myimage);
                       }


//                        if (!customer_savings.get(0).getTotal().equalsIgnoreCase("0")){
//                            total_saving.setText(customer_savings.get(0).getTotal());
//                        }else {
//                            total_saving.setText("0");
//                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                     Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }


        }, this, params);
    }

    @OnClick(R.id.submit)
    public void submit() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        submit.startAnimation(animation);
        //   finish();
        //RazorPay code start here....

        if (!edtremarks.getText().toString().isEmpty()) {

            try {
                razorpayClient = new RazorpayClient(getResources().getString(R.string.razorpay_key_id), getResources().getString(R.string.razorpay_secret_key));
                Checkout.preload(getApplicationContext());
                checkout = new Checkout();
            } catch (RazorpayException e) {
                e.printStackTrace();
            }

            Map<String, String> headers = new HashMap<String, String>();
            razorpayClient.addHeaders(headers);

            try {
                JSONObject orderRequest = new JSONObject();

                String pay = edtgrandtotal.getText().toString();
                Log.e("pay", pay);
                double amm = Double.parseDouble(pay);
                Log.e("amm", String.valueOf(amm));
                int total = (int) (amm * 100);
                Log.e("total", String.valueOf(total));
                orderRequest.put("amount", total); // amount in the smallest currency unit
                orderRequest.put("currency", "INR");
                orderRequest.put("receipt", order_receipt_no);
                orderRequest.put("payment_capture", true);

                order = razorpayClient.Orders.create(orderRequest);

                startpayments(order);


            } catch (RazorpayException e) {
                // Handle Exception
                System.out.println(e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // startpayments(order);

        }else{
            Toast.makeText(getApplicationContext(),"Kindly Enter Service Name",Toast.LENGTH_SHORT).show();

        }

    }

    private void saveDataOnServer(String orderId, String paymentId){

        cancelProgressBar.show();
        HashMap<String, String> params = new HashMap<String, String>();
        //data comes from getcurrent user this is temporary that we are taken.
        params.put("p_id", m_id);
        Log.e("p_id", m_id);
        params.put("f_id", f_id);
        Log.e("f_id", f_id);
        //here are others
        params.put("orderId",orderId);
        Log.e("orderId",orderId);
        params.put("paymentId",paymentId);
        Log.e("paymentId",paymentId);
        params.put("custcashback",edtwallet.getText().toString());
        Log.e("custcashback",edtwallet.getText().toString());
        params.put("partner_discount",discount);
        Log.e("partner_discount",discount);
        params.put("detuct_wallet_amount",detuct_wallet_amount);
        Log.e("detuct_wallet_amount",detuct_wallet_amount);
        params.put("cashback",String.valueOf(cashbackvoucher));
        Log.e("cashback",String.valueOf(cashbackvoucher));
        params.put("billingamount",edtbillingamount.getText().toString());
        Log.e("billingamount",edtbillingamount.getText().toString());
        params.put("remarks","Online Payment Done");
        Log.e("remarks","Online Payment Done");
        params.put("service_type",edtremarks.getText().toString());
        Log.e("service_type",edtremarks.getText().toString());
        params.put("serviceholdername",User.getCurrentUser().getName());
        Log.e("serviceholdername",User.getCurrentUser().getName());
        params.put("card_id", user.getId());
        Log.e("card_id", user.getId());
        params.put("card_no", user.getCard_no());
        Log.e("card_no", user.getCard_no());
        params.put("grand_total", edtgrandtotal.getText().toString());
        Log.e("grand_total", edtgrandtotal.getText().toString());
        params.put("det",det);
        Log.e("det", det);
        params.put("ratio",ratio);
        Log.e("ratio", ratio);
        if(custdetailid!=null) {
            params.put("detid", custdetailid);
        }else{
            params.put("detid", "0");
        }
        params.put("wallet",wallet_amount);
        Log.e("wallet", wallet_amount);
        Log.e("jjhhjhj",Constants.getBaseURL() + Constants.ADD_SERVICE_URL);
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.ADD_SERVICE_URL, apiCallbacktttt, this, params);
    }

    ApiHandler.ApiCallback apiCallbacktttt = new ApiHandler.ApiCallback() {
        @Override
        public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
            cancelProgressBar.dismiss();
            if (error == null) {
                try {
                    r_id=jsonObject.getString("r_id");
                    Log.e("r_id", jsonObject.getString("r_id") + "");
                    Toast.makeText(getApplicationContext(),jsonObject.getString("r_id"),Toast.LENGTH_SHORT).show();
                   //finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("fetcherror", error.getMessage() + "");
                //  Utils.showSnackBar(error.getMessage(), parent);
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

            }
        }
    };




    private void startpayments(Order order) {
        checkout.setKeyID(getResources().getString(R.string.razorpay_key_id));

        Checkout checkout = new Checkout();

        /**
         * Set your logo here
         */
        // checkout.setImage(R.drawable.rzp_name_logo);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        try {

            JSONObject options = new JSONObject();
            options.put("name", "CARD PLANET");
            // options.put("description", "Reference No. #123456");
            String description="Order id: "+order.get("id");
            options.put("description", description);
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("order_id", order.get("id"));//from response of step 3.
            options.put("theme.color", "#3399cc");

            options.put("currency", "INR");

//            String pay=edtgrandtotal.getText().toString();
            String pay=edtgrandtotal.getText().toString();
            Log.e("pay",pay);
            double amm= Double.parseDouble(pay);
            Log.e("amm", String.valueOf(amm));
            int total= (int) (amm * 100);
            Log.e("total", String.valueOf(total));
            options.put("amount", total);//pass amount in currency subunits
            String email,phone;
            if (User.getCurrentUser().getEmailid() != null){
                email=User.getCurrentUser().getEmailid();
            }else{
                email="cardplanet@gmail.com";
            }
            phone=User.getCurrentUser().getMobile();

            JSONObject prefill = new JSONObject();
            prefill.put("email", email);
            prefill.put("contact",phone);
//        JSONObject retryObj = new JSONObject();
//        retryObj.put("enabled", true);
//        retryObj.put("max_count", 4);
            options.put("prefill", prefill);

            checkout.open(activity, options);

        } catch(Exception e) {
            Log.e("TAG", "Error in starting Razorpay Checkout", e);
            Toast.makeText(getApplicationContext(),"Error "+ e.getMessage(),Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
     //   Toast.makeText(getApplicationContext()," order_id "+ paymentData.getOrderId()+ "  pay_id "+paymentData.getPaymentId(),Toast.LENGTH_LONG).show();
     //   Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
      //  edtwallet.setText("Payment ID: " + s);
//        edtwallet.append("\nOrder ID: " + order.get("id"));
//        edtwallet.append("\n" + order_reference_no);
        saveDataOnServer(paymentData.getOrderId(),paymentData.getPaymentId());

        dialogLog1 = new Dialog(this);
        dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(false);
        dialogLog1.setContentView(R.layout.success_dialog);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        CircularImageView myimage = (CircularImageView) dialogLog1.findViewById(R.id.myimage);
        CircularImageView m_image = (CircularImageView) dialogLog1.findViewById(R.id.m_image);
        TextView titleee = (TextView) dialogLog1.findViewById(R.id.title);
        //title=(TextView) dialogLog1.findViewById(R.id.title);
        titleee.setText("Rupees Paid to "+"'"+m_name +"'"+" Successfully.");
        //if (!image.equalsIgnoreCase("")) {
        Picasso.with(this).load(image).placeholder(R.drawable.person_1).into(m_image);
        Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.person_1).into(myimage);

        //   Log.e("image",image);

        EditText amount1 = (EditText) dialogLog1.findViewById(R.id.amount);
        amount1.setEnabled(false);
        amount1.setText(amount);
        TextView btnProceed = (TextView) dialogLog1.findViewById(R.id.btnProceed);

        ImageView btnNo = (ImageView) dialogLog1.findViewById(R.id.imgBackmsg);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNo.startAnimation(animation);
                dialogLog1.dismiss();
                finish();
            }
        });
        TextView btnClose = (TextView) dialogLog1.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnClose.startAnimation(animation);
                if (r_id.equalsIgnoreCase("") || r_id==null ){
                    Toast.makeText(getApplicationContext(),"Error to View Receipt, Try Later",Toast.LENGTH_LONG).show();

                }else{
                    dialogLog1.dismiss();
                    Intent intent=new Intent(PayToMerchantActivity.this,ViewReceipt.class);
                    intent.putExtra("r_id",r_id);
                    startActivity(intent);
                    finish();
                    Animatoo.animateShrink(PayToMerchantActivity.this);
                }


            }
        });
        dialogLog1.show();


    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        Toast.makeText(getApplicationContext(),"Transaction Failed, Try Again",Toast.LENGTH_LONG).show();
       // edtwallet.setText("Error: " + s);
    }
}