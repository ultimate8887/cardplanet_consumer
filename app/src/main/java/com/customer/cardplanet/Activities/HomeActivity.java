package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.floatingnavigationview.FloatingNavigationView;
import com.android.volley.Request;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.customer.cardplanet.BeanClasses.Customer_Saving;
import com.customer.cardplanet.BeanClasses.Offer_Banner;
import com.customer.cardplanet.BeanClasses.User;
import com.customer.cardplanet.Fragments.HistoryFragment;
import com.customer.cardplanet.Fragments.HomeFragment;
import com.customer.cardplanet.Fragments.BMIFragment;
import com.customer.cardplanet.Fragments.SearchFragment;
import com.customer.cardplanet.R;
import com.customer.cardplanet.Utility.ApiHandler;
import com.customer.cardplanet.Utility.ApiHandlerError;
import com.customer.cardplanet.Utility.CardPlanetProgress;
import com.customer.cardplanet.Utility.Constants;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.zxing.WriterException;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.joery.animatedbottombar.AnimatedBottomBar;

public class HomeActivity extends AppCompatActivity {

AnimatedBottomBar animatedBottomBar;
FragmentManager fragmentManager;
    FrameLayout frameLayout;
    private FloatingNavigationView mFloatingNavigationView;
//    @BindView(R.id.headtext)
     public static TextView headtext;
    TextView stu_name,amount;
    CircularImageView navi_profile;
    CardPlanetProgress cancelProgressBar;

    @BindView(R.id.qr_scanner)
    ImageView qr_scanner;
    @BindView(R.id.qr_code)
    ImageView qr_code;

    RelativeLayout btnNo;
    Dialog dialogLog,dialogLog1;

    ArrayList<Offer_Banner> offer_banners;
    ArrayList<Customer_Saving> customer_savings;

    ImageView imageViewBackground;
    TextView title;
    TextView disc;
    private Animation animation;
    private Bitmap bitmap;
    private QRGEncoder qrgEncoder;
    String inputValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        frameLayout= (FrameLayout) findViewById(R.id.fragment_container_view_tag);
        animatedBottomBar= (AnimatedBottomBar) findViewById(R.id.bottom_bar);
        cancelProgressBar=new CardPlanetProgress(this);
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);

      headtext= (TextView) findViewById(R.id.headtext);
        if (savedInstanceState==null){
            animatedBottomBar.selectTabById(R.id.home,true);
            headtext.setText("Dashboard");
            HomeFragment homeFragment=new HomeFragment();
            fragmentManager=getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container_view_tag,homeFragment).commit();
        }

        animatedBottomBar.setOnTabSelectListener(new AnimatedBottomBar.OnTabSelectListener() {
            @Override
            public void onTabSelected(int lastIndex, @Nullable AnimatedBottomBar.Tab lasttab, int newIndx, @NotNull AnimatedBottomBar.Tab newtab) {
                Fragment fragment=null;

                switch (newtab.getId()){
                    case R.id.home:
                        fragment= new HomeFragment();
                        headtext.setText("Dashboard");
                        break;
                    case R.id.search:
                        fragment= new SearchFragment();
                        headtext.setText("Search");
                        break;
                    case R.id.bmi:
                        fragment= new BMIFragment();
                        headtext.setText("BMI Calculator");
                        break;
                    case R.id.history:
                        fragment= new HistoryFragment();
                        headtext.setText("History");
                        break;
                }

                if (fragment!=null){
                    fragmentManager=getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.fragment_container_view_tag,fragment).commit();
                }else{
                    Toast.makeText(getApplicationContext(),"Error to load Fragments",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onTabReselected(int i, @NotNull AnimatedBottomBar.Tab tab) {

            }
        });


        mFloatingNavigationView = (FloatingNavigationView) findViewById(R.id.floating_navigation_view);

        /* Side Drawer and Hrader files set data here*/
        View headerView = mFloatingNavigationView.getHeaderView(0);
        stu_name = (TextView) headerView.findViewById(R.id.stu_name);
        amount = (TextView) headerView.findViewById(R.id.amount);
        navi_profile = (CircularImageView) headerView.findViewById(R.id.navi_profile);
       // imgMoreUser = (ImageView) headerView.findViewById(R.id.imgMoreUser);


            if (User.getCurrentUser().getProfilepic() != null) {
                Picasso.with(this).load(User.getCurrentUser().getProfilepic()).placeholder(R.drawable.person_1).into(navi_profile);
            } else {
                Picasso.with(this).load(R.drawable.person_1).into(navi_profile);
            }

        if (User.getCurrentUser().getName() != null ) {
            stu_name.setText(User.getCurrentUser().getName());

        } else {
            stu_name.setText("");
        }

        mFloatingNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFloatingNavigationView.open();
            }
        });

        mFloatingNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                if (item.getTitle().equals("My Account")){
                   // Toast.makeText(getApplicationContext(),"Change Password",Toast.LENGTH_SHORT).show();
                    //  reset_password();
                    Intent intent=new Intent(HomeActivity.this,ProfileActivity.class);
                    startActivity(intent);
                    Animatoo.animateShrink(HomeActivity.this);
                } else if (item.getTitle().equals("Change Password")){
                    Toast.makeText(getApplicationContext(),"Change Password",Toast.LENGTH_SHORT).show();
                    //  reset_password();
                }
                else if (item.getTitle().equals("Feedback")){
                    Toast.makeText(getApplicationContext(),"Feedback",Toast.LENGTH_SHORT).show();
                    //  open_changePassword();
                } else if (item.getTitle().equals("Rate Us")){
                    Toast.makeText(getApplicationContext(),"Rate Us",Toast.LENGTH_SHORT).show();

                } else if (item.getTitle().equals("Share Us")){
                    Toast.makeText(getApplicationContext(),"Share Us",Toast.LENGTH_SHORT).show();


                } else if (item.getTitle().equals("Privacy Policy")){




                }
                else if (item.getTitle().equals("App Info")){
                    Toast.makeText(getApplicationContext(),"App Info",Toast.LENGTH_SHORT).show();
                    // oenInfoDialog();
                } else{

                    Toast.makeText(getApplicationContext(),"Logout!",Toast.LENGTH_SHORT).show();
                }

                //  Snackbar.make((View) mFloatingNavigationView.getParent(), item.getTitle() + " Selected!", Snackbar.LENGTH_SHORT).show();
                mFloatingNavigationView.close();
                return true;
            }
        });




    }

    @OnClick(R.id.qr_scanner)
    public void create_qr_scanner() {
        qr_scanner.startAnimation(animation);
        startActivity(new Intent(getApplicationContext(),ScannerView.class));
        Animatoo.animateShrink(this);
    }

    @OnClick(R.id.qr_code)
    public void create_qr_code() {
        qr_code.startAnimation(animation);
        Animatoo.animateShrink(this);

        dialogLog1 = new Dialog(this);
        dialogLog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog1.setCancelable(true);
        dialogLog1.setContentView(R.layout.qr_dialoge);
        dialogLog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView imageView = (ImageView) dialogLog1.findViewById(R.id.myimage);

       if (User.getCurrentUser() !=null){
            inputValue=User.getCurrentUser().getId();
        }
        if (!inputValue.equalsIgnoreCase("")) {
            //Toast.makeText(getApplicationContext(),"hiiii "+inputValue,Toast.LENGTH_LONG).show();
            QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 800);
//            qrgEncoder.setColorBlack(R.color.dark_black);
//            qrgEncoder.setColorWhite(R.color.white);
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.getBitmap();
            // Setting Bitmap to ImageView
            imageView.setImageBitmap(bitmap);
        }else {
            dialogLog1.dismiss();
        }
        btnNo = (RelativeLayout) dialogLog1.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLog1.dismiss();

            }
        });
        dialogLog1.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        fetchOfferList();
        fetWalletPrice();
    }

    private void fetWalletPrice() {

        HashMap<String, String> params = new HashMap<>();
        params.put("userid",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.SAVING_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (customer_savings != null)
                            customer_savings.clear();
                        customer_savings = Customer_Saving.parseCustomer_SavingArrayyy(jsonObject.getJSONArray("saving_data"));

                      //  Log.e("title" , offer_banners.get(0).getC_status());
                        if (!customer_savings.get(0).getWallet().equalsIgnoreCase("0")){
                            amount.setText(customer_savings.get(0).getWallet()+".00");
                        }else{
                            amount.setText("0");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }


        }, this, params);
    }

    private void offerDialog(ArrayList<Offer_Banner> offer_banners) {
        dialogLog = new Dialog(this);
        dialogLog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLog.setCancelable(false);
        dialogLog.setContentView(R.layout.offer_lyt);
        dialogLog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        imageViewBackground = (ImageView) dialogLog.findViewById(R.id.myimage);
        title = (TextView) dialogLog.findViewById(R.id.title);
        disc = (TextView) dialogLog.findViewById(R.id.disc);
        setMewsData(offer_banners);
        btnNo = (RelativeLayout) dialogLog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  updateNewsdta();

            }
        });
        dialogLog.show();
    }

    private void updateNewsdta() {
        cancelProgressBar.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("userid",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.UPDATE_OFFER_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                dialogLog.dismiss();
            }
        }, this, params);

    }

    private void fetchOfferList() {

        HashMap<String, String> params = new HashMap<>();
        params.put("userid",User.getCurrentUser().getId());
        ApiHandler.apiHit(Request.Method.POST, Constants.getBaseURL() + Constants.OFFER_URL, new ApiHandler.ApiCallback() {
            @Override
            public void onDataFetched(JSONObject jsonObject, ApiHandlerError error) {
                cancelProgressBar.dismiss();
                if (error == null) {
                    try {
                        if (offer_banners != null)
                            offer_banners.clear();
                        offer_banners = Offer_Banner.parseOffer_BannerArrayyy(jsonObject.getJSONArray("offer_data"));

                    //    Log.e("title" , offer_banners.get(0).getC_status());

                            if (offer_banners.get(0).getC_status().equalsIgnoreCase("0")){
                             offerDialog(offer_banners);
                             Animatoo.animateShrink(HomeActivity.this);
                            }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                   // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }


        }, this, params);

    }

    private void setMewsData(ArrayList<Offer_Banner> offer_banners) {

        if (offer_banners.get(0).getImage() != null) {
            Picasso.with(this).load(offer_banners.get(0).getImage()).placeholder(R.drawable.categories).into(imageViewBackground);
        } else {
            Picasso.with(this).load(R.drawable.categories).into(imageViewBackground);
        }

        if (offer_banners.get(0).getTitle() != null){
            title.setText(offer_banners.get(0).getTitle());
        }
        if (offer_banners.get(0).getDescription() != null){
            disc.setText(offer_banners.get(0).getDescription());
        }

    }

}