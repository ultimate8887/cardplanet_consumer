package com.customer.cardplanet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.customer.cardplanet.AdapterClasses.CateWiseViewPager;
import com.customer.cardplanet.AdapterClasses.FilterTabPager;
import com.customer.cardplanet.R;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterCateWiseActivity extends AppCompatActivity {

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tab)
    TabLayout tablayout;

    @BindView(R.id.imgBackmsg)
    ImageView imgBackmsg;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    private Animation animation;
    String data="",cate_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_history);
        ButterKnife.bind(this);
        if (getIntent().getExtras()!=null){
            cate_name=getIntent().getExtras().getString("cate_name");
            data=getIntent().getExtras().getString("cate_id");
        }
        txtTitle.setText(cate_name);
        setupTabPager();
    }

    @OnClick(R.id.imgBackmsg)
    public void imgBackssss() {
        animation = AnimationUtils.loadAnimation(this, R.anim.btn_blink_animation);
        imgBackmsg.startAnimation(animation);
        finish();
    }
    private void setupTabPager() {
        CateWiseViewPager adapter = new CateWiseViewPager(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
    }

    public String setdata(){
        String cate_id=data;
        return cate_id;
    }
}