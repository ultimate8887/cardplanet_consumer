package com.customer.cardplanet;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.multidex.MultiDexApplication;

import com.customer.cardplanet.Utility.Constants;

public class CardPlanetCustomerApp extends MultiDexApplication {
    private static CardPlanetCustomerApp mInstance;
    private static final String TAG = CardPlanetCustomerApp.class.getName();
    private SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        preferences = this.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        setOpenedDialog(false);
    }

    public static synchronized CardPlanetCustomerApp getInstance() {
        return mInstance;
    }

    @Override
    public void onTerminate() {
        mInstance = null;
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        //Utils.deleteCache(this);
        super.onLowMemory();
    }

    public void setOpenedDialog(boolean value) {
        preferences.edit().putBoolean(Constants.OPENED_STATUS, value).commit();
    }

    public boolean getOpenedDialog() {
        return preferences.getBoolean(Constants.OPENED_STATUS, true);
    }
}
